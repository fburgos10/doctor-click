var routes = [
  // Index page
  /*{
    path: '/',
    url: './home.html',
    name: 'home',
  },*/
  {
   path: '/',
   url: './home.html',
   tabs: [
     {
       path: '/',
       id: 'tab-1'
     },
     {
       path: '/tab-2/',
       id: 'tab-2'
     },
     {
       path: '/tab-3/',
       id: 'tab-3'
     },
     {
       path: '/tab-4/',
       id: 'tab-4'
     }
   ]
  },
  {
    path: '/prueba/:value?',
    url: './pages/prueba.html',
    name: 'prueba',
  },
  // Servicio page
  {
    path: '/servicio/:value?',
    url: './pages/servicio.html',
    name: 'servicio',
  },
  {
    path: '/visita-medica/:idDependiente?/:titular?',
    url: './pages/visita-medica.html',
    name: 'visita-medica',
  },
  {
    path: '/citas/',
    url: './pages/citas.html',
    name: 'citas',
  },
  {
    path: '/accioncita/',
    url: './pages/accioncita.html',
    name: 'accioncita',
  },
  {
    path: '/centros/:value?/:idDependiente?/:titular?',
    url: './pages/centros.html',
    name: 'centros',
  },
  // Factura page chat-linea
  {
    path: '/chat-linea/:value?',
    url: './pages/chat-linea.html',
    name: 'chat-linea',
  },
   {
    path: '/elegir-paciente/:value?',
    url: './pages/elegir-paciente.html',
    name: 'elegir-paciente',
  },
  {
    path: '/centros-elegir-profile/:value?/:idDependiente?/:titular?',
    url: './pages/centros-elegir-profile.html',
    name: 'centros-elegir-profile',
  },
   {
    path: '/chat/:name?',
    url: './pages/chat.html',
    name: 'chat',
  },
  {
    path: '/factura/:value?',
    url: './pages/factura.html',
    name: 'factura',
  },
  // Carrito page
  {
    path: '/carrito/:value?/:tipo?',
    url: './pages/carrito.html',
    name: 'carrito',
  },
  // Acerca de page
  {
    path: '/acerca/',
    url: './pages/acerca.html',
    name: 'acerca',
  },
  // Busqueda page
  {
    path: '/dependientes/',
    url: './pages/dependientes.html',
    name: 'dependientes',
  },
  {
    path: '/seguro-medico/:idDependiente?/:titular?',
    url: './pages/seguro-medico.html',
    name: 'seguro-medico',
  },
  {
    path: '/add_seguro/',
    url: './pages/add_seguro.html',
    name: 'add_seguro',
  },
  {
    path: '/add_dependientes/:value?',
    url: './pages/add_dependientes.html',
    name: 'add_dependientes',
  },
  // Dependientes page 
  {
    path: '/busqueda/',
    url: './pages/busqueda.html',
    name: 'busqueda',
  },
  {
    path: '/perfil-doctor/:id?',
    url: './pages/perfil-doctor.html',
    name: 'perfil-doctor',
  },
  {
    path: '/hospital-profile/:id?/:idTypeCent?/:idDependiente?/:titular?/:lat?/:long?',
    url: './pages/hospital-profile.html',
    name: 'hospital-profile',
  },
  // Reset password 
  {
    path: '/pass-update/',
    url: './pages/pass-update.html',
    name: 'pass-update',
  },
  // Unete al equipo.
  {
    path: '/historial-medico/:idDependiente?/:titular?',
    url: './pages/historial-medico.html',
    name: 'historial-medico',
  },
  // Unete al equipo.
  {
    path: '/unete/',
    url: './pages/unete.html',
    name: 'unete',
  },
  // Restaurantes de page
  {
    path: '/restaurantes/:value?',
    url: './pages/restaurantes.html',
    name: 'restaurantes',
  },
  // Atencion al cliente
  {
    path: '/atencion/',
    url: './pages/atencion.html',
    name: 'atencion',
  },
  // Perfil
  {
    path: '/perfil/',
    url: './pages/perfil.html',
    name: 'perfil',
  },
  // Empresa de page
  {
    path: '/empresa/',
    url: './pages/empresa.html',
    name: 'empresa',
  },
   // Perfil de page
  {
    path: '/perfil-info/',
    url: './pages/perfil-info.html',
    name: 'perfil-info',
  },
   // Informacion personal
  {
    path: '/informacion-perfil/',
    url: './pages/informacion-perfil.html',
    name: 'informacion-perfil',
  },
   // Editar cita
  {
    path: '/editarCita/:idcita?/:iduser?',
    url: './pages/editarCita.html',
    name: 'editarCita',
  },
   // Editar cita
  {
    path: '/pagina-historial/:idDependiente?',
    url: './pages/pagina-historial.html',
    name: 'pagina-historial',
  },
   // Favoritos
  {
    path: '/favoritos/',
    url: './pages/favoritos.html',
    name: 'favoritos',
  },
   // Programar citas
  {
    path: '/programar-cita/:idDependiente?/:titular?',
    url: './pages/programar-cita.html',
    name: 'programar-cita',
  },,
   // Pagina de citas
  {
    path: '/pagina-citas/',
    url: './pages/pagina-citas.html',
    name: 'pagina-citas',
  },
   // detalle de Informacion personal
  {
    path: '/info-personal/',
    url: './pages/info-personal.html',
    name: 'info-personal',
  },
  {
    path: '/notificaciones/',
    url: './pages/notificaciones.html',
    name: 'notificaciones',
  },
  // busquedaCategoria de page 
  {
    path: '/busquedaCategoria/:value?',
    url: './pages/busquedaCategoria.html',
    name: 'busquedaCategoria',
  },
  {
    path: '/politica/',
    url: './pages/politica.html',
    name: 'politica',
  },
  // Pagina cambiar contraseña
  {
    path: '/cambiarPass/',
    url: './pages/cambiarPass.html',
    name: 'cambiarPass',
  },
  {
    path: '/politica-uso/',
    url: './pages/politica-uso.html',
    name: 'politica-uso',
  },
  // Detalle de page 
  {
    path: '/detalle/:idcita?/:iduser?/:num?/:centro?',
    url: './pages/detalle.html',
    name: 'detalle',
  }, 
  {
    path: '/detalle-seguro/:idseguro?',
    url: './pages/detalle-seguro.html',
    name: 'detalle-seguro',
  },
  {
    path: '/detalleRechazo/:idcita?/:idcentro?/:iddependiente?',
    url: './pages/detalleRechazo.html',
    name: 'detalleRechazo',
  },
  {
    path: '/detalleSolicitud/:idcita?/:iddoctor?',
    url: './pages/detalleSolicitud.html',
    name: 'detalleSolicitud',
  },
  // direccion de page
  {
    path: '/direccion/:value?',
    url: './pages/direccion.html',
    name: 'direccion',
  },
  // add_direccion de page
  {
    path: '/add_direccion/',
    url: './pages/add_direccion.html',
    name: 'add_direccion',
  },
  // Pago de page
  {
    path: '/pago/:value?',
    url: './pages/pago.html',
    name: 'pago',
  },
  // Metodo Pago de page
  {
    path: '/metodo-pago/',
    url: './pages/metodo-pago.html',
    name: 'metodo-pago',
  },
  // Formulario general
  {
    path: '/formulario-general/',
    url: './pages/formulario-general.html',
    name: 'formulario-general',
  },
  // Formulario antecedentes
  {
    path: '/formulario-antecedentes/',
    url: './pages/formulario-antecedentes.html',
    name: 'formulario-antecedentes',
  },
  // Formulario resultados
  {
    path: '/formResult/',
    url: './pages/formResult.html',
    name: 'formResult',
  },
  // Formulario otros antecedentes
  {
    path: '/formulario-otrosAntecedentes/',
    url: './pages/formulario-otrosAntecedentes.html',
    name: 'formulario-otrosAntecedentes',
  },
  
  // Formulario otros antecedentes
  {
    path: '/formulario-medicacion/',
    url: './pages/formulario-medicacion.html',
    name: 'formulario-medicacion',
  },
  // Add Pago de page
  {
    path: '/add_pago/',
    url: './pages/add_pago.html',
    name: 'add_pago',
  },
  // Terminos de page
  {
    path: '/terminos/',
    url: './pages/terminos.html',
    name: 'terminos',
  },
  // Contactanos de page
  {
    path: '/contactanos/',
    url: './pages/contactanos.html',
    name: 'contactanos',
  },
  // Seguimiento de page
  {
    path: '/seguimiento/:value?/:orden?/:lat?/:lng?',
    url: './pages/seguimiento.html',
    name: 'seguimiento',
  },
  {
    path: '/cancelar/:value?',
    url: './pages/cancelar.html',
    name: 'cancelar',
  },
  // Right Panel pages
  {
    path: '/panel-right-1/',
    content: '\
      <div class="page">\
        <div class="navbar">\
          <div class="navbar-bg"></div>\
          <div class="navbar-inner sliding">\
            <div class="left">\
              <a href="#" class="link back">\
                <i class="icon icon-back"></i>\
                <span class="if-not-md">Back</span>\
              </a>\
            </div>\
            <div class="title">Panel Page 1</div>\
          </div>\
        </div>\
        <div class="page-content">\
          <div class="block">\
            <p>This is a right panel page 1</p>\
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo saepe aspernatur inventore dolorum voluptates consequatur tempore ipsum! Quia, incidunt, aliquam sit veritatis nisi aliquid porro similique ipsa mollitia eaque ex!</p>\
          </div>\
        </div>\
      </div>\
    ',
  },
  {
    path: '/panel-right-2/',
    content: '\
      <div class="page">\
        <div class="navbar">\
          <div class="navbar-bg"></div>\
          <div class="navbar-inner sliding">\
            <div class="left">\
              <a href="#" class="link back">\
                <i class="icon icon-back"></i>\
                <span class="if-not-md">Back</span>\
              </a>\
            </div>\
            <div class="title">Panel Page 2</div>\
          </div>\
        </div>\
        <div class="page-content">\
          <div class="block">\
            <p>This is a right panel page 2</p>\
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo saepe aspernatur inventore dolorum voluptates consequatur tempore ipsum! Quia, incidunt, aliquam sit veritatis nisi aliquid porro similique ipsa mollitia eaque ex!</p>\
          </div>\
        </div>\
      </div>\
    ',
  },

  // Components
  {
    path: '/accordion/',
    url: './pages/accordion.html',
  },
  {
    path: '/action-sheet/',
    componentUrl: './pages/action-sheet.html',
  },
  {
    path: '/appbar/',
    componentUrl: './pages/appbar.html',
  },
  {
    path: '/autocomplete/',
    componentUrl: './pages/autocomplete.html',
  },
  {
    path: '/badge/',
    componentUrl: './pages/badge.html',
  },
  {
    path: '/buttons/',
    url: './pages/buttons.html',
  },
  {
    path: '/calendar/',
    componentUrl: './pages/calendar.html',
  },
  {
    path: '/calendar-page/',
    componentUrl: './pages/calendar-page.html',
  },
  {
    path: '/cards/',
    url: './pages/cards.html',
  },
  {
    path: '/cards-expandable/',
    url: './pages/cards-expandable.html',
  },
  {
    path: '/checkbox/',
    componentUrl: './pages/checkbox.html',
  },
  {
    path: '/chips/',
    componentUrl: './pages/chips.html',
  },
  {
    path: '/color-picker/',
    componentUrl: './pages/color-picker.html',
  },
  {
    path: '/contacts-list/',
    url: './pages/contacts-list.html',
  },
  {
    path: '/content-block/',
    url: './pages/content-block.html',
  },
  {
    path: '/data-table/',
    componentUrl: './pages/data-table.html',
  },
  {
    path: '/dialog/',
    componentUrl: './pages/dialog.html',
  },
  {
    path: '/elevation/',
    url: './pages/elevation.html',
  },
  {
    path: '/fab/',
    url: './pages/fab.html',
  },
  {
    path: '/fab-morph/',
    url: './pages/fab-morph.html',
  },
  {
    path: '/form-storage/',
    url: './pages/form-storage.html',
  },
  {
    path: '/gauge/',
    componentUrl: './pages/gauge.html',
  },
  {
    path: '/grid/',
    url: './pages/grid.html',
  },
  {
    path: '/icons/',
    componentUrl: './pages/icons.html',
  },
  {
    path: '/infinite-scroll/',
    componentUrl: './pages/infinite-scroll.html',
  },
  {
    path: '/inputs/',
    url: './pages/inputs.html',
  },
  {
    path: '/lazy-load/',
    url: './pages/lazy-load.html',
  },
  {
    path: '/list/',
    url: './pages/list.html',
  },
  {
    path: '/list-index/',
    componentUrl: './pages/list-index.html',
  },
  {
    path: '/login-screen/',
    componentUrl: './pages/login-screen.html',
  },
  {
    path: '/login-screen-page/',
    componentUrl: './pages/login-screen-page.html',
  },
  {
    path: '/menu/',
    componentUrl: './pages/menu.html',
  },
  {
    path: '/messages/',
    componentUrl: './pages/messages.html',
  },
  {
    path: '/navbar/',
    url: './pages/navbar.html',
  },
  {
    path: '/navbar-hide-scroll/',
    url: './pages/navbar-hide-scroll.html',
  },
  {
    path: '/notifications/',
    componentUrl: './pages/notifications.html',
  },
  {
    path: '/panel/',
    url: './pages/panel.html',
  },
  {
    path: '/photo-browser/',
    componentUrl: './pages/photo-browser.html',
  },
  {
    path: '/picker/',
    componentUrl: './pages/picker.html',
  },
  {
    path: '/popup/',
    componentUrl: './pages/popup.html',
  },
  {
    path: '/popover/',
    url: './pages/popover.html',
  },
  {
    path: '/preloader/',
    componentUrl: './pages/preloader.html',
  },
  {
    path: '/progressbar/',
    componentUrl: './pages/progressbar.html',
  },
  {
    path: '/pull-to-refresh/',
    componentUrl: './pages/pull-to-refresh.html',
  },
  {
    path: '/radio/',
    url: './pages/radio.html',
  },
  {
    path: '/range/',
    componentUrl: './pages/range.html',
  },
  {
    path: '/searchbar/',
    url: './pages/searchbar.html',
  },
  {
    path: '/searchbar-expandable/',
    url: './pages/searchbar-expandable.html',
  },
  {
    path: '/sheet-modal/',
    componentUrl: './pages/sheet-modal.html',
  },
  {
    path: '/skeleton/',
    componentUrl: './pages/skeleton.html',
  },
  {
    path: '/smart-select/',
    url: './pages/smart-select.html',
  },
  {
    path: '/sortable/',
    url: './pages/sortable.html',
  },
  {
    path: '/stepper/',
    componentUrl: './pages/stepper.html',
  },
  {
    path: '/subnavbar/',
    url: './pages/subnavbar.html',
  },
  {
    path: '/subnavbar-title/',
    url: './pages/subnavbar-title.html',
  },
  {
    path: '/swiper/',
    url: './pages/swiper.html',
    routes: [
      {
        path: 'swiper-horizontal/',
        url: './pages/swiper-horizontal.html',
      },
      {
        path: 'swiper-vertical/',
        url: './pages/swiper-vertical.html',
      },
      {
        path: 'swiper-space-between/',
        url: './pages/swiper-space-between.html',
      },
      {
        path: 'swiper-multiple/',
        url: './pages/swiper-multiple.html',
      },
      {
        path: 'swiper-nested/',
        url: './pages/swiper-nested.html',
      },
      {
        path: 'swiper-loop/',
        url: './pages/swiper-loop.html',
      },
      {
        path: 'swiper-3d-cube/',
        url: './pages/swiper-3d-cube.html',
      },
      {
        path: 'swiper-3d-coverflow/',
        url: './pages/swiper-3d-coverflow.html',
      },
      {
        path: 'swiper-3d-flip/',
        url: './pages/swiper-3d-flip.html',
      },
      {
        path: 'swiper-fade/',
        url: './pages/swiper-fade.html',
      },
      {
        path: 'swiper-scrollbar/',
        url: './pages/swiper-scrollbar.html',
      },
      {
        path: 'swiper-gallery/',
        componentUrl: './pages/swiper-gallery.html',
      },
      {
        path: 'swiper-custom-controls/',
        url: './pages/swiper-custom-controls.html',
      },
      {
        path: 'swiper-parallax/',
        url: './pages/swiper-parallax.html',
      },
      {
        path: 'swiper-lazy/',
        url: './pages/swiper-lazy.html',
      },
      {
        path: 'swiper-pagination-progress/',
        url: './pages/swiper-pagination-progress.html',
      },
      {
        path: 'swiper-pagination-fraction/',
        url: './pages/swiper-pagination-fraction.html',
      },
      {
        path: 'swiper-zoom/',
        url: './pages/swiper-zoom.html',
      },
    ],
  },
  {
    path: '/swipeout/',
    componentUrl: './pages/swipeout.html',
  },
  {
    path: '/tabs/',
    url: './pages/tabs.html',
  },
  {
    path: '/tabs-static/',
    url: './pages/tabs-static.html',
  },
  {
    path: '/tabs-animated/',
    url: './pages/tabs-animated.html',
  },
  {
    path: '/tabs-swipeable/',
    url: './pages/tabs-swipeable.html',
  },
  {
    path: '/tabs-routable/',
    url: './pages/tabs-routable.html',
    tabs: [
      {
        path: '/',
        id: 'tab1',
        content: ' \
        <div class="block"> \
          <p>Tab 1 content</p> \
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam enim quia molestiae facilis laudantium voluptates obcaecati officia cum, sit libero commodi. Ratione illo suscipit temporibus sequi iure ad laboriosam accusamus?</p> \
          <p>Saepe explicabo voluptas ducimus provident, doloremque quo totam molestias! Suscipit blanditiis eaque exercitationem praesentium reprehenderit, fuga accusamus possimus sed, sint facilis ratione quod, qui dignissimos voluptas! Aliquam rerum consequuntur deleniti.</p> \
          <p>Totam reprehenderit amet commodi ipsum nam provident doloremque possimus odio itaque, est animi culpa modi consequatur reiciendis corporis libero laudantium sed eveniet unde delectus a maiores nihil dolores? Natus, perferendis.</p> \
        </div> \
        ',
      },
      {
        path: '/tab2/',
        id: 'tab2',
        content: '\
        <div class="block"> \
          <p>Tab 2 content</p> \
          <p>Suscipit, facere quasi atque totam. Repudiandae facilis at optio atque, rem nam, natus ratione cum enim voluptatem suscipit veniam! Repellat, est debitis. Modi nam mollitia explicabo, unde aliquid impedit! Adipisci!</p> \
          <p>Deserunt adipisci tempora asperiores, quo, nisi ex delectus vitae consectetur iste fugiat iusto dolorem autem. Itaque, ipsa voluptas, a assumenda rem, dolorum porro accusantium, officiis veniam nostrum cum cumque impedit.</p> \
          <p>Laborum illum ipsa voluptatibus possimus nesciunt ex consequatur rem, natus ad praesentium rerum libero consectetur temporibus cupiditate atque aspernatur, eaque provident eligendi quaerat ea soluta doloremque. Iure fugit, minima facere.</p> \
        </div> \
        ',
      },
      {
        path: '/tab3/',
        id: 'tab3',
        content: '\
        <div class="block"> \
          <p>Tab 3 content</p> \
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam enim quia molestiae facilis laudantium voluptates obcaecati officia cum, sit libero commodi. Ratione illo suscipit temporibus sequi iure ad laboriosam accusamus?</p> \
          <p>Deserunt adipisci tempora asperiores, quo, nisi ex delectus vitae consectetur iste fugiat iusto dolorem autem. Itaque, ipsa voluptas, a assumenda rem, dolorum porro accusantium, officiis veniam nostrum cum cumque impedit.</p> \
          <p>Laborum illum ipsa voluptatibus possimus nesciunt ex consequatur rem, natus ad praesentium rerum libero consectetur temporibus cupiditate atque aspernatur, eaque provident eligendi quaerat ea soluta doloremque. Iure fugit, minima facere.</p> \
        </div> \
        ',
      },
    ],
  },
  {
    path: '/text-editor/',
    componentUrl: './pages/text-editor.html'
  },
  {
    path: '/toast/',
    componentUrl: './pages/toast.html',
  },
  {
    path: '/toggle/',
    url: './pages/toggle.html',
  },
  {
    path: '/toolbar-tabbar/',
    componentUrl: './pages/toolbar-tabbar.html',
    routes: [
      {
        path: 'tabbar/',
        componentUrl: './pages/tabbar.html',
      },
      {
        path: 'tabbar-labels/',
        componentUrl: './pages/tabbar-labels.html',
      },
      {
        path: 'tabbar-scrollable/',
        componentUrl: './pages/tabbar-scrollable.html',
      },
      {
        path: 'toolbar-hide-scroll/',
        url: './pages/toolbar-hide-scroll.html',
      },
    ],
  },
  {
    path: '/tooltip/',
    componentUrl: './pages/tooltip.html',
  },
  {
    path: '/treeview/',
    componentUrl: './pages/treeview.html',
  },
  {
    path: '/timeline/',
    url: './pages/timeline.html',
  },
  {
    path: '/timeline-vertical/',
    url: './pages/timeline-vertical.html',
  },
  {
    path: '/timeline-horizontal/',
    url: './pages/timeline-horizontal.html',
  },
  {
    path: '/timeline-horizontal-calendar/',
    url: './pages/timeline-horizontal-calendar.html',
  },
  {
    path: '/virtual-list/',
    componentUrl: './pages/virtual-list.html',
  },
  {
    path: '/virtual-list-vdom/',
    componentUrl: './pages/virtual-list-vdom.html',
  },
  {
    path: '/vi/',
    componentUrl: './pages/vi.html',
  },

  // Color Themes
  {
    path: '/color-themes/',
    componentUrl: './pages/color-themes.html',
  },

  // Effects
  {
    path: '/page-transitions/',
    componentUrl: './pages/page-transitions.html',
  },
  {
    path: '/page-transitions/:effect',
    componentUrl: './pages/page-transitions-effect.html',
  },

  // Page Loaders
  {
    path: '/page-loader-template7/:user/:userId/:posts/:postId/',
    templateUrl: './pages/page-loader-template7.html',
    // additional context
    options: {
      context: {
        foo: 'bar',
      },
    },
  },
  {
    path: '/page-loader-component/:user/:userId/:posts/:postId/',
    componentUrl: './pages/page-loader-component.html',
    // additional context
    options: {
      context: {
        foo: 'bar',
      },
    },
  },
  {
    path: '/master-detail/',
    url: './pages/master-detail-master.html',
    master: true,
    detailRoutes: [
      {
        path: '/master-detail/:id/',
        templateUrl: './pages/master-detail-detail.html',
      },
    ]
  },

  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
