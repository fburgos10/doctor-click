// Dom7
var $$ = Dom7;

// Theme
var theme = "auto";
if (document.location.search.indexOf("theme=") >= 0) {
  theme = document.location.search.split("theme=")[1].split("&")[0];
  console.log(theme);
}

// Init App
var app = new Framework7({
  id: "com.doctor.click",
  root: "#app",
  name: "Doctor Click",
  dialog: {
    title: "Doctor Click",
    buttonOk: "Ok",
  },
  theme: theme,
  data: function () {
    return {
      name: "Fernando Burgos",
      user: {
        firstName: "Fernando",
        lastName: "Burgos",
      },
      loading: false,
      effect: null,
    };
  },
  methods: {
    helloWorld: function () {
      app.dialog.alert("Hello World!");
    },
    load: function (effect) {
      var self = this;
      if (self.loading) return;
      self.$setState({
        effect: effect,
        loading: true,
      });
      setTimeout(function () {
        self.$setState({
          loading: false
        });
      }, 3000);
    },
  },
  routes: routes,
  input: {
    scrollIntoViewOnFocus: Framework7.device.cordova && !Framework7.device.electron,
    scrollIntoViewCentered: Framework7.device.cordova && !Framework7.device.electron,
  },
  // Cordova Statusbar settings
  /**+
  statusbar: {
    overlay: Framework7.device.cordova && Framework7.device.ios || 'auto',
    iosOverlaysWebView: true,
    androidOverlaysWebView: true,
  },*/
  popup: {
    closeOnEscape: true,
  },
  sheet: {
    closeOnEscape: true,
  },
  popover: {
    closeOnEscape: true,
  },
  actions: {
    closeOnEscape: true,
  },
  vi: {
    placementId: "pltd4o7ibb9rc653x14",
  },
  on: {
    init: function () {
      var f7 = this;
      if (f7.device.cordova) {
        // Init cordova APIs (see cordova-app.js)
        cordovaApp.init(f7);
      }
    },
  },
});

if (app.device.android) {
  console.log("It is android device");
  $$("#addCss").addClass("restAndroid-navbar");
  $$("#addCssi").addClass("restAndroid-navbar-inner");
  $$("#OnlyIos").hide();
  $$("#addCss").removeClass("restIOS-navbar");
  $$("#addCssi").removeClass("restIOS-navbar-inner");
}
var pictureSource;   // picture source - fuente de imagen*/
var destinationType; // sets the format of returned value - establece el formato del valor devuelto

$$(document).on("deviceready", function () {
  console.log("Device is ready");
  //localizar();
  pictureSource=navigator.camera.PictureSourceType;
  destinationType = navigator.camera.DestinationType;
  /*----------------------------NOTIFICACIONES---------------------------------*/
  var notificationOpenedCallback = function (jsonData) {
    console.log("notificationOpenedCallback: " + JSON.stringify(jsonData));
    /*----------------------------COSUNLTA DE TAREA------------------------------*/
    /* Aqui van las funciones a llamar */
    notificaciones(); //Llamar las notificaciones
    ordenes(); //Traer ordenes del cliente
    slideHome(); //Slide del home imagenes
    calificacion();
  };

  window.plugins.OneSignal.startInit("73ecb559-00fa-49c3-a713-096444003ee0")
    //.handleNotificationReceived(notificationOpenedCallback)
    .handleNotificationReceived(function (jsonData) {
      console.log("Notification received: \n" + JSON.stringify(jsonData));
      console.log("Did I receive a notification: " + JSON.stringify(jsonData));
      notificaciones(); //Llamar las notificaciones
      ordenes(); //Traer ordenes del cliente
      slideHome(); //Slide del home imagenes
      calificacion();
    })
    .handleNotificationOpened(function (jsonData) {
      console.log("Notification opened: \n" + JSON.stringify(jsonData));
      console.log(
        "didOpenRemoteNotificationCallBack: " + JSON.stringify(jsonData)
      );
      notificaciones(); //Llamar las notificaciones
      ordenes(); //Traer ordenes del cliente
      slideHome(); //Slide del home imagenes
      calificacion();
    })
    .endInit();

  //checkConnection(); // Llamando la informacion de conexion
  //onDeviceReady(); // Llamando la informacion del dispositivo
});

function capturePhoto() {
  navigator.camera.getPicture(onPhotoDataSuccess, onFail, {
    quality: 50,
    allowEdit: true,
    destinationType: destinationType.DATA_URL,
  });
}

function onFail(message) {
  alert(tr("Failed because") + " " + tr(message));
}

function onPhotoDataSuccess(imageData) {
  var smallImage = document.getElementById("smallImage");
  //smallImage.style.display = "block";
  smallImage.src = "data:image/jpeg;base64," + imageData;
  console.log(smallImage.src);
  var id = getParameterByName("id");
  app.request({
    type: "PUT",
    url: "http://52.14.84.187/api-enyfix/public/api/photo/add/" + id,
    data: {
      foto: smallImage.src,
    },
    beforeSend: function (xhr) {
      app.dialog.preloader("Cargando foto");
      //localStorage.removeItem("fotoProfile");
    },
    success: function (data) {
      app.dialog.close();
      console.log("Reponse: " + data);
      //window.localStorage.setItem("fotoProfile", smallImage.src);
    },
    error: function (xhr) {
      app.dialog.close();
      console.log("Error cargando foto");
    },
  });
}

////////////////////////////////////////
function capturePhoto1() {
  navigator.camera.getPicture(onPhotoDataSuccess1, onFail, {
    quality: 50,
    allowEdit: true,
    destinationType: destinationType.DATA_URL,
  });
}

function onPhotoDataSuccess1(imageData) {
  console.log('entre bien al 1');
  let imageSeguro = document.getElementById("seguroFoto");
  let aSeguro = document.getElementById("tomarSeguro");
  aSeguro.style.display = "none";
  imageSeguro.style.display = "block";
  imageSeguro.src = "data:image/jpeg;base64," + imageData;
  console.log(imageSeguro.src);
  // var id = getParameterByName("id");
  // app.request({
  //   type: "PUT",
  //   url: "https://laundryappmovil.com/api-nes/public/api/saveFotoS/" + id,
  //   data: {
  //     foto: imageSeguro.src,
  //   },
  //   beforeSend: function (xhr) {
  //     app.dialog.preloader("Cargando foto");
  //     //localStorage.removeItem("fotoProfile");
  //   },
  //   success: function (data) {
  //     app.dialog.close();
  //     console.log("Reponse: " + data);
  //     //window.localStorage.setItem("fotoProfile", smallImage.src);
  //   },
  //   error: function (xhr) {
  //     app.dialog.close();
  //     console.log("Error cargando foto");
  //   },
  // });
}

////////////////////////////////////////////////////////////////////////
function capturePhoto2() {
  navigator.camera.getPicture(onPhotoDataSuccess2, onFail, {
    quality: 50,
    allowEdit: true,
    destinationType: destinationType.DATA_URL,
  });
}

function onPhotoDataSuccess2(imageData) {
  console.log('entre bien al 2');
  let imageCedula = document.getElementById("cedulaFoto");
  let aCedula = document.getElementById("tomarCedula");
  aCedula.style.display = "none";
  imageCedula.style.display = "block";
  imageCedula.src = "data:image/jpeg;base64," + imageData;
  console.log(imageCedula.src);
  // var id = getParameterByName("id");
  // app.request({
  //   type: "PUT",
  //   url: "https://laundryappmovil.com/api-nes/public/api/saveFotoC/" + id,
  //   data: {
  //     foto: imageCedula.src,
  //   },
  //   beforeSend: function (xhr) {
  //     app.dialog.preloader("Cargando foto");
  //     //localStorage.removeItem("fotoProfile");
  //   },
  //   success: function (data) {
  //     app.dialog.close();
  //     console.log("Reponse: " + data);
  //     //window.localStorage.setItem("fotoProfile", smallImage.src);
  //   },
  //   error: function (xhr) {
  //     app.dialog.close();
  //     console.log("Error cargando foto");
  //   },
  // });
}

function localizar() {
  AdvancedGeolocation.start(
    function (success) {
      var jsonObject = JSON.parse(success);
      console.log(jsonObject);
      //alert('Latitud: '+jsonObject.latitude+' Longitude: '+jsonObject.longitude+' No bulto');
      $$("#lat").val(jsonObject.latitude);
      $$("#lon").val(jsonObject.longitude);
    },
    function (error) {
      console.log("ERROR! " + JSON.stringify(error));
    }, {
      minTime: 500, // Min time interval between updates (ms)
      minDistance: 1, // Min distance between updates (meters)
      noWarn: true, // Native location provider warnings
      providers: "all", // Return GPS, NETWORK and CELL locations
      useCache: true, // Return GPS and NETWORK cached locations
      satelliteData: false, // Return of GPS satellite info
      buffer: false, // Buffer location data
      bufferSize: 0, // Max elements in buffer
      signalStrength: false, // Return cell signal strength data
    }
  );
}

/**************************************************/
/*************Funciones de subir foto*************/
/*************************************************
function onPhotoDataSuccess(imageData) {
  var smallImage = document.getElementById('smallImage');
  smallImage.style.display = 'block';
  smallImage.src = "data:image/jpeg;base64," + imageData;
}

function capturePhoto() {
  navigator.camera.getPicture(onPhotoDataSuccess, onFail, {
    quality: 50,
    destinationType: destinationType.DATA_URL
  });
}

function onPhotoURISuccess(imageURI) {
  var largeImage = document.getElementById('largeImage');
  largeImage.style.display = 'block';
  largeImage.src = imageURI;
}

function onFail(message) {
  alert('Failed because: ' + message);
}*/

/* Detectar dispositivo */
var device = Framework7.prototype.device;
if (device.iphone) {
  /*Dispositivo IOs */
  $$(".navbar").addClass("relux");

  /* Menu Tabs efectt*/
  $$(".tab-link-active").addClass("color-tive");
  $$(".toolbar-inner .tab-link").click(function () {
    $$(".tab-link").removeClass("color-tive");
    $$(this).addClass("color-tive");
  });

  /*$$('#addHere').addClass("iosclass");
  $$('#addHereInner').addClass("iosclassInner");*/
}

/**************************************************/
/*****************PARAMETROS DE URL****************/
/**************************************************/
function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null ?
    "" :
    decodeURIComponent(results[1].replace(/\+/g, " "));
}
var id = getParameterByName("id");
var nombre = getParameterByName("nombre");
var apellido = getParameterByName("apellido");
var celular = getParameterByName("celular");
var telefono = getParameterByName("telefono");
var usuario = getParameterByName("usuario");
var password = getParameterByName("password");
var formulario = getParameterByName("formulario");
var correo = getParameterByName("correo");
var id_vehiculo = getParameterByName("id_vehiculo");
var ApiKey = "ffd788ec-908e-4234-abc9-848c6662bab1";

$$("#nombreApe").html("Doctor Click");
/*************************************************************/
/*****************VALIDAR NUMEROS EN DDF************************/
/************************************************************/
function validaNumericos(event) {
  if (event.charCode >= 48 && event.charCode <= 57) {
    return true;
  }
  return false;
}

/*************************************************************/
/*****************INDEX PAGE**********************************/
/************************************************************/
var _originalSize = $$(window).width() + $$(window).height();

$$(window).resize(function () {
  if ($$(window).width() + $$(window).height() != _originalSize) {
    console.log("keyboard show up");
    $$(".indexfondo").css("height", "700px");
    $$("#keywordFooter").hide();
  } else {
    console.log("keyboard closed");
    //$$(".terminos").css("position","absolute");
    $$(".indexfondo").css("height", "100%");
    $$("#keywordFooter").show();
  }
});

$$("#menuVisualizar").on("click", function () {
  app.views.main.router.navigate("/restaurantes-login/");
});

$$("#enviar-zip").on("click", function () {
  var zip = $$("#zip").val();
  var len = $$("#zip").val().length;

  if (zip == "") {
    app.dialog.alert("Completa el campo correctamente");
  } else if (len < 5) {
    app.dialog.alert("zip code incorrecto");
  } else {
    app.views.main.router.navigate("/register/" + zip);
  }
});

$$(document).on("page:init", '.page[data-name="centros"]', function (e) {
  var idTipoCentro = app.view.main.router.currentRoute.params.value;
  idDependiente = app.view.main.router.currentRoute.params.idDependiente;
  titular = app.view.main.router.currentRoute.params.titular;
  console.log("dependiente: " + idDependiente + " titular: " + titular);
  $$("#ConserIdCentro").val(idTipoCentro);
  tiposcentros(idTipoCentro, idDependiente, titular);
});

/* Pagina de Pago*/
$$(document).on("page:init", '.page[data-name="pago"]', function (e) {
  var past = app.view.main.router.currentRoute.params.value;
  if (past == 2) {
    $$("#otraRedi").show();
    $$("#normalRedi").hide();
  } else {
    $$("#normalRedi").show();
    $$("#otraRedi").hide();
  }

  pagos(); //llamando la funcion de pago

  $$(".metodoPagoLink").on("click", function () {
    app.views.main.router.navigate("/add_pago/");
  });
});

$$(document).on("page:init", '.page[data-name="perfil-info"]', function (e) {
  var nombre = getParameterByName("nombre");
  var apellido = getParameterByName("apellido");
  var celular = getParameterByName("telefono");
  var resetPass = getParameterByName("reset");
  var correo = getParameterByName("correo");

  // PROFILE MODULE
  $$("#PhoneNumberUser").keyup(function (event) {
    var len = $$("#PhoneNumberUser").val().length;
    var dato = $$("#PhoneNumberUser").val();

    if (len == 3 || len == 7) {
      $$("#PhoneNumberUser").val(dato + "-");
    }
  });

  $$("input[name=emailUser]").blur(function () {
    var eMaile = $$("#emailUser").val();
    validEmail(eMaile);
  });

  $$("#nameUser").val(nombre);
  $$("#LastNameUser").val(apellido);
  var res = celular.substr(0, 3);
  var res1 = celular.substr(3, 3);
  var res2 = celular.substr(6, 8);
  $$("#PhoneNumberUser").val(res + "-" + res1 + "-" + res2);
  $$("#emailUser").val(correo);

  var id = getParameterByName("id");
  traerFoto(id, 1);

  $$("#UpdateProfileInfo").on("click", function (e) {
    console.log("Presionaste boton actualizar perfil");

    var id = getParameterByName("id");
    var name = $$("#nameUser").val();
    var last = $$("#LastNameUser").val();
    var tele = $$("#PhoneNumberUser").val();
    var email = $$("#emailUser").val();

    var res = tele.substr(0, 3);
    var res1 = tele.substr(4, 3);
    var res2 = tele.substr(8, 8);
    var num = res + "" + res1 + "" + res2;

    var lenname = $$("#nameUser").val().length;
    var lenapellido = $$("#LastNameUser").val().length;
    var lentele = $$("#PhoneNumberUser").val().length;

    console.log(name + "" + last + "" + tele + "" + email);
    if (name == "" || last == "" || tele == "" || email == "") {
      app.dialog
        .create({
          title: "Doctor click",
          text: "Llena los campos correctamente",
          buttons: [{
            text: "OK",
          }, ],
          verticalButtons: true,
        })
        .open();
    } else if (lenname < 3) {
      app.dialog.alert("No es un nombre valido");
    } else if (lenapellido < 3) {
      app.dialog.alert("No es un apellido valido");
    } else if (lentele != 12 || isNaN(num)) {
      app.dialog.alert("No es un numero de telefono valido");
    } else {}
  });
});

/* Pagina de Contactanos*/
$$(document).on("page:init", '.page[data-name="contactanos"]', function (e) {
  var img_url =
    "https://maps.googleapis.com/maps/api/staticmap?center=18.3893875,-66.13051241&zoom=15&size=800x800&maptype=roadmap&markers=color:red%7Clabel:C%7C18.4571563,-69.9529851&key=AIzaSyB-ac3xmSzZIrXLNF-46hKymj56tKQH-s0";
  $$("#map_photo").css({
    "background-image": "url('" + img_url + "')",
    "background-position": "center",
    "background-repeat": "no-repeat",
    "background-size": "cover",
    height: "100%",
    width: "100%",
  });
});

function especialidades() {
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/especialidades",
    beforeSend: function (xhr) {},
    success: function (data) {
      var html = "";
      var tabsEspecial = "";
      console.log(data);
      tiposcentros();
      html += `<a href="#tab10000" class="tab-link tab-link-active" style="width: 30%" onclick="tiposcentros()">
      <img src="./img/stethoscope.png" style="width: 40%;">
      <p style="margin:0;">Populares</p>
      </a>`;
      tabsEspecial += `<div id="tab10000" class="page-content tab tab-active" style="padding: 0;padding-bottom: 0;">
      <div class="block" style="padding-left: 0;margin:0;">
      <div class="list media-list" style="margin: 0;padding-left:0;">
      <ul class="doctoresPopulares"></ul>
      </div>
      </div>
      </div>`;

      $.each(data, function (i, item) {
        html +=
          `<a href="#tab` +
          item.id +
          `" onclick="doctoresPopulares(` +
          item.id +
          `)" class="tab-link" style="width: 30%">
        <img src="./` +
          item.img +
          `" style="width: 40%;">
        <p style="margin:0;">` +
          item.especialidades +
          `</p>
        </a>`;

        tabsEspecial +=
          `<div id="tab` +
          item.id +
          `" class="page-content tab" style="padding-top: 0;padding-bottom: 0;">
        <div class="block" style="padding-left: 0;margin:0;">
        <div class="list media-list" style="margin: 0;padding-left:0;">
         <ul class="doctoresPopulares"></ul>
        </div>
        </div>
        </div>`;
      });
      $$(".tabsLink").html(html);
      $$(".tabsLinkMenu").html(tabsEspecial);
    },
    error: function () {
      app.preloader.hide();
    },
  });
}

function consultCat(id) {
  //Consultar la categoria
  console.log("[Doctor Click] Diste click " + id);
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/categoria/menu/" +
      id +
      "/" +
      idRestauranteMenu,
    beforeSend: function (xhr) {
      app.preloader.show();
    },
    success: function (data) {
      console.log(data);
      app.preloader.hide();
      var html = "";
      if (data == "" || data == "[]") {
        html += `
       <img src="img/notabalible.jpg" style="width:100%;">"`;
      } else {
        $.each(data, function (i, item) {
          if (item.nombre == "" || item.descripcion == null) {
            var descrip = "";
          } else {
            var descrip = item.descripcion;
          }
          html +=
            `
           <li>
           <a href="#" onclick="anadirCart(` +
            item.id +
            `)" class="item-link item-content sheet-open" data-sheet=".my-sheet-swipe-to-step">
           <div class="item-inner">
           <div class="item-title">
           ` +
            item.nombre +
            `
           <div class="item-footer">` +
            descrip +
            `</div>
           </div>
           <div class="item-after" style="color: darkgreen">US$ ` +
            formatter.format(item.precio).replace("$", "") +
            `</div>
           </div>
           </a>
           </li>
           `;
        });
      }
      $$(".pussy").html(html);
    },
    error: function (xhr) {
      console.log(xhr);
      app.preloader.hide();
    },
  });
}

$$(document).on("page:init", '.page[data-name="dependientes"]', function (e) {
  console.log("[Doctor Click] Pagina de dependientes");
  var id = getParameterByName("id");
  //listDependientes();
  app.request({
    type: "GET",
    dataType: "json",
    url: "http://laundryappmovil.com/api-nes/public/api/dependientes/" + id,
    beforeSend: function (xhr) {
      //app.dialog.preloader('Cargando...');
    },
    success: function (data) {
      //app.dialog.close();
      console.log(data);
      var html = "";
      if(data == ""){
        html = `<div style="text-align: center">
          <img src="../../img/group.png" style="width: 60%"/>
          <p style="color: #0f7bcf; font-weight: 600">Aún no tiene dependientes registrados.</p>
        </div>`;
      } else {
        $.each(data, function (i, item) {
          html +=
            `<li class="swipeout">
          <div href="#" class="item-content swipeout-content">
          <div class="item-media">
          <img src="../../img/img_avatar.png"/>
          </div>
          <div class="item-inner">
          <div class="item-title">
          <div class="item-header">` +
            item.parentesco +
            `</div>
          ` +
            item.nombre +
            ` ` +
            item.apellido +
            `
          </div>
          <div class="item-after">Edad ` +
            item.edad +
            `</div>
          </div>
          </div>
          <div class="swipeout-actions-right">
          <a href="#" class="open-more-actions" onclick="editarDependiente(` +
            item.id +
            `)">Editar</a>
          <a href="#" class="swipeout-delete" onclick="borrarDependiente(` +
            item.id +
            `)"><i class="f7-icons">trash</i></a>
          </div>
          </li>`;
        });
      }
      $$("#parentsList").html(html);
    },
    error: function (xhr) {
      app.dialog.close();
    },
  });
});

$$(document).on(
  "page:init",
  '.page[data-name="add_dependientes"]',
  function (e) {
    console.log("[Doctor Click] Pagina de añadir dependientes");
    listadoParent();

    var idDepend = app.view.main.router.currentRoute.params.value;
    if (idDepend >= 1) {
      $$(".SaveParent").hide();
      $$(".EditParent").show();
      $$(".titi").html("<center>Editar dependiente</center>");
      editParent(idDepend);
    } else {
      $$(".EditParent").hide();
      $$(".SaveParent").show();
      $$(".titi").html("<center>Añadir nuevo dependiente</center>");
    }

    $$(".SaveParent").on("click", function () {
      var nombreDependiente = $$("#nombreDependiente").val();
      var apellidoDependiente = $$("#apellidoDependiente").val();
      var edadDependiente = $$("#edadDependiente").val();
      var parentescoID = $$("#parentescoID").val();
      var id = getParameterByName("id");

      if (
        nombreDependiente == "" ||
        apellidoDependiente == "" ||
        edadDependiente == "" ||
        parentescoID == ""
      ) {
        app.dialog.alert("Completa todos los campos correctamente");
      } else {
        app.request({
          type: "POST",
          dataType: "json",
          url: "http://laundryappmovil.com/api-nes/public/api/dependientes/add",
          data: {
            nombre: nombreDependiente,
            apellido: apellidoDependiente,
            edad: edadDependiente,
            parentesco: parentescoID,
            id_cliente: id,
          },
          beforeSend: function (xhr) {},
          success: function (date) {
            console.log(date);
            app.dialog.alert("Dependiente agregado con exitos");
            $$("input").val("");
            $$("select").val("");
            listDependientes();
          },
          error: function (xhr) {
            console.log(xhr);
          },
        });
      }
    });

    $$(".EditParent").on("click", function () {
      var nombreDependiente = $$("#nombreDependiente").val();
      var apellidoDependiente = $$("#apellidoDependiente").val();
      var edadDependiente = $$("#edadDependiente").val();
      var parentescoID = $$("#parentescoID").val();
      var idPamt = $$("#idParent").val();

      if (
        nombreDependiente == "" ||
        apellidoDependiente == "" ||
        edadDependiente == "" ||
        parentescoID == ""
      ) {
        app.dialog.alert("Completa todos los campos correctamente");
      } else {
        app.request({
          type: "PUT",
          dataType: "json",
          url: "http://laundryappmovil.com/api-nes/public/api/dependientes/edit",
          data: {
            nombre: nombreDependiente,
            apellido: apellidoDependiente,
            edad: edadDependiente,
            parentesco: parentescoID,
            id: idPamt,
          },
          beforeSend: function (xhr) {},
          success: function (date) {
            console.log(date);
            app.dialog.alert("Dependiente editado con exitos");
            listDependientes();
          },
          error: function (xhr) {
            console.log(xhr);
          },
        });
      }
    });
  }
);

/* Pagina de menu*/
$$(document).on("page:init", '.page[data-name="restaurantes"]', function (e) {
  var idRestauranteMenu = app.view.main.router.currentRoute.params.value;
  counNoty();
  //categoriaMenu(idRestauranteMenu); // Categorias del Menu del restaurante.

  var searchbar = app.searchbar.create({
    el: ".searchbar",
    searchContainer: ".list",
    searchIn: ".item-title",
    on: {
      search(sb, query, previousQuery) {
        console.log(query, previousQuery);
      },
    },
  });

  $$("#bebidaVal").on("change", function (e) {
    console.log("Cambio el asunto");
  });

  $$("input[type=checkbox]:checked").each(function () {
    console.log("Cambio el asunto");
  });

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  $$("#goCart").on("click", function () {
    app.views.main.router.navigate("/carrito/2/" + idRestauranteMenu);
  });

  //menusi(idRestauranteMenu);

  $$("#addCarritoPedido").on("click", function () {
    var id = getParameterByName("id");
    var item = $$("#idItems").val();
    var cantidad = $$("#inputPlus").val();
    var precio = $$("#addPrice").val();
    var comentario = $$("#comentario").val();
    var porcentajeItemMunicipal = $$("#porcentajeItemMunicipal").val();
    var porcentajeItemState = $$("#porcentajeItemState").val();
    // Valores de los extras. precioExtras
    var checked = [];
    var precioExtras = [];
    var taxMExtras = [];
    var taxSextras = [];

    $$("input[name='extras[]']:checked").each(function () {
      var cadena1 = $$(this).val();
      checked.push(parseInt(cadena1.split(",")[0]));
      precioExtras.push(parseFloat(cadena1.split(",")[1]));
      taxMExtras.push(parseFloat(cadena1.split(",")[2]));
      taxSextras.push(parseFloat(cadena1.split(",")[3]));
    });

    console.log(precioExtras.join(", "));
    console.log(checked.join(", "));
    console.log(taxMExtras.join(", "));
    console.log(taxSextras.join(", "));
    //console.log('Precio: '+precio+' Cantidad: '+cantidad+' item: '+item+' id: '+id);
    app.request({
      type: "POST",
      url: "https://laundryappmovil.com/api-nes/api/addCart.php",
      data: {
        id: id,
        item: item,
        cantidad: cantidad,
        precio: precio,
        porcentajeItemMunicipal: porcentajeItemMunicipal,
        porcentajeItemState: porcentajeItemState,
        comentario: comentario,
        checked: checked,
        precioExtras: precioExtras,
        idRestauranteMenu: idRestauranteMenu,
        taxMExtras: taxMExtras,
        taxSextras: taxSextras,
      },
      beforeSend: function (xhr) {
        app.preloader.show();
      },
      success: function (data) {
        console.log(data);
        if (data == 2) {
          app.dialog.alert(
            "Ya tienes articulos de otro restaurante en el carrito."
          );
        }
        app.preloader.hide();
        counNoty();
      },
      error: function (xhr) {
        console.log(xhr);
        app.preloader.hide();
      },
    });
  });

  $$("#plusAdd").on("click", function () {
    //Total de precio
    var total = 1;
    var change = false; //
    var cant = parseFloat($("#inputPlus").val());
    $("#inputPlus").val(cant + 1);
    $(".monto").each(function () {
      if (!isNaN(parseFloat($(this).val()))) {
        change = true;
        total *= parseFloat($(this).val());
      }
    });
    total = change ? total : 0;
    document.getElementById("precioAdd").innerHTML =
      "US$ " + formatter.format(total).replace("$", "");
  });

  $$("#minusAdd").on("click", function () {
    //Resta del monto
    var total = 1;
    var change = false; //
    var cant = parseFloat($("#Cantidad").val());

    if (parseFloat($("#Cantidad").val()) === 1) {
      parseFloat($("#Cantidad").val(1));
    } else {
      $("#Cantidad").val(cant - 1);
    }

    $(".monto").each(function () {
      if (!isNaN(parseFloat($(this).val()))) {
        change = true;
        total *= parseFloat($(this).val());
      }
    });

    total = change ? total : 0;
    document.getElementById("precioAdd").innerHTML =
      "US$ " + formatter.format(total).replace("$", "");
  });
});

function doctoresPopulares(id) {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });
  var direct;
  if (id == "" || id == null) {
    direct = "https://laundryappmovil.com/api-nes/public/api/doctores/limite/";
  } else {
    direct =
      "https://laundryappmovil.com/api-nes/public/api/doctores/limit/" + id;
  }

  app.request({
    type: "GET",
    dataType: "json",
    url: direct,
    beforeSend: function (xhr) {},
    success: function (data) {
      console.log(data);
      var html = "";
      $.each(data, function (i, item) {
        html +=
          `<li>
        <a href="/perfil-doctor/` +
          item.id +
          `" class="item-link item-content">
        <div class="item-media">
        <img src="./img/doctorImg.jpg" width="60"/>
        </div>
        <div class="item-inner">
        <div class="item-title-row">
        <div class="item-title" style="font-weight: bold">` +
          item.nombre +
          ` ` +
          item.apellido +
          `</div>
        <div class="item-after"></div>
        </div>
        <div class="item-subtitle" style="font-style: italic;color: darkgray;">` +
          formatAMPMe(item.opentime) +
          ` - ` +
          formatAMPMe(item.closedtime) +
          `</div>
        <div class="item-text"> Este ` +
          minuscula(item.especialidades) +
          ` esta ` +
          minuscula(item.estatus) +
          `</div>
        </div>
        </a>
        </li>`;
        $$(".doctoresPopulares").html(html);
      });
    },
    error: function () {
      app.preloader.hide();
    },
  });
}

function minuscula(value) {
  var res = value.toLowerCase();
  return res;
}

//Pagina del perfil del Doctor perfil-doctor
$$(document).on("page:init", '.page[data-name="perfil-doctor"]', function (e) {
  var idDoct = app.view.main.router.currentRoute.params.id;
  listadoDoctores(idDoct);
});

//Pagina de Add pago
$$(document).on("page:init", '.page[data-name="add_pago"]', function (e) {
  /* Tabs hover effect */
  $$(".toolbar-inner .weo").click(function () {
    $$(".weo").removeClass("activess");
    $$(this).addClass("activess");
  });

  //Metodo de guardado de tarjetas de pago
  $$("#addPago").on("click", function (e) {
    console.log("Presioanste add pago");
    var numero = $$("#cardNumber").val();
    var mes = $$("#mes").val();
    var ano = $$("#ano").val();
    var cvv = $$("#cvv").val();
    var tipo_tarjeta = $$("#tipo_tarjeta").val();

    if (numero == "" || numero < 16 || cvv == "" || cvv < 3) {
      app.dialog.alert("Completa los campo correctamente");
    } else {
      var id = getParameterByName("id");
      app.request({
        type: "POST",
        url: "https://laundryappmovil.com/api-nes/public/api/pagos/add",
        data: {
          id_user: id,
          numero: numero,
          mes: mes,
          ano: ano,
          cvv: cvv,
          tipo_tarjeta: tipo_tarjeta,
          token: "fhbdx100",
        },
        beforeSend: function (xhr) {
          app.preloader.show();
        },
        success: function (data) {
          app.preloader.hide();
          console.log("toy aqui papa");
          console.log(data);
          app.dialog.alert("Tarjeta agregada con éxito.");
          $$("#cardNumber").val("");
          $$("#cvv").val("");
        },
        error: function (xhr) {
          app.preloader.hide();
          console.log("hubo error");
        },
      });
    }
  }); // Add pago end
});

// Pagina de reset password
$$(document).on("page:init", '.page[data-name="reset-password"]', function (e) {
  console.log("Estoy en la pagina de reset password");
  $$("#verificar-email").on("click", function () {
    var email = $$("#emailReset").val();
    var lenaEmail = $$("#emailReset").val().length;
    if (!isEmail(email) || lenaEmail < 4) {
      app.dialog.alert("Correo electronico invalido.");
    } else {
      app.request({
        type: "POST",
        url: "https://laundryappmovil.com/api-nes/correo/send.php",
        data: {
          email: email,
        },
        beforeSend: function (xhr) {
          app.dialog.preloader("Autenticando correo electrónico");
        },
        success: function (resp) {
          app.dialog.close();
          console.log(resp);
          if (resp == 2) {
            app.dialog.alert("Este correo no pertenece a ninguna cuenta.");
          } else {
            app.views.main.router.navigate("/send-email/");
          }
        },
        error: function (xhr) {
          app.dialog.close();
          console.log(xhr);
        },
      });
    }
  });
});

// Pagina de pass update
$$(document).on("page:init", '.page[data-name="pass-update"]', function (e) {
  console.log("Estoy en restablecer contraseña");

  $$("#ps_nueva")
    .keyup(function () {
      // set password variable
      var pswd = $(this).val();

      // Validar tamaño
      if (pswd.length < 8) {
        $$("#length").removeClass("valid").addClass("invalid");
      } else {
        $$("#length").removeClass("invalid").addClass("valid");
      }

      // Validar letras
      if (pswd.match(/[A-z]/)) {
        $$("#letter").removeClass("invalid").addClass("valid");
      } else {
        $$("#letter").removeClass("valid").addClass("invalid");
      }

      // Validar letra mayuscula
      if (pswd.match(/[A-Z]/)) {
        $$("#capital").removeClass("invalid").addClass("valid");
      } else {
        $$("#capital").removeClass("valid").addClass("invalid");
      }

      // Validar numero
      if (pswd.match(/\d/)) {
        $$("#number").removeClass("invalid").addClass("valid");
      } else {
        $$("#number").removeClass("valid").addClass("invalid");
      }
    })
    .focus(function () {
      $$("#pswd_info").show();
    })
    .blur(function () {
      $$("#pswd_info").hide();
    });

  $$("#cambiar-pass").on("click", function () {
    console.log("Presionaste cambiar pass");
    var newPass = $$("#ps_nueva").val();
    var oldPass = $$("#ps_actual").val();
    var lenNewPass = newPass.length;
    if (newPass == "" || oldPass == "") {
      app.dialog.alert("Completa los campos correctamente");
    } else if (lenNewPass < 8) {
      app.dialog.alert("Debería tener 8 carácteres como mínimo");
    } else {
      var id = getParameterByName("id");
      app.request({
        type: "PUT",
        url: "https://laundryappmovil.com/api-nes/public/api/cliente/pass/" + id,
        data: {
          pass: newPass,
        },
        beforeSend: function (xhr) {
          app.dialog.preloader("Actualizando contraseña");
        },
        success: function (resp) {
          app.dialog.close();
          app.dialog.alert("Contraseña actulizada con exito.");
          setTimeout(function () {
            logout();
          }, 1000);
        },
        error: function (xhr) {
          app.dialog.close();
          console.log(xhr);
        },
      });
    }
  });
});

// Pagina de reset password
$$(document).on("page:init", '.page[data-name="send-email"]', function (e) {
  console.log("Estoy en la pagina de send email");
  $$("#go-login").on("click", function () {
    app.views.main.router.navigate("/login/");
  });
});

/* Pagina de Registro*/
$$(document).on("page:init", '.page[data-name="register"]', function (e) {
  console.log("Estoy en la pagina de register");
  console.log(app.view.main.router.currentRoute.params.value);

  var calendarModal = app.calendar.create({
    inputEl: "#demo-calendar-modal",
    openIn: "customModal",
    header: true,
    footer: true,
  });
  //hash de la cedula
  $$("#cedula").keypress(function (event) {
    var len = $$("#cedula").val().length;
    var dato = $$("#cedula").val();

    if (len == 3 || len == 11) {
      $$("#cedula").val(dato + "-");
    }
  });

  $$("input[name=emaile]").blur(function () {
    var eMaile = $$("#emaile").val();
    validEmail(eMaile);
  });

  $$("#user").blur(function () {
    var UserName = $$("#user").val();
    UserNameTest(UserName);
  });

  $$("#save-user").on("click", function (e) {
    console.log("Presionaste Save User");

    var nombre = $$("#nombre").val();
    var apellido = $$("#apellido").val();
    var user = $$("#user").val();
    var telefono = $$("#telefono").val();
    var celular = $$("#celular").val();
    var email = $$("#emaile").val();
    //var fecha = $$("#fecha").val();
    var fecha = $$(".fecha").val();
    var passe = $$("#passe").val();
    var passe2 = $$("#passe2").val();

    /*Validar contraseña*/
    /*var len = $$("#passe").val().length;
    var len2 = $$("#passe2").val().length;
    var lenUser = $$("#user").val().length;
    var lenNombre = $$("#nombre").val().length;
    var lenapellido = $$("#apellido").val().length;
    var lenaTelefono = $$("#telefono").val().length;
    var lenaCelular = $$("#celular").val().length;*/

    /*Validar Correo*/
    var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

    if (user == "" || nombre == "") {
      app.dialog.alert("Completa los campo correctamente.");
      /*}else if( lenUser < 8 ){
        app.dialog.alert('El nombre de usuario debe ser mayor de 8 caracteres.');
      }else if( lenNombre < 3 ){
        app.dialog.alert('Esto no parece ser un nombre valido.');
      }else if( lenapellido < 3 ){
        app.dialog.alert('Esto no parece ser un apellido valido.');
      }else if(isNaN(telefono) || lenaTelefono != 10){
        app.dialog.alert('El número de teléfono debe tener al menos 10 números.');
      }else if(isNaN(celular) || lenaCelular != 10){
        app.dialog.alert('El número de celular debe tener al menos 10 números.');
      }else if(!isEmail(email)){
        app.dialog.alert('Correo electronico invalido.');
      }else if( len < 8 || len2 < 8 ){
        app.dialog.alert('la contraseña debe ser mayor de 8 caracteres.');*/
    } else {
      app.request({
        type: "POST",
        url: "https://laundryappmovil.com/api-nes/public/api/cliente/add",
        data: {
          email: email,
          nombre: nombre,
          apellido: apellido,
          telefono: telefono,
          fecha_nac: fecha,
          usuario: user,
          pass: passe,
          celular: celular,
        },
        beforeSend: function (xhr) {
          app.dialog.preloader("Cargando...");
        },
        success: function (resp) {
          console.log(
            "Esta es la respuesta cuando registro un cliente " + resp
          );
          app.dialog.close();

          $$("#telefono").val("");
          $$("#email").val("");
          $$("#passe").val("");
          $$("#passe2").val("");
          $$("#user").val("");
          $$("#nombre").val("");
          $$("#apellido").val("");
          $$("#celular").val("");

          //Agregar una alerta que al confirmar con un mensaje redireccione
          app.dialog.preloader("Registro exitoso.");
          setTimeout(function () {
            app.dialog.close();
            app.views.main.router.navigate("/login/");
          }, 3000);
        },
        error: function (xhr) {
          app.dialog.close();
          app.dialog.alert("Error");
        },
      });
    }
  });
});
/**************************************************************/
/**************************************************/
/*****************LOGIN PAGE***********************/
/**************************************************/
/**************************************************************/
$$(document).on(
  "page:init",
  '.page[data-name="restaurantes-login"]',
  function (e) {
    console.log("Estoy en el restaurante login");
    var searchbar = app.searchbar.create({
      el: ".searchbar",
      searchContainer: ".list",
      searchIn: ".item-title",
      on: {
        search(sb, query, previousQuery) {
          console.log(query, previousQuery);
        },
      },
    });

    app.request({
      type: "GET",
      dataType: "json",
      url: "https://laundryappmovil.com/api-nes/public/api/restaurantes",
      beforeSend: function (xhr) {
        app.preloader.show();
      },
      success: function (data) {
        app.preloader.hide();
        console.log(data);
        var html = "";
        if (data == "" || data == "[]") {
          console.log("No tiene restaurantes.");
          app.dialog.alert("No tiene restaurantes registrados.");
        } else {
          $.each(data, function (i, item) {
            if (item.logo == null || item.logo == "") {
              var logoResult = "img/tools-and-utensils.png";
            } else {
              var logoResult = item.logo;
            }

            if (item.calificacion == null || item.calificacion == "") {
              var cali = 1;
            } else {
              var cali = item.calificacion;
            }

            if (item.estatus == "Cerrado") {
              html +=
                `<li>
              <a href="#" class="item-link item-content" style="opacity:0.5;" onclick="erroGoMenu()">
              <div class="item-media">
              <img src="https://sys.eatsoonpr.com/` +
                logoResult +
                `" width="80"/>
              </div>
              <div class="item-inner">
              <div class="item-title-row">
              <div class="item-title" style="font-weight: bold">` +
                item.nombre +
                `</div>
              <div class="item-after">
              <img src="img/star.png" style="width: 15px;height: 15px;vertical-align: middle;display: inline-block;padding-right: 5px;padding-top: 2px;">
              <span style="vertical-align: middle;display: inline-block;font-size: 13px">` +
                item.calificacion +
                `</span> 
              </div>
              </div>
              <div class="item-subtitle" style="font-style: italic;color: darkgray;">` +
                item.categoria +
                `</div>
              <div class="item-text">Open: ` +
                formatAMPMe(item.openTime) +
                ` To: ` +
                formatAMPMe(item.closedTime) +
                `</div>
              </div>
              </a>
              </li>`;
            } else {
              html +=
                `<li>
              <a href="#" onclick="goMenuLogin(` +
                item.id +
                `)" class="item-link item-content">
              <div class="item-media">
              <img src="https://sys.eatsoonpr.com/` +
                logoResult +
                `" width="80"/>
              </div>
              <div class="item-inner">
              <div class="item-title-row">
              <div class="item-title" style="font-weight: bold">` +
                item.nombre +
                `</div>
              <div class="item-after">
              <img src="img/star.png" style="width: 15px;height: 15px;vertical-align: middle;display: inline-block;padding-right: 5px;padding-top: 2px;">
              <span style="vertical-align: middle;display: inline-block;font-size: 13px">` +
                cali +
                `</span> 
              </div>
              </div>
              <div class="item-subtitle" style="font-style: italic;color: darkgray;">` +
                item.categoria +
                `</div>
              <div class="item-text">Open: ` +
                formatAMPMe(item.openTime) +
                ` To: ` +
                formatAMPMe(item.closedTime) +
                `</div>
              </div>
              </a>
              </li>`;
            }
            $$("#busquedaRest").html(html);
          });
        }
      },
      error: function () {
        app.preloader.hide();
      },
    });
  }
);

$$(document).on(
  "page:init",
  '.page[data-name="detail-restaurante"]',
  function (e) {
    var formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });
    var idRestauranteMenu = app.view.main.router.currentRoute.params.value;
    app.request({
      type: "GET",
      dataType: "json",
      url: "https://laundryappmovil.com/api-nes/public/api/menu/restaurante/" +
        idRestauranteMenu,
      beforeSend: function (xhr) {
        app.preloader.show();
      },
      success: function (data) {
        app.preloader.hide();
        console.log(data);
        var html = "";
        $.each(data, function (i, item) {
          if (item.nombre == "" || item.descripcion == null) {
            var descrip = "";
          } else {
            var descrip = item.descripcion;
          }

          html +=
            `<li>
        <a href="#" onclick="alerta()" class="item-link item-content sheet-open" data-sheet=".my-sheet-swipe-to-step">
        <div class="item-inner">
        <div class="item-title">
        ` +
            item.nombre +
            `
        <div class="item-footer">` +
            descrip +
            `</div>
        </div>
        <div class="item-after" style="color: darkgreen">US$ ` +
            formatter.format(item.precio).replace("$", "") +
            `</div>
        </div>
        </a>
        </li>`;
          $$("#nombreRestaurante").html(item.nombreRestaurante);
          $$("#menusii").html(html);
        });
      },
      error: function () {
        app.preloader.hide();
      },
    });
  }
);

function alerta() {
  /*$$('.open-confirm').on('click', function () {*/
  app.dialog.confirm("Debe registrarse para ordenar", function () {
    app.views.main.router.navigate("/register/");
  });
  /*});*/
}

$$(document).on("page:init", '.page[data-name="login"]', function (e) {
  console.log("Estoy en el login");

  var _originalSize = $(window).width() + $(window).height();
  $(window).resize(function () {
    if ($(window).width() + $(window).height() != _originalSize) {
      console.log("keyboard show up");
      //$$(".terminos").css("position","relative");
      $$(".terminos").hide();
    } else {
      console.log("keyboard closed");
      //$$(".terminos").css("position","absolute");
      $$(".terminos").show();
    }
  });

  recordarUsuario();
  //VER CONTRASEÑA
  document.querySelector(".verp span").addEventListener("click", (e) => {
    const passwordInput = document.querySelector("#pass");
    if (e.target.classList.contains("show")) {
      e.target.classList.remove("show");
      passwordInput.type = "text";
    } else {
      e.target.classList.add("show");
      passwordInput.type = "password";
    }
  });

  $$("#iniciar-session").on("click", function () {
    console.log("Boton iniciar session presionado.");
    var username = $$("#email").val();
    var password = $$("#pass").val();
    var send = "https://laundryappmovil.com/api-nes/public/api/login";
    var checkBox = document.getElementById("checkUserRec");

    if (checkBox.checked == true) {
      console.log("Esta chequeado");
      window.localStorage.setItem("UserLogin", username);
      window.localStorage.setItem("PasswordLogin", password);
    } else {
      localStorage.removeItem("UserLogin");
      localStorage.removeItem("PasswordLogin");
      console.log("No esta chequeado");
    }

    if (username == "" || password == "") {
      app.dialog
        .create({
          title: "Doctor Click",
          text: "Completa los campos correctamente",
          buttons: [{
            text: "OK",
          }, ],
          verticalButtons: true,
        })
        .open();
    } else {
      app.request({
        type: "POST",
        url: send,
        data: {
          email: username,
          password: password,
        },
        beforeSend: function (xhr) {
          app.dialog.preloader("Iniciando Sesión");
        },
        success: function (data) {
          console.log(data);

          if (data == 1) {
            app.dialog.close();
            app.dialog
              .create({
                title: "Doctor Click",
                text: "contraseña incorrecta.",
                buttons: [{
                  text: "OK"
                }],
                verticalButtons: true,
              })
              .open();
          } else if (data == 2) {
            app.dialog.close();
            app.dialog
              .create({
                title: "Doctor Click",
                text: "Usuario o correo incorrecto.",
                buttons: [{
                  text: "OK"
                }],
                verticalButtons: true,
              })
              .open();
          } else {
            //Extraer los valores del JSON
            var obj = JSON.parse(data);
            console.log(obj.nombre + " " + obj.apellido);
            window.localStorage.setItem("id", obj.id_cliente);
            window.localStorage.setItem("login", 1);
            window.localStorage.setItem("username", username);
            window.localStorage.setItem("password", password);

            //NOTIFICACIONES - REGISTRO DE TOKEN
            /*window.plugins.OneSignal.getIds(function(ids){
               var token = ids.userId;
               $.ajax({
                 url: "https://laundryappmovil.com/api-nes/api/cliente_token.php?token="+token+"&id="+obj.id_cliente,
                 method: 'GET',
                 success:function(resp){
                   console.log(resp);
                 }
               });
               //alert(token);
               console.log(token);
             });*/
            setTimeout(function () {
              app.dialog.close();
              location.href =
                "home.html?id=" +
                obj.id_cliente +
                "&nombre=" +
                obj.nombre +
                "&apellido=" +
                obj.apellido +
                "&usuario=" +
                obj.usuario +
                "&password=" +
                password +
                "&celular=" +
                obj.celular +
                "&telefono=" +
                obj.telefono +
                "&correo=" +
                obj.email + "&formulario=" + obj.formulario;
            }, 2000);
          }
        },
        error: function (xhr) {
          app.dialog.close();
          console.log("Hay bobo aqui");
          app.dialog
            .create({
              title: "Doctor Click",
              text: "No tiene acceso a internet.",
              buttons: [{
                text: "OK"
              }],
              verticalButtons: true,
            })
            .open();
        },
      });
    }
  });
});
/**************************************************************/
/**************************************************************/
/**************************************************************/
/**************************************************************/
/**************************************************************/
function counNoty() {
  var id = getParameterByName("id");
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/api/countCarrito.php?id=" + id,
    success: function (data) {
      //console.log('Info car '+data);
      $$(".counNoty").html(data);
    },
    error: function (xhr) {
      console.log(xhr);
    },
  });
}

/**************************************************************/
/**************************************************************/
/*****************HOME PAGE - PAGINA DE INCIO******************/
/**************************************************************/
/**************************************************************/
$$(document).on("page:init", '.page[data-name="home"]', function (e) {
  console.log("[Doctor Click] estoy en el HomePage");
  slideHome(); // Slide del home imagenes.
  tiposcentros(); // Centros populares
  especialidades();
  initMap();
  citas();

  var searchbar = app.searchbar.create({
    el: ".busquedCentros",
    searchContainer: ".list",
    searchIn: ".item-title",
    on: {
      search(sb, query, previousQuery) {
        console.log(query, previousQuery);
      },
    },
  });

  // FECHA ACTUAL
  var nowDate = new Date();
  var options = {
    month: "long",
    day: "numeric"
  };
  var nowt = nowDate.toLocaleDateString("es-EN", options);
  //Dia actual
  var optionsDay = {
    weekday: "long"
  };
  var today = nowDate.toLocaleDateString("es-EN", optionsDay);
  $$(".todayi").html(
    '<span style="text-transform: capitalize;font-size:20px;"><strong>' +
    today +
    "</strong></span><br>" +
    nowt +
    ""
  );

  var nombre = getParameterByName("nombre");
  var apellido = getParameterByName("apellido");
  var cedula = getParameterByName("cedula");
  var telefono = getParameterByName("telefono");
  var celular = getParameterByName("celular");
  var correo = getParameterByName("correo");

  $$("#nombreUser").html(nombre + " " + apellido);
  $$(".nameUser").html(nombre + " " + apellido);

  var res = celular.substr(0, 3);
  var res1 = celular.substr(3, 3);
  var res2 = celular.substr(6, 6);
  $$(".TelefonoProfilePage").html("(" + res + ") " + res1 + "-" + res2);
  $$(".UsuarioProfilePage").html(nombre + " " + apellido);

  $$(".especilistasLink").on("click", function () {
    app.views.main.router.navigate("/busqueda/");
  });

  /*$$('.goHospitalProfile').on('click', function(){
      app.views.main.router.navigate('/hospital-profile/');
  });*/

  $$(".vamosaNoty").on("click", function () {
    app.views.main.router.navigate("/notificaciones/");
  });

  $$(".elegirDependienteCita").on("click", function () {
    abrirPopup.open();
    klkkaren();
  });

  var id = getParameterByName("id");
  traerFoto(id, 0);
});

function saludo() {
  var html = 0;
}

/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
var infoWindow = null;
var map = null;
var markersArray = [];
var pathArray = [];

function initMap(lat = 18.4800295, long = -70.0169205) {
  var myLatlng = new google.maps.LatLng(lat, long);
  var myOptions = {
    zoom: 18,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    maxZoom: 18
  };

  map = new google.maps.Map(document.getElementById("mapi"), myOptions);
  geocoder = new google.maps.Geocoder();
  placesService = new google.maps.places.PlacesService(map);

  marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29),
    position: myLatlng
  });

  infoWindow = new google.maps.InfoWindow();
  updateMaps();
}

function clearOverlays() {
  for (var i = 0; i < markersArray.length; i++) {
    markersArray[i].setMap(null);
  }
}

function clearpath() {
  for (var i = 0; i < pathArray.length; i++) {
    pathArray[i].setMap(null);
  }
}
///////////////////////////AQUI ES QUE ESTA LA GRASA/////////////////////////////////////////
function updateMaps() {
  clearOverlays();
  clearpath();

  var timestamp = new Date().getTime();
  var url =
    "https://laundryappmovil.com/api-nes/src/routes/markertClick.php?t=" +
    timestamp;

  $.ajax({
    url: url,
    method: "GET",
    success: function (data) {
      Object.getOwnPropertyNames(data).forEach(function (val, idx, array) {
        console.log(val + " -> " + data[val]);
      });
      console.log("datos: " + JSON.stringify(data));
      $(data)
        .find("marker")
        .each(function () {
          var marker = $(this); ///'/'
          var ide = marker.attr("ide");
          var tipo = marker.attr("tipo_centro");
          var name = marker.attr("centro");
          var lat = marker.attr("lat");
          var lng = marker.attr("lng");
          var direccion = marker.attr("direccion");
          var telefono = marker.attr("telefono");
          var latlng = new google.maps.LatLng(
            parseFloat(marker.attr("lat")),
            parseFloat(marker.attr("lng"))
          );
          var html =
            `<strong>Centro :</strong> ` +
            name +
            `<br> 
            <strong>Telefono:</strong> ` +
            telefono +
            `<br> 
            <strong>Direccion:</strong> ` +
            direccion +
            `<br> 
            <a href="/hospital-profile/` +
            ide +
            "/" +
            tipo +
            `">Agendar cita</a>`;

          console.log("direccion:" + latlng);
          console.log("ide:" + ide);
          var customerSet = new google.maps.LatLng(lat, lng);
          img = "img/hospital.png";
          imgCustomer = "img/hospital.png";

          var icone = {
            url: img,
            scaledSize: new google.maps.Size(80, 80),
          };

          var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            center: latlng,
            icon: icone,
          });

          google.maps.event.addListener(marker, "click", function () {
            infoWindow.setContent(html);

            map.setCenter(marker.getPosition());
            infoWindow.open(map, marker);
            map.setZoom(18);
          });

          markersArray.push(marker);
          //markersArray.push(markerCustomer);
        });
    },
  });
}

/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/

$$(document).on(
  "page:init",
  '.page[data-name="historial-ordenes"]',
  function (e) {
    console.log("[Pronto2GO] pagina de historial de Ordenes.");
    var calendarRange = app.calendar.create({
      inputEl: "#demo-calendar-range",
      rangePicker: true,
    });
  }
);

/* Pagina de busqueda de restaurantes */
$$(document).on("page:init", '.page[data-name="busqueda"]', function (e) {
  var searchbar = app.searchbar.create({
    el: ".searchbar",
    searchContainer: ".list",
    searchIn: ".item-title",
    on: {
      search(sb, query, previousQuery) {
        console.log(query, previousQuery);
      },
    },
  });

  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/restaurantes",
    beforeSend: function (xhr) {
      app.preloader.show();
    },
    success: function (data) {
      app.preloader.hide();
      console.log(data);
      var html = "";
      if (data == "" || data == "[]") {
        console.log("No tiene restaurantes.");
        app.dialog.alert("No tiene restaurantes registrados.");
      } else {
        $.each(data, function (i, item) {
          if (item.logo == null || item.logo == "") {
            var logoResult = "img/tools-and-utensils.png";
          } else {
            var logoResult = item.logo;
          }

          if (item.calificacion == null || item.calificacion == "") {
            var cali = 1;
          } else {
            var cali = item.calificacion;
          }

          if (item.estatus == "Cerrado") {
            html +=
              `<li>
              <a href="#" class="item-link item-content" style="opacity:0.5;" onclick="erroGoMenu()">
              <div class="item-media">
              <img src="https://sys.eatsoonpr.com/` +
              logoResult +
              `" width="80"/>
              </div>
              <div class="item-inner">
              <div class="item-title-row">
              <div class="item-title" style="font-weight: bold">` +
              item.nombre +
              `</div>
              <div class="item-after">
              <img src="img/star.png" style="width: 15px;height: 15px;vertical-align: middle;display: inline-block;padding-right: 5px;padding-top: 2px;">
              <span style="vertical-align: middle;display: inline-block;font-size: 13px">` +
              item.calificacion +
              `</span> 
              </div>
              </div>
              <div class="item-subtitle" style="font-style: italic;color: darkgray;">` +
              item.categoria +
              `</div>
              <div class="item-text">Open: ` +
              formatAMPMe(item.openTime) +
              ` To: ` +
              formatAMPMe(item.closedTime) +
              `</div>
              </div>
              </a>
              </li>`;
          } else {
            html +=
              `<li>
              <a href="#" onclick="goMenu(` +
              item.id +
              `,` +
              item.cerrado +
              `)" class="item-link item-content">
              <div class="item-media">
              <img src="https://sys.eatsoonpr.com/` +
              logoResult +
              `" width="80"/>
              </div>
              <div class="item-inner">
              <div class="item-title-row">
              <div class="item-title" style="font-weight: bold">` +
              item.nombre +
              `</div>
              <div class="item-after">
              <img src="img/star.png" style="width: 15px;height: 15px;vertical-align: middle;display: inline-block;padding-right: 5px;padding-top: 2px;">
              <span style="vertical-align: middle;display: inline-block;font-size: 13px">` +
              cali +
              `</span> 
              </div>
              </div>
              <div class="item-subtitle" style="font-style: italic;color: darkgray;">` +
              item.categoria +
              `</div>
              <div class="item-text">Open: ` +
              formatAMPMe(item.openTime) +
              ` To: ` +
              formatAMPMe(item.closedTime) +
              `</div>
              </div>
              </a>
              </li>`;
          }
          $$("#busquedaEspecialista").html(html);
        });
      }
    },
    error: function () {
      app.preloader.hide();
    },
  });
});

/* Pagina de hospital-profile */
$$(document).on("page:init", '.page[data-name="hospital-profile"]', function (e) {
  console.log("[DC | Hospital Profile]");
  especialidadesDrop();
  var idCent = app.view.main.router.currentRoute.params.id;
  var tipeCent = app.view.main.router.currentRoute.params.idTypeCent;
  let idDependiente = app.view.main.router.currentRoute.params.idDependiente;
  let titular = app.view.main.router.currentRoute.params.titular;
  let lat = app.view.main.router.currentRoute.params.lat;
  let long = app.view.main.router.currentRoute.params.long;
  initMap(lat, long);
  console.log("idDependiente: " + idDependiente + ", titular: " + titular + ", idCent: " + idCent + ", tipeCent: " + tipeCent + ", lat: " + lat + ", long: " + long);
  $$("#idTypeCent").val(tipeCent);

  $$("#ListEspecialidad").on("change", function (e) {
    console.log("Especialidad: " + e.target.value);
    $$("#ListEspecialista").val("");
    verEspecialista(e.target.value);
  });

  $$("#ListEspecialista").on("change", function (e) {
    console.log("Especialista: " + e.target.value);
  });

  $$("#BackCentrList").on("click", function () {
    app.views.main.router.navigate("/centros/" + tipeCent);
  });

  profileCentro(idCent);

  let smartselect = $$(".smart-select");

  $$("#SendSolid").on("click", function () {
    // console.log("Enviar Solicitud: " + doctor);
    let dependiente = (titular == 1 ? 0 : idDependiente);
    console.log('dependiente al enviar: ' + dependiente);

    var fecha = $$("#fechaResr").val();
    var hora = $$("#horaResr").val();
    var especialista = $$("#ListEspecialista").val();
    console.log('especialista al enviar: ' + especialista);

    if (fecha == "" || hora == "" || especialista == "") {
      app.dialog.alert("Por favor rellenar todos los campos.");
    } else {
      sendSolid(fecha, hora, idCent, especialista, dependiente);
      // app.request({
      //   type: "GET",
      //   url: "https://laundryappmovil.com/api-nes/public/api/doctorNombre/" + especialista,
      //   dataType: "json",
      //   beforeSend: function (xhr) {
      //     console.log('Buscando...');
      //   },
      //   success: function (data) {
      //       console.log("data: " + data);
      //     $.each(data, function (i, item) {
      //     });
      //   },
      // });
    }
  });
});

const sendSolid = (fecha, hora, idCent, especialista, dependiente) => {
  app.request({
    type: "POST",
    url: "https://laundryappmovil.com/api-nes/public/api/citas/add/",
    data: {
      paciente: id,
      fecha: fecha,
      hora: hora,
      centro: idCent,
      especialista: especialista,
      dependiente: dependiente,
    },
    beforeSend: function (xhr) {
      app.dialog.preloader("Creando solicitud");
    },
    success: function (data) {
      app.dialog.close();
      console.log(data);
      app.dialog.alert("Solicitud Enviada.");
      setTimeout(function () {
        app.views.main.router.navigate("/");
      }, 2000);
    },
    error: function (xhr) {
      console.log(xhr);
      app.dialog.close();
    },
  });
}

$$(document).on("page:init", '.page[data-name="citas"]', function (e) {
  citas();
  // citasSolicitud();
  // citasRechazadas();
});

$$(document).on("page:init", '.page[data-name="accioncita"]', function (e) {
  citas();
});

$$(document).on("page:init", '.page[data-name="detalle-seguro"]', function (e) {
  var seguro = app.view.main.router.currentRoute.params.idseguro;
  console.log("seguro: " + seguro);
  consultDetalleSeguroM(seguro);
});

$$(document).on("page:init", '.page[data-name="seguro-medico"]', function (e) {
  console.log("estoy en la pagina de seguros medicos");
  let dependiente = app.view.main.router.currentRoute.params.idDependiente;
  let titular = app.view.main.router.currentRoute.params.titular;
  console.log("dependiente: " + dependiente);
  consultSeguroM(dependiente, titular);
});

$$(document).on("page:init", '.page[data-name="add_seguro"]', function (e) {
  console.log("estoy en la pagina de crear seguros medicos");

  app.request({
    type: "GET",
    url: "https://laundryappmovil.com/api-nes/public/api/seguroMedico",
    dataType: "json",
    beforeSend: function(xhr){},
    success: function(data){
      let html = "";
      console.log('seguros medicos: ' + data);
      $.each(data, function(i, item){
        html += `<option value="`+ item.seguro +`">`+ item.seguro +`</option>`;
      });
      $$("#addSeguroMedico").html(html);
    }, error: function(xhr){
      console.log('error: ' + xhr);
    }
  });

  $$("#tomarSeguro").on('click', function(){
    capturePhoto1();
  });

  $$("#tomarCedula").on('click', function(){
    capturePhoto2();
  });

  $$("#crearSeguro").on("click", function () {
    console.log("boton presionado");
    let imagenSeguro = document.getElementById("seguroFoto").src;
    let imagenCedula = document.getElementById("cedulaFoto").src;
    var idCliente = getParameterByName("id");
    var seguro = $$("#addSeguroMedico").val();
    var tipo = $$(".tipo").val();
    var fecha_venc = $$(".fecha_venc").val();
    var fecha_emis = $$(".fecha_emis").val();
    var nombre = $$(".nombre").val();
    var apellido = $$(".apellido").val();
    var noAfiliado = $$(".noAfiliado").val();
    var nss = $$(".nss").val();
    var contracto = $$(".contracto").val();
    var nacimiento = $$(".nacimiento").val();

    if (seguro == "null" || seguro == "" || tipo == "" || nacimiento == "" || fecha_venc == "" || fecha_emis == "" || nombre == "" || apellido == "" || noAfiliado == "" || nss == "") {
      app.dialog.alert("Completa los campos correctamente");
    } else {
      app.request({
        type: "POST",
        dataType: "json",
        url: "https://laundryappmovil.com/api-nes/public/api/seguroM/add",
        data: {
          seguro: seguro,
          tipo: tipo,
          fecha_venc: fecha_venc,
          fecha_emis: fecha_emis,
          id_cliente: idCliente,
          nombre: nombre,
          apellido: apellido,
          noAfiliado: noAfiliado,
          nacimiento: nacimiento,
          nss: nss,
          contracto: contracto,
          seguroimg: imagenSeguro,
          cedulaimg: imagenCedula
        },
        beforeSend: function (xhr) {
          app.preloader.show();
        },
        success: function (data) {
          console.log(data);
          app.preloader.hide();
          app.dialog.alert("Seguro agregado con exito");
          setTimeout(function () {
            app.views.main.router.navigate("/");
          }, 2000);
        },
        error: function (xhr) {
          console.log(xhr);
        },
      });
    }
  });
});

$$(document).on("page:init", '.page[data-name="visita-medica"]', function (e) {
  idDependiente = app.view.main.router.currentRoute.params.idDependiente;
  titular = app.view.main.router.currentRoute.params.titular;
  console.log("dependiente: " + idDependiente + " titular: " + titular);
  especialidadesDrop();
  var id = getParameterByName("id");

  $$("#especialistaEspec").on("change", function () {
    let este = $(this).val();
    app.request({
      type: "GET",
      dataType: "json",
      url: "https://laundryappmovil.com/api-nes/public/api/especialista/" + este,
      success: function (data) {
        var html = "";
        $.each(data, function (i, item) {
          html +=
            `<option value="` +
            item.id +
            `">` +
            item.especialista +
            ` (` +
            item.especialidades +
            `)</option>`;
        });
        $$("#especialista").html(html);
      },
      error: function (xhr) {
        console.log(xhr);
      },
    });
  });
  var id, nombre, apellido;
  // app.request({
  //   type: "GET",
  //   dataType: "json",
  //   url: "https://laundryappmovil.com/api-nes/public/api/dependientes",
  //   success: function (data) {
  //     $.each(data, function (i, item) {
  //       id = item.id;
  //       nombre = item.nombre;
  //       apellido = item.apellido;
  //     });
  //   }, error: function (xhr) {
  //     console.log(xhr);
  //   }
  // });

  $$("#listadoDependientes").on("click", function () {
    verdependiente.open();
  });
  console.log("nombre: " + nombre);

  var verdependiente = app.actions.create({
    grid: true,
    buttons: [
      [{
        text: nombre + " " + apellido,
        icon: '<img src="img/img_avatar.png" width="48"/>',
        onClick: function () {},
      }, ],
    ],
    on: {
      opened: function () {
        app.request({
          type: "GET",
          dataType: "json",
          url: "https://laundryappmovil.com/api-nes/public/api/dependientes/" + id,
          success: function (data) {
            $.each(data, function (i, item) {
              console.log(item.id);
            });
          },
        });
      },
    },
  });

  $$(".asesoria").on("click", function () {
    $$(".linkede").hide();
  });

  $$(".asesoria2").on("click", function () {
    $$(".linkede").show();
  });

  $$("#listEntrega").on("click", function () {
    direccionEntregaFunct.open();
  });

  var direccionEntregaFunct = app.actions.create({
    grid: true,
    buttons: [
      [{
          text: "Casa",
          icon: '<img src="img/house.png" width="48"/>',
          onClick: function () {
            app.request({
              type: "GET",
              dataType: "json",
              url: "https://laundryappmovil.com/api-nes/public/api/direcciones/" +
                id +
                "/hogar",
              beforeSend: function (xhr) {
                app.preloader.show();
              },
              success: function (data) {
                app.preloader.hide();
                console.log(data);
                if (data == "" || data == "[]") {
                  console.log("No tiene una dirección particular registrada");
                  app.dialog.alert(
                    "No tiene una dirección particular registrada"
                  );
                  $$("#optionAdress").show(1000);
                } else {
                  $.each(data, function (i, item) {
                    $$("#direccion").val(item.id);
                    $$("#direccionEntrega").html(
                      item.calle + " (" + item.tipo + ")"
                    );
                    //delivery(item.cod_postal, idRestaurante);
                  });
                }
              },
              error: function (xhr) {
                app.preloader.hide();
                app.dialog.alert(
                  "No tiene una dirección particular registrada"
                );
              },
            });
          },
        },
        {
          text: "Oficina",
          icon: '<img src="img/building.png" width="48">',
          onClick: function () {
            app.request({
              type: "GET",
              dataType: "json",
              url: "https://laundryappmovil.com/api-nes/public/api/direcciones/" +
                id +
                "/Oficina",
              beforeSend: function (xhr) {
                app.preloader.show();
              },
              success: function (data) {
                app.preloader.hide();
                console.log(data);
                if (data == "" || data == "[]") {
                  console.log("No tiene una dirección de oficina registrada");
                  app.dialog.alert(
                    "No tiene una dirección de oficina registrada"
                  );
                  $$("#optionAdress").show(1000);
                } else {
                  $.each(data, function (i, item) {
                    $$("#direccion").val(item.id);
                    $$("#direccionEntrega").html(
                      item.calle + " (" + item.tipo + ")"
                    );
                    //delivery(item.cod_postal, idRestaurante);
                  });
                }
              },
              error: function (xhr) {
                app.preloader.hide();
                app.dialog.alert(
                  "No tiene una dirección de oficina registrada"
                );
              },
            });
          },
        },
        {
          text: "Otro",
          icon: '<img src="img/placeholder.png" width="48">',
          onClick: function () {
            app.request({
              type: "GET",
              dataType: "json",
              url: "https://laundryappmovil.com/api-nes/public/api/direcciones/" +
                id +
                "/Otros",
              beforeSend: function (xhr) {
                app.preloader.show();
              },
              success: function (data) {
                app.preloader.hide();
                console.log(data);
                if (data == "" || data == "[]") {
                  console.log("No tiene otra dirección registrada");
                  app.dialog.alert("No tiene otra dirección registrada");
                  $$("#optionAdress").show(1000);
                  eso;
                } else {
                  $.each(data, function (i, item) {
                    $$("#direccion").val(item.id);
                    $$("#direccionEntrega").html(
                      item.calle + " (" + item.tipo + ")"
                    );
                    //delivery(item.cod_postal, idRestaurante);
                  });
                }
              },
              error: function (xhr) {
                app.preloader.hide();
                app.dialog.alert("No tiene otra dirección registrada");
              },
            });
          },
        },
        {
          text: "Ubicación actual",
          icon: '<img src="img/place.png" width="48">',
          onClick: function () {
            $$("#detalleAdreesCurrent").val();
            initMapDetalle();
          },
        },
        {
          text: "Añadir dirección",
          icon: '<img src="img/add-location.png" width="48">',
          onClick: function () {
            let popupAddress =
              document.getElementsByClassName("OpenTriggerAdress");
            $$(".OpenTriggerAdress").click();
          },
        },
      ],
    ],
  });

  $$("#SendSolidVisita").on("click", function () {
    let paciente;
    if (titular == 1) {
      paciente = 0;
    } else {
      paciente = idDependiente;
    }
    console.log("boton enviar solicitud presionado...");
    var id = getParameterByName("id");
    var fecha = $$("#fechaResr").val();
    var hora = $$("#horaResr").val();
    var dependiente = $$("#dependiente").val();
    var especialista = $$("#especialista").val();
    var direccion = $$("#direccion").val();

    if (
      fecha == "" ||
      hora == "" ||
      dependiente == "" ||
      especialista == "" ||
      direccion == ""
    ) {
      app.dialog.alert("Por favor rellenar todos los campos.");
    } else {
      console.log("ide: " + direccion);

      app.request({
        type: "POST",
        url: "https://laundryappmovil.com/api-nes/public/api/citas/add/",
        data: {
          paciente: id,
          fecha: fecha,
          hora: hora,
          //"centro": idCent,
          dependiente: paciente,
          especialista: especialista,
          direccion: direccion,
        },
        beforeSend: function (xhr) {
          app.dialog.preloader("Creando solicitud");
        },
        success: function (data) {
          app.dialog.close();
          console.log(data);
          app.dialog.alert("Solicitud Enviada.");
          setTimeout(function () {
            app.views.main.router.navigate("/");
          }, 2000);
        },
        error: function (xhr) {
          console.log(xhr);
          app.dialog.close();
        },
      });
    }
  });
});

//////////////////////////////////Vista de dependientes//////////////////////////////////////
$$("#listadoDependientes").on("click", function () {
  listDependientes.open();
});

var listDependientes = app.actions.create({
  grid: true,
  buttons: [
    app.request({
      type: "GET",
      url: "http://laundryappmovil.com/api-nes/public/api/dependientes/" + id,
      dataType: "json",
      beforeSend: function (xhr) {},
      success: function (data) {
        console.log("data: " + data);
        var html = "";
        $.each(data, function (i, item) {
          [{
            text: item.nombre + " " + item.apellido,
            icon: '<img src="img/iconos/usuario.png" width="48"/>',
            onClick: function () {
              $$("#dependiente").text(item.nombre + " " + item.apellido);

              /*app.request({
                type: "GET",
                dataType: "json",
                url: "https://laundryappmovil.com/api-nes/public/api/direcciones/" + id + "/hogar",
                beforeSend: function (xhr) {
                  app.preloader.show();
                },
                success: function (data) {
                  app.preloader.hide();
                  console.log(data);
                  if (data == "" || data == "[]") {
                    console.log('No tiene una dirección particular registrada');
                    app.dialog.alert('No tiene una dirección particular registrada');
                    $$('#optionAdress').show(1000);
                  } else {
                    $.each(data, function (i, item) {
                      $$('#direccion').val(item.id);
                      $$('#direccionEntrega').html(item.calle + ' (' + item.tipo + ')');
                      //delivery(item.cod_postal, idRestaurante);
                    });
                  }

                }, error: function (xhr) {
                  app.preloader.hide();
                  app.dialog.alert('No tiene una dirección particular registrada');
                }
              });*/
            },
          }, ];
        });
      },
    }),
  ],
});

/////////////////////////Fin de vista de dependientes////////////////////////////////

$$(document).on(
  "page:init",
  '.page[data-name="centros-elegir-profile"]',
  function (e) {
    console.log("[Doctor Click] Pagina del centros-elegir-profile");
    idDependiente = app.view.main.router.currentRoute.params.idDependiente;
    titular = app.view.main.router.currentRoute.params.titular;
    console.log("dependiente: " + idDependiente + " titular: " + titular);
    especialidadesDrop();

    $$("#especialistaEspec").on("change", function () {
      let este = $(this).val();
      app.request({
        type: "GET",
        dataType: "json",
        url: "https://laundryappmovil.com/api-nes/public/api/especialista/" + este,
        success: function (data) {
          var html = "";
          $.each(data, function (i, item) {
            html +=
              `<option value="` +
              item.id_doctor +
              `">` +
              item.especialista +
              ` (` +
              item.especialidades +
              `)</option>`;
          });
          $$("#especialista").html(html);
        },
        error: function (xhr) {
          console.log(xhr);
        },
      });
    });

    app.request({
      type: "GET",
      dataType: "json",
      url: "https://laundryappmovil.com/api-nes/public/api/dependientes",
      success: function (data) {
        var html = "";
        $.each(data, function (i, item) {
          html +=
            `<option value="` +
            item.id +
            `">` +
            item.nombre +
            ` ` +
            item.apellido +
            `</option>`;
        });
        $$("#dependiente").html(html);
      },
      error: function (xhr) {
        console.log(xhr);
      },
    });

    var typeSend = app.view.main.router.currentRoute.params.value;
    var nameType;

    if (typeSend == 1) {
      nameType = "Online";
    } else if (typeSend == 2) {
      nameType = "Visita medica y Psicologica (en casa)";
    } else if (typeSend == 3) {
      nameType = "Centros Odontológicos";

      $$(".selecEspecialidad").hide();
      var este = 2;
      app.request({
        type: "GET",
        dataType: "json",
        url: "https://laundryappmovil.com/api-nes/public/api/especialista/" + este,
        success: function (data) {
          var html = "";
          $.each(data, function (i, item) {
            html +=
              `<option value="` +
              item.id_doctor +
              `">` +
              item.especialista +
              `</option>`;
          });
          $$("#especialista").html(html);
        },
        error: function (xhr) {
          console.log(xhr);
        },
      });
    } else if (typeSend == 4) {
      nameType = "Laboratorios";
    }

    $$(".NameCentSelecty").html(nameType);

    $$(".asesoria").on("click", function () {
      $$(".linkede").hide();
    });

    $$(".asesoria2").on("click", function () {
      $$(".linkede").show();
    });

    $$("#SendSolidOnline").on("click", function () {
      let paciente;
      if (titular == 1) {
        paciente = 0;
      } else {
        paciente = idDependiente;
      }
      console.log("boton enviar solicitud presionado...");
      var id = getParameterByName("id");
      var fecha = $$("#fechaResr").val();
      var hora = $$("#horaResr").val();
      var dependiente = $$("#dependiente").val();
      var especialista = $$("#especialista").val();

      if (
        fecha == "" ||
        hora == "" ||
        dependiente == "" ||
        especialista == ""
      ) {
        app.dialog.alert("Por favor rellenar todos los campos.");
      } else {
        console.log("ide: " + dependiente);
        console.log("id: " + especialista);

        app.request({
          type: "POST",
          url: "https://laundryappmovil.com/api-nes/public/api/citas/add/",
          data: {
            paciente: id,
            fecha: fecha,
            hora: hora,
            //"centro": idCent,
            dependiente: paciente,
            especialista: especialista,
          },
          beforeSend: function (xhr) {
            app.dialog.preloader("Creando solicitud");
          },
          success: function (data) {
            app.dialog.close();
            console.log(data);
            app.dialog.alert("Solicitud Enviada.");
            setTimeout(function () {
              app.views.main.router.navigate("/");
            }, 2000);
          },
          error: function (xhr) {
            console.log(xhr);
            app.dialog.close();
          },
        });
      }
    });
  }
);

/* Pagina de Add Direccion*/
$$(document).on("page:init", '.page[data-name="direccion"]', function (e) {
  var past = app.view.main.router.currentRoute.params.value;
  if (past == 2) {
    $$("#otraRedi").show();
    $$("#normalRedi").hide();
  } else {
    $$("#normalRedi").show();
    $$("#otraRedi").hide();
  }
  direcciones();

  $$(".add_direccion").on("click", function () {
    app.views.main.router.navigate("/add_direccion/");
  });
});

$$(document).on("page:init", '.page[data-name="servicio"]', function (e) {
  $$(".toolbar-inner .weo").click(function () {
    $$(".weo").removeClass("activess");
    $$(this).addClass("activess");
  });

  //Parametro que define el tipo de servicio lavado y planchado y solo planchado
  $$("#idService").val(app.view.main.router.currentRoute.params.value);
  var tipo = app.view.main.router.currentRoute.params.value;

  $$("#goCart").on("click", function () {
    app.views.main.router.navigate("/carrito/2/" + tipo);
  });

  //Cambio del Titulo del page de servicio
  ////Archivo con la informacion de servicio
  if (tipo == 1) {
    $$("#tituloService").html("Lavado y planchado");
    $.getScript("js/servicioLavPlanchado.js"); //precios y cantidades
  } else {
    $$("#tituloService").html("Solo planchado");
    $.getScript("js/servicioPlanchado.js"); //precios y cantidades
  }

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  //BOTON O ACCION DE ADD AL CARRITO.
  $$("#addCarrito").on("click", function () {
    //Variables
    var id = getParameterByName("id"); //ID del cliente.
    var idService = $$("#idService").val(); // ID del servicio
    // Ropa de Hombres
    var inputCamisa = $$("#inputCamisaPlus").val();
    var inputPatalon = $$("#inputPantalonPlus").val();
    var inputSaco = $$("#inputSacoPlus").val();
    var inputTraje = $$("#inputTrajePlus").val();
    var inputCorbata = $$("#inputCorbataPlus").val();
    var inputChacabana = $$("#inputChacabanaPlus").val();
    // Ropa de Mujeres
    var inputFalda = $$("#inputFaldaPlus").val();
    var inputFaldaLarga = $$("#inputFaldaLargaPlus").val();
    var inputVestidoCorto = $$("#inputVestidoCortoPlus").val();
    var inputVestidoLargo = $$("#inputVestidoLargoPlus").val();
    var inputChaquetaMujer = $$("#inputChaquetaMujerPlus").val();
    var inputBlusaMujer = $$("#inputBlusaMujerPlus").val();
    var inputPantalonMujer = $$("#inputPantalonMujerPlus").val();
    var inputPiezasMujer = $$("#inputPiezasMujerPlus").val();
    // Total
    var totalArtInput = $$("#totalArtInput").val();

    if (totalArtInput == "RD$ 0.00" || totalArtInput == "") {
      app.dialog.alert("por favor selccionar un articulo");
    } else {
      app.request({
        type: "POST",
        url: "https://laundryappmovil.com/api-nes/api/carritoAdd.php",
        data: {
          id: id,
          idService: idService,
          //Ropa de Hombre
          inputCamisaPlus: inputCamisa,
          inputPantalonPlus: inputPatalon,
          inputSacoPlus: inputSaco,
          inputTrajePlus: inputTraje,
          inputCorbataPlus: inputCorbata,
          inputChacabanaPlus: inputChacabana,
          // Ropa de mujer
          inputFaldaPlus: inputFalda,
          inputFaldaLargaPlus: inputFaldaLarga,
          inputVestidoCortoPlus: inputVestidoCorto,
          inputVestidoLargoPlus: inputVestidoLargo,
          inputChaquetaMujerPlus: inputChaquetaMujer,
          inputBlusaMujerPlus: inputBlusaMujer,
          inputPantalonMujerPlus: inputPantalonMujer,
          inputPiezasMujerPlus: inputPiezasMujer,
        },
        beforeSend: function (xhr) {
          app.dialog.preloader("Añadiendo al carrito...");
        },
        success: function (data) {
          app.dialog.close();
          console.log(data);

          app.dialog
            .create({
              title: "Doctor Click",
              text: "Articulos guardados con exito",
              buttons: [{
                text: "OK",
              }, ],
              verticalButtons: true,
            })
            .open();
        },
      });
    }
  });
}); //CIERRE DE LA PAGINA DE SERVICIO

/* Pagina de busqueda por Categoria*/
$$(document).on(
  "page:init",
  '.page[data-name="busquedaCategoria"]',
  function (e) {
    var categoria = app.view.main.router.currentRoute.params.value;

    app.request({
      type: "GET",
      dataType: "json",
      url: "https://laundryappmovil.com/api-nes/public/api/restaurantes/categoria/" +
        categoria,
      beforeSend: function (xhr) {
        app.preloader.show();
      },
      success: function (data) {
        app.preloader.hide();
        console.log(data);
        var html = "";
        if (data == "" || data == "[]") {
          console.log("Esta categoria no tiene restaurantes.");
          app.dialog.alert("Esta categoria no tiene restaurantes registrados.");
          $$("#busquedaCategorias").html(`
            <li>
              <center>
              <img src="img/unnamed.png" style="width: 70%;">
              </center>
            </li>`);
        } else {
          $.each(data, function (i, item) {
            if (item.logo == null || item.logo == "") {
              var logoResult = "img/tools-and-utensils.png";
            } else {
              var logoResult = item.logo;
            }

            if (item.calificacion == null || item.calificacion == "") {
              var calis = 1;
            } else {
              var calis = item.calificacion;
            }

            if (item.estatus == "Cerrado") {
              html +=
                `<li>
              <a href="#" class="item-link item-content" style="opacity:0.5;" onclick="erroGoMenu()">
              <div class="item-media">
              <img src="https://sys.eatsoonpr.com/` +
                logoResult +
                `" width="80"/>
              </div>
              <div class="item-inner">
              <div class="item-title-row">
              <div class="item-title" style="font-weight: bold">` +
                item.nombre +
                `</div>
              <div class="item-after">
              <img src="img/star.png" style="width: 15px;height: 15px;vertical-align: middle;display: inline-block;padding-right: 5px;padding-top: 2px;">
              <span style="vertical-align: middle;display: inline-block;font-size: 13px">` +
                item.calificacion +
                `</span> 
              </div>
              </div>
              <div class="item-subtitle" style="font-style: italic;color: darkgray;">` +
                item.categoria +
                `</div>
              <div class="item-text">Open: ` +
                formatAMPMe(item.openTime) +
                ` To: ` +
                formatAMPMe(item.closedTime) +
                `</div>
              </div>
              </a>
              </li>`;
            } else {
              html +=
                `<li>
              <a href="#" onclick="goMenu(` +
                item.id +
                `,` +
                item.cerrado +
                `)" class="item-link item-content">
              <div class="item-media">
              <img src="https://sys.eatsoonpr.com/` +
                logoResult +
                `" width="80"/>
              </div>
              <div class="item-inner">
              <div class="item-title-row">
              <div class="item-title" style="font-weight: bold">` +
                item.nombre +
                `</div>
              <div class="item-after">
              <img src="img/star.png" style="width: 15px;height: 15px;vertical-align: middle;display: inline-block;padding-right: 5px;padding-top: 2px;">
              <span style="vertical-align: middle;display: inline-block;font-size: 13px">` +
                calis +
                `</span> 
              </div>
              </div>
              <div class="item-subtitle" style="font-style: italic;color: darkgray;">` +
                item.categoria +
                `</div>
              <div class="item-text">Open: ` +
                formatAMPMe(item.openTime) +
                ` To: ` +
                formatAMPMe(item.closedTime) +
                `</div>
              </div>
              </a>
              </li>`;
            }
            $$("#busquedaCategorias").html(html);
          });
        }
      },
      error: function () {
        app.preloader.hide();
      },
    });
  }
);

/* Pagina de Carrito*/
$$(document).on("page:init", '.page[data-name="carrito"]', function (e) {
  console.log("Estoy en el carrito.");
  //limpiarCarritoError();
  carrito();

  // ID del restaurante.
  var tipo = app.view.main.router.currentRoute.params.tipo;
  $$("#goRest").on("click", function () {
    app.views.main.router.navigate("/restaurantes/" + tipo);
  });

  // Para saber si viene del home o de servicio.
  var past = app.view.main.router.currentRoute.params.value;
  if (past == 2) {
    // Desde la pagina de servicio
    $$(".otraRedi").show();
    $$(".normalRedi").hide();
    console.log("weo 1");
  } else {
    // Desde el home
    $$(".normalRedi").show();
    $$(".otraRedi").hide();
    console.log("weo 2");
  }

  $$("#actulizarArticulo").on("click", function () {
    var comentario = $$("#ArtiComentario").val();
    var cantidad = $$("#ArtiCant").val();
    var precio = $$("#priceValArti").val();
    var idArti = $$("#idArticulo").val();
    console.log(
      "Precio: " +
      precio +
      " Cantidad: " +
      cantidad +
      " Comentario: " +
      comentario
    );
    app.request({
      type: "PUT",
      dataType: "json",
      url: "https://laundryappmovil.com/api-nes/public/api/carrito/editar/" +
        idArti,
      data: {
        comentario: comentario,
        cantidad: cantidad,
        precio: precio,
      },
      beforeSend: function (xhr) {
        app.preloader.show();
      },
      success: function (data) {
        console.log(data);
        carrito();
        app.preloader.hide();
      },
      error: function (xhr) {
        console.log(xhr);
        app.preloader.hide();
      },
    });
  });

  $$("#confirmarOrden").on("click", function () {
    var total = $$("#totalInput").val();
    var idRestaurantes = $$("#idRestaurantes").val();
    var taxMunicipalF = $$("#TaxMunicipalT").val();
    var taxStateF = $$("#TaxStateT").val();
    //app.views.main.router.navigate('/detalle/' + total + '/' + idRestaurantes + '/' + taxMunicipalF + '/' + taxStateF);
  });
});

// var lt, lng, nombre;
// Pagina de detalle de la cita
$$(document).on("page:init", '.page[data-name="detalle"]', function (e) {
  console.log("DC | Detalle");
  var idcita = app.view.main.router.currentRoute.params.idcita;
  var iduser = app.view.main.router.currentRoute.params.iduser;
  var num = app.view.main.router.currentRoute.params.num;
  var centro = app.view.main.router.currentRoute.params.centro;
  console.log("centro: " + centro);
  console.log("cita: " + idcita);
  console.log("user: " + iduser);
  console.log("num: " + num);

  detalleCita(idcita, iduser, 1);
  var botones = document.getElementById("botones");
  var df = document.getElementById("detallefinalizado");
  var vch = document.getElementById("vistacitahecha");
  var btnmap = document.getElementById("IniciarViajeMap");
  if (num == 1) {
    botones.style.display = "none";
    df.style.display = "none";
    vch.style.display = "block";
  } else if (num == 2) {
    botones.style.display = "flex";
    df.style.display = "block";
    vch.style.display = "none";
  } else if(num == 3) {
    botones.style.display = "none";
    df.style.display = "block";
    vch.style.display = "none";
  } else if (num == 4){
    botones.style.display = "none";
    df.style.display = "block";
    vch.style.display = "block";
    btnmap.style.display = "none";
  }

  app.request({
    type: "GET",
    url: "https://laundryappmovil.com/api-nes/public/api/centrosLatLong/" + centro,
    dataType: "json",
    beforeSend: function (xhr) {},
    success: function (data) {
      console.log(data);
      $.each(data, function (i, item) {
        initMapa(centro, item.lat, item.lng);
        verEnMaps(item.lat, item.lng);
      });
    },
  });

  function verEnMaps(lat, lng) {
    $$("#IniciarViajeMap").on("click", function () {
      window.location =
        "https://maps.google.com/?q=" + lat + "," + lng + "&z=14";
    });
  }

  $$("#editarOrden").on("click", function () {
    console.log("editar orden");
    app.views.main.router.navigate("/editarCita/" + idcita + "/" + iduser, {
      transition: true,
    });
  });

  $$("#cancelOrden").on("click", function () {
    console.log("cancelar orden");
    cancelarCita(idcita, iduser, 1);
  });
});

function cancelarCita(idcita, iduser, opcion){
  app.request({
    type: "PUT",
    url: "https://laundryappmovil.com/api-nes/public/api/cancelarCita/" + idcita + "/" + iduser,
    beforeSend: function (xhr) {
      app.preloader.show();
    },
    success: function (data) {
      app.preloader.hide();
      if(opcion == 1){
        app.dialog.alert("Cita cancelada");
      }
      app.views.main.router.navigate("/", {
        transition: "f7-cover"
      });
    },
    error: function (xhr) {
      console.log("error: " + error);
    },
  });
}

function initMapa(centro, lat, lng) {
  console.log("lat: " + lat + " long: " + lng);
  var map, lt, long;
  lt = parseFloat(lat);
  long = parseFloat(lng);
  map = new google.maps.Map(document.getElementById("map"), {
    center: {
      lat: lt,
      lng: long
    },
    zoom: 13,
  });

  var marker = new google.maps.Marker({
    position: {
      lat: lt,
      lng: long
    },
    map: map,
    title: centro,
  });
}

// Pagina de detalle de la cita
$$(document).on("page:init", '.page[data-name="editarCita"]', function (e) {
  console.log("Estoy en editar cita");
  var idcita = app.view.main.router.currentRoute.params.idcita;
  var iduser = app.view.main.router.currentRoute.params.iduser;
  console.log("cita: " + idcita);
  console.log("user: " + iduser);

  editarCita(idcita, iduser);

  app.request({
    type: "GET",
    url: "https://laundryappmovil.com/api-nes/public/api/detalleCita/" +
      idcita +
      "/" +
      iduser,
    dataType: "json",
    beforeSend: function (xhr) {},
    success: function (data) {
      console.log(data);
      $.each(data, function (i, item) {
        $$(".NameCent").html();
        $$(".TelDoc").html(item.telefono);
        $$(".DirecCent").html();
        $$("#fechaResr").html();
        $$("#horaResr").html();
        $$("#especialistaEspec").html();
        $$("#especialista").html();
      });
    },
    error: function (xhr) {
      console.log("error: " + xhr);
    },
  });

  /*$$("#actSolid").on('click', function(){
    console.log('actualizar cita');

    app.request({
      type: "UPDATE",
      url: "https://laundryappmovil.com/api-nes/public/api/updateCita/" + idcita + '/' + iduser,
      data: {

      },
      beforeSend: function(xhr){
        app.preloader.show();
      }, success: function(data){
        app.preloader.hide();
        app.dialog.alert('Cita modificada y actualizada');
        app.views.main.router.navigate('/', { transition: true })
      }, error: function(xhr){
        console.log('error: ' + xhr);
      }
    });
  });*/
});

function editarCita(idcita, iduser) {
  console.log("cita: " + idcita + " user: " + iduser);
}

/* Pagina de la Informacion de la factura */
$$(document).on("page:init", '.page[data-name="factura"]', function (e) {
  var id = getParameterByName("id");
  var nombre = getParameterByName("nombre");
  var apellido = getParameterByName("apellido");
  var cedula = getParameterByName("cedula");
  var telefono = getParameterByName("telefono");
  var usuario = getParameterByName("usuario");
  var password = getParameterByName("password");
  var correo = getParameterByName("correo");

  /*Nombre del cliente*/
  $$("#nombre").html(nombre + " " + apellido);

  function formatAMPM(date) {
    var hours = date.substr(0, 3); //extraer hora
    var minutes = date.substr(3, 3); //extraer minutos
    minutes = minutes.replace(":", "");
    var ampm = hours >= 12 ? "am" : "pm";
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? "" + minutes : minutes;
    var strTime = hours + ":" + minutes + " " + ampm;
    return strTime;
  }

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/factura/" + id,
    beforeSend: function (xhr) {
      app.preloader.show();
    },
    success: function (data) {
      app.preloader.hide();
      console.log(data);
      var html = "";
      var total = 0;
      var subtotal = 0;
      $.each(data, function (i, item) {
        var date = item.fecha;
        var id = item.id;
        console.log(date);
        var hora = date.substr(10, 14);
        var fecha = date.substr(0, 10);
        $$("#fecha").html(fecha);
        $$("#id").html("000" + id);
        $$("#titlee").html("Factura 000" + id);
        $$("#hora").html(formatAMPM(hora));
        $$("#restaurante").html(item.restaurante);
        subtotal += parseInt(item.cantidad) * parseFloat(item.precio);
        var precioFact = parseInt(item.cantidad) * parseFloat(item.precio);

        var porcentajeStateF = parseFloat(item.taxstate); // Porcentaje
        var porcentajeMunicipalF = parseFloat(item.taxmunicipal); // Porcentaje

        $$("#taxStateF").html(
          "US$ " + formatter.format(porcentajeStateF).replace("$", "")
        );
        $$("#taxMunicipalF").html(
          "US$ " + formatter.format(porcentajeMunicipalF).replace("$", "")
        );
        $$("#deliveryF").html(
          "US$ " + formatter.format(item.delivery).replace("$", "")
        );
        $$("#total").html(
          "US$ " +
          formatter.format(parseFloat(item.costo_total)).replace("$", "")
        );
        $$("#subtotal").html(
          "US$ " + formatter.format(subtotal).replace("$", "")
        );

        html +=
          `<div class="row no-gap" style="padding-left: 30px;padding-right: 30px;">
      <div class="col" style="padding-bottom: 10px;">` +
          item.item +
          `</div>
      <div class="col" style="padding-bottom: 10px;">` +
          item.cantidad +
          `</div>
      <div class="col" style="padding-bottom: 10px;">US$ ` +
          formatter.format(precioFact).replace("$", "") +
          `</div>
      </div>`;
      });

      $$("#resultado").html(html);
    },
    error: function (xhr) {
      app.preloader.hide();
    },
  });
});

$$(document).on("page:init", '.page[data-name="empresa"]', function (e) {
  console.log("estoy en la pagina de informacion legal");

  $$(".nameUser").html(nombre + " " + apellido);

  var id = getParameterByName("id");
  traerFoto(id, 0);
});

$$(document).on("page:init", '.page[data-name="notificaciones"]', function (e) {
  console.log("estoy en la pagina de notificaciones");

  $$(".nameUser").html(nombre + " " + apellido);

  var id = getParameterByName("id");
  traerFoto(id, 0);
});

$$(document).on("page:init", '.page[data-name="atencion"]', function (e) {
  console.log("estoy en la pagina de atencion al cliente");

  $$(".nameUser").html(nombre + " " + apellido);

  var id = getParameterByName("id");
  traerFoto(id, 0);
});

$$(document).on("page:init", '.page[data-name="perfil"]', function (e) {
  console.log("estoy en la pagina del perfil");
  $$(".nameUser").html(nombre + " " + apellido);

  var id = getParameterByName("id");
  traerFoto(id, 0);

  // Create dynamic Popup
  $$("#historial").on("click", function () {
    dynamicPopupi.open();
    klkkaren(1);
  });

  $$("#segurom").on("click", function () {
    dynamicPopupi.open();
    klkkaren(2);
  });
  /*  */
  var dynamicPopupi = app.popup.create({
    content: `
    <div class="popup" style="height: 40%; top: 60%; display: block;">
      <div class="page-content">
        <div class="block klkkaren" style="display: grid;grid-template-columns: repeat(3, 1fr);
          grid-gap: 10px;align-items: center;align-content: center;">
        </div>
      </div>
    </div>
  `,
    // Events
    on: {
      open: function (popup) {
        console.log("Popup open");
      },
      opened: function (popup) {
        console.log("Popup opened");
      },
    },
  });

  //
  function klkkaren(tipo) {
    console.log("tipo: " + tipo);
    let ruta;
    if (tipo == 1) {
      ruta = "historial-medico";
    } else {
      ruta = "seguro-medico";
    }

    app.request({
      type: "GET",
      url: "http://laundryappmovil.com/api-nes/public/api/dependientes/" + id,
      dataType: "json",
      success: function (data) {
        console.log("dependientes: " + data);
        var html = "";
        html +=
          `<a href="/` +
          ruta +
          "/" +
          id +
          `/1" class="link item-content popup-close">
      <div class="item-content nombreDependiente` +
          id +
          `">
        <center>
          <img src="../img/img_avatar.png" width="48"/>
          <p style="color: black" class="sds` +
          id +
          `">` +
          nombre +
          " " +
          apellido +
          `</p>
        </center>
      </div>
      </a>`;
        $.each(data, function (i, item) {
          html +=
            `<a href="/` +
            ruta +
            "/" +
            item.id +
            `/2" class="link item-content popup-close">
        <div class="item-content nombreDependiente` +
            item.id +
            `">
          <center>
            <img src="../img/img_avatar.png" width="48"/>
            <p style="color: black" class="sds` +
            item.id +
            `">` +
            item.nombre +
            " " +
            item.apellido +
            `</p>
          </center>
        </div>
        </a>`;
        });
        $$(".klkkaren").html(html);
      },
    });
  }
});

$$(document).on("page:init", '.page[data-name="pagina-citas"]', function (e) {
  console.log("estoy en la pagina de la pagina citas");

  $$(".nameUser").html(nombre + " " + apellido);

  var id = getParameterByName("id");
  traerFoto(id, 0);

  $$(".elegirDependienteCita").on("click", function () {
    abrirPopup.open();
    klkkaren();
  });
});

var abrirPopup = app.popup.create({
  content: `<div class="popup" style="height: 40%; top: 60%; display: block;">
    <div class="page-content">
      <center><div class="block-title" style="overflow: initial; font-size: 18px;margin-bottom: -10px;margin-top: 20px;">Elegir dependiente</div></center>
      <div class="block klkkaren" style="display: grid;grid-template-columns: repeat(3, 1fr);
        grid-gap: 10px;align-items: center;align-content: center;">
      </div>
    </div>
  </div>`,
  // Events
  on: {
    open: function (popup) {
      console.log("Popup open");
    },
    opened: function (popup) {
      console.log("Popup opened");
    },
  },
});

function klkkaren() {
  app.request({
    type: "GET",
    url: "http://laundryappmovil.com/api-nes/public/api/dependientes/" + id,
    dataType: "json",
    success: function (data) {
      console.log("dependientes: " + data);
      var html = "";
      html += `<a href="/programar-cita/` + id + `/1" class="link item-content popup-close">
        <div class="item-content nombreDependiente` + id + `">
          <center>
            <img src="../../img/img_avatar.png" width="48"/>
            <p style="color: black" class="sds` + id + `">` + nombre + " " + apellido + `</p>
          </center>
        </div>
      </a>`;
      $.each(data, function (i, item) {
        html += `<a href="/programar-cita/` + item.id + `/2" class="link item-content popup-close">
          <div class="item-content nombreDependiente` + item.id + `">
            <center>
              <img src="../../img/img_avatar.png" width="48"/>
              <p style="color: black" class="sds` + item.id + `">` + item.nombre + " " + item.apellido + `</p>
            </center>
          </div>
        </a>`;
      });
      $$(".klkkaren").html(html);
    },
  });
}

function traerFoto(id, opcion){
  app.request({
    type: "GET",
    url: "https://laundryappmovil.com/api-nes/public/api/cliente/" + id,
    dataType: "json",
    beforeSend: function(xhr){
      let divimg = document.getElementById("imgUser");
      divimg.style.width = "25%";
      let divimgHome = document.getElementById("imgUserHome");
      divimgHome.style.width = "35%";
      $$(".imgUser").html(
        '<img class="skeleton-block skeleton-effect-fade" style="width: 100%; height: 85px; border-radius: 50%"/>'
      );
    }, success: function(data){
      if(opcion == 0){
        $.each(data, function (i, item) {
          if (item.foto == "") {
            $$(".imgUser").html(
              '<img src="./img/profile-user.png" style="width: 100%; height: 85px; border-radius: 50%"/>'
            );
          } else {
            $$(".imgUser").html(
              '<img style="width: 100%; height: 85px; border-radius: 50%" src="' +
              item.foto +
              '"/>'
            );
          }
        });
      } else {
        $.each(data, function (i, item) {
          if (item.foto == "") {
            $$(".imgPrevia").html(
              '<img src="./img/profile-user.png" id="smallImage" style="width:110px;border-radius:100px;margin-top: 10px"/>'
            );
          } else {
            $$(".imgPrevia").html(
              '<img src="' +
              item.foto +
              '" id="smallImage" style="width:110px;height: 110px;border-radius:100px;margin-top: 10px"/>'
            );
          }
        });
      }
      
    }, error: function(xhr){

    }
  });
}

$$(document).on("page:init", '.page[data-name="programar-cita"]', function (e) {
  console.log("DC | Programar Cita");

  var idDependiente = app.view.main.router.currentRoute.params.idDependiente;
  var titular = app.view.main.router.currentRoute.params.titular;

  console.log("iddependiente: " + idDependiente + " titular: " + titular);

  $$(".nameUser").html(nombre + " " + apellido);

  var id = getParameterByName("id");
  traerFoto(id, 0);

  $$("#centroM").on("click", function () {
    app.views.main.router.navigate(
      "/centros/1/" + idDependiente + "/" + titular
    );
  });

  $$(".centroE").on("click", function () {
    app.views.main.router.navigate(
      "/centros/2/" + idDependiente + "/" + titular
    );
  });

  $$(".centroD").on("click", function () {
    app.views.main.router.navigate(
      "/centros/3/" + idDependiente + "/" + titular
    );
  });

  $$(".centroO").on("click", function () {
    app.views.main.router.navigate(
      "/centros-elegir-profile/3/" + idDependiente + "/" + titular
    );
  });
  
  $$(".laboratorios").on("click", function () {
    app.views.main.router.navigate(
      "/centros-elegir-profile/4/" + idDependiente + "/" + titular
    );
  });

  $$(".citasV").on("click", function () {
    app.views.main.router.navigate(
      "/visita-medica/" + idDependiente + "/" + titular
    );
  });
});

$$(document).on("page:init",'.page[data-name="pagina-historial"]',function (e) {
  console.log("estoy en la pagina de la pagina historial");
  var idDependiente = app.view.main.router.currentRoute.params.idDependiente;
  $$(".nameUser").html(nombre + " " + apellido);
  console.log("dependiente: " + idDependiente);

  citas();
  var id = getParameterByName("id");
  traerFoto(id, 0);
});

$$(document).on("page:init", '.page[data-name="cambiarPass"]', function (e) {
  console.log("estoy en la pagina de cambiar contraseña");

  //VER CONTRASEÑA
  document.querySelector(".ver1 span").addEventListener("click", (e) => {
    const passwordInput1 = document.querySelector(".contraActual");
    if (e.target.classList.contains("show")) {
      e.target.classList.remove("show");
      passwordInput1.type = "text";
    } else {
      e.target.classList.add("show");
      passwordInput1.type = "password";
    }
  });
  
  document.querySelector(".ver2 span").addEventListener("click", (e) => {
    const passwordInput2 = document.querySelector(".contra1");
    if (e.target.classList.contains("show")) {
      e.target.classList.remove("show");
      passwordInput2.type = "text";
    } else {
      e.target.classList.add("show");
      passwordInput2.type = "password";
    }
  });

  document.querySelector(".ver3 span").addEventListener("click", (e) => {
    const passwordInput3 = document.querySelector(".contra2");
    if (e.target.classList.contains("show")) {
      e.target.classList.remove("show");
      passwordInput3.type = "text";
    } else {
      e.target.classList.add("show");
      passwordInput3.type = "password";
    }
  });
  $$("#cambiarContra").on('click', function(){
    let id = getParameterByName('id');
    let contraActual = $$('.contraActual').val();
    let contra1 = $$('.contra1').val();
    let contra2 = $$('.contra2').val();

    if(contraActual == "" || contra1 == "" || contra2 == ""){
      app.dialog.alert("Los campos no pueden estar vacios");
    } else if(contra1 != contra2) {
      app.dialog.alert("Las contraseñas no coinciden");
    } else {
      console.log(contraActual, contra2);
      app.request({
        type: "PUT",
        url: "https://laundryappmovil.com/api-nes/public/api/ValidarContra/" + id,
        data: {
          password1: contraActual,
          password2: contra2
        },
        beforeSend: function(xhr){
          app.dialog.preloader("Actualizando contraseña");
        }, success: function(data){
          app.dialog.close();
          console.log('data: ' + data);
          app.dialog.alert('Contraseña actualizada correctamente');
          setTimeout(function () {
            app.views.main.router.navigate("/",  {
              transition: "f7-cover", reloadAll: true, clearPreviousHistory: true
            });
          }, 3000);
        }, error: function(xhr){
          app.dialog.close();
          console.log('error: ' + xhr);
        }
      });
    }
  });
});

$$(document).on("page:init", '.page[data-name="info-personal"]', function (e) {
  console.log("estoy en la pagina del detalle de la persona");

  $$(".nameUser").html(nombre + " " + apellido);
  var correo = getParameterByName("correo");
  var telefono = getParameterByName("telefono");

  var id = getParameterByName("id");
  // traerFoto(id, 0);

  $$(".emailP").html(correo);
  $$(".telefonoP").html(telefono);

  $$(".editarInformacion").on("click", function () {
    app.views.main.router.navigate("/perfil-info/");
  });

  $$("#editarTelefono").on("click", function () {
    app.dialog.prompt("Cambiar telefono", function (tele) {
      app.dialog.confirm(
        "Estás seguro de cambiar tu telefono a: " + tele + "",
        function () {
          app.request({
            type: "PUT",
            url: "https://laundryappmovil.com/api-nes/public/api/cliente/update/" +
              id,
            data: {
              telefono: tele,
            },
            beforeSend: function (xhr) {
              app.dialog.preloader("Updating data");
            },
            success: function (data) {
              app.dialog.close();
              console.log(
                "Todo bien todo correcto y yo" + data + "que me alegro"
              );
              var toastCenter = app.toast.create({
                text: "Telefono actualizado correctamente",
                position: "center",
                closeTimeout: 2000,
              });
              toastCenter.open();
              setTimeout(function () {
                app.views.main.router.navigate("/", {
                  transition: "f7-cover"
                });
              }, 3000);
              $.ajax({
                type: "PUT",
                url: "https://laundryappmovil.com/api-nes/api/logout.php?id=" + id,
                beforeSend: function (xhr) {
                  app.dialog.preloader("Closing session...");
                },
                success: function (data) {
                  app.dialog.close();
                  console.log(data);
                  window.localStorage.setItem("login", 0);
                  localStorage.removeItem("usuario");
                  localStorage.removeItem("password");
                  location.href = "index.html";
                },
                error: function (xhr) {
                  console.log(xhr);
                  app.dialog.close();
                },
              });
            },
            error: function (xhr) {
              app.dialog.close();
              console.log("CAAAALVOOOOOOOOO");
            },
          });
        }
      );
      console.log(tele);
    });
  });

  let formulario = getParameterByName("formulario");
  let buton = document.getElementById('llenarForm');
  let boton = document.getElementById('resumenForm');
  console.log('formulario: ' + formulario);
  if(formulario == 1){
    buton.style.display = "none";
    boton.style.display = "block";
  } else {
    buton.style.display = "block";
    boton.style.display = "none";
  }

  $$("#llenarForm").on("click", function () {
    app.views.main.router.navigate("/formulario-general/", {
      transition: "f7-cover",
    });
  });

  $$("#resumenForm").on("click", function () {
    app.views.main.router.navigate("/formResult/", {
      transition: "f7-cover",
    });
  });
});

$$(document).on("page:init", '.page[data-name="formResult"]', function (e) {
  console.log('pagina de resultados de formularios');

  verForm();
});

function verForm(){
  let ide = getParameterByName('id');
  console.log('ide: ' + ide);
  console.log('traer formularios');
  app.request({
    type: "GET",
    url: "https://laundryappmovil.com/api-nes/public/api/verForm/" + ide,
    dataType: "json",
    beforeSend: function(xhr){
      app.preloader.show();
    },
    success: function(data){
      console.log(data);
      $.each(data, function(i, item){
        $$(".nombreForm").html(item.nombre);
        $$(".cedulaForm").html(item.cedula);
        $$(".nacionalidadForm").html(item.nacionalidad);
        $$(".telForm").html(item.telefono);
        $$(".celForm").html(item.celular);
        $$(".sexoForm").html(item.sexo);
        $$(".edadForm").html(item.edad);
        $$(".estcivilForm").html(item.estado_civil);
        $$(".seguroForm").html(item.seguro);
        $$(".nacForm").html(item.lugar_nac);
        $$(".resactualForm").html(item.residencia_actual);
        $$(".religionForm").html(item.religion);
        $$(".ocupacionForm").html(item.ocupacion);
        $$(".toficioForm").html(item.tiempo_oficio);
        $$(".mconsultaForm").html(item.motivo_consulta);
        $$(".antecedentesForm").html(item.antecedentes);
        $$(".apreviasForm").html(item.atenciones_previas);
        $$(".alergiasForm").html(item.alergias);
        $$(".aspectosgForm").html(item.aspectos_generales);
        $$(".padreForm").html(item.antecedentes_padre);
        $$(".madreForm").html(item.antecedentes_madre);
        $$(".hermanoForm").html(item.antecedentes_hermanos);
        $$(".tioForm").html(item.antecedentes_tios);
        $$(".abueloForm").html(item.antecedentes_abuelos);
        $$(".aquirurgicosForm").html(item.antecedentes_q);
        $$(".hiperForm").html(item.hipersensibilidad);
        $$(".atraumaForm").html(item.antecedentes_traumas);
        $$(".htoxicoForm").html(item.habitos_toxicos);
        $$(".peso6mesesForm").html(item.peso_6meses);
        $$(".hospiForm").html(item.hospitalizaciones);
        $$(".operacitugiaForm").html(item.operaciones_c);
        $$(".pesoactualForm").html(item.peso_actual);
        $$(".pesohabitualForm").html(item.peso_habitual);
        $$(".tallaForm").html(item.talla);
        $$(".pcForm").html(item.PC);
        $$(".estudiosForm").html(item.estudios);
        $$(".analisisForm").html(item.analisis);
        $$(".medicamentoForm").html(item.medicamentos);
      });
      app.request({
        type: "GET",
        url: "https://laundryappmovil.com/api-nes/public/api/verEnf/" + ide,
        dataType: "json",
        beforeSend: function(xhr){
          app.preloader.show();
        }, success: function(data){
          let html = "";
          console.log(data);
          app.preloader.hide();
          $.each(data, function(i, item){
            html += `<ul><li>`+ item.enfermedad +`</li></ul>`;
          });
          $$("#listENf").html(html);
        }, error: function(xhr){
          console.log('error: ' + xhr);
        }
      });

      app.request({
        type: "GET",
        url: "https://laundryappmovil.com/api-nes/public/api/verMA/" + ide,
        dataType: "json",
        beforeSend: function(xhr){
          app.preloader.show();
        }, success: function(data){
          let html1 = "";
          console.log(data);
          app.preloader.hide();
          $.each(data, function(i, item){
            html1 += `<div class="row"><div class="col" style="font-size: 14px">`+ item.pastilla +`</div>
            <div class="col" style="font-size: 14px">`+ item.miligramos +`</div>
            <div class="col" style="font-size: 14px">`+ item.dosis_diaria +`</div>
            <div class="col" style="font-size: 14px">`+ item.tiempo_medicacion +`</div></div>`;
          });
          $$("#listMedicacion").html(html1)
        }, error: function(xhr){
          console.log('error: ' + xhr);
        }
      });
    }, error: function(xhr){
      console.log('error: ' + xhr);
    }
  });
}

$$(document).on(
  "page:init",
  '.page[data-name="formulario-general"]',
  function (e) {
    console.log("Formulario general");
    let sexo = document.myForm.sexo;
    let estatus = document.myForm1.estatus;
    let segurobi = document.myForm2.seguro;
    let religion = document.myForm3.religion;
    let atencionbi = document.myForm4.atencion;
    let alergia = document.myForm5.alergia;
    let campoSeguro = document.getElementById("seguroConfirm");
    let campoAtencion = document.getElementById("LugarAtencion");
    let campoAlergia = document.getElementById("alergiasConfirm");
    let gen = "",
      status = "",
      segurob = "",
      rel = "",
      atencionb = "",
      alerg = "";
      campoSeguro.style.display = "none";
      campoAtencion.style.display = "none";
      campoAlergia.style.display = "none";

    for (var i = 0; i < sexo.length; i++) {
      sexo[i].addEventListener("change", function () {
        console.log(this.value);
        let valueSet = this.value;
        if (valueSet == "Hombre") {
          gen = "Masculino";
          window.localStorage.setItem('sexo', gen);
          console.log("hombre");
        } else {
          gen = "Femenino";
          window.localStorage.setItem('sexo', gen);
          console.log("mujer");
        }
      });
    }
    console.log("gen: " + gen);

    for (var j = 0; j < estatus.length; j++) {
      estatus[j].addEventListener("change", function () {
        console.log(this.value);
        let valueSet = this.value;
        if (valueSet == "Soltero") {
          status = "Soltero";
          window.localStorage.setItem('status', status);
        } else {
          status = "Casado";
          window.localStorage.setItem('status', status);
        }
      });
    }
    console.log("status: " + status);

    for (var k = 0; k < segurobi.length; k++) {
      segurobi[k].addEventListener("change", function () {
        console.log(this.value);
        let valueSet = this.value;
        if (valueSet == "No") {
          campoSeguro.style.display = "none";
        } else {
          campoSeguro.style.display = "block";
        }
      });
    }
    console.log("segurob: " + segurob);

    for (var l = 0; l < religion.length; l++) {
      religion[l].addEventListener("change", function () {
        console.log(this.value);
        let valueSet = this.value;
        if (valueSet == "Catolico") {
          rel = "Catolico";
          window.localStorage.setItem('religion', rel);
        } else if (valueSet == "Protestante") {
          rel = "Protestante";
          window.localStorage.setItem('religion', rel);
        } else if (valueSet == "Testigo de Jehova") {
          rel = "Testigo de Jehova";
          window.localStorage.setItem('religion', rel);
        } else {
          rel = "Otras";
          window.localStorage.setItem('religion', rel);
        }
      });
    }
    console.log("rel: " + rel);

    for (var a = 0; a < atencionbi.length; a++) {
      atencionbi[a].addEventListener("change", function () {
        console.log(this.value);
        let valueSet = this.value;
        if (valueSet == "No") {
          campoAtencion.style.display = "none";
        } else {
          campoAtencion.style.display = "block";
        }
      });
    }
    console.log("atencionb: " + atencionb);

    for (var f = 0; f < alergia.length; f++) {
      alergia[f].addEventListener("change", function () {
        console.log(this.value);
        let valueSet = this.value;
        if (valueSet == "No") {
          campoAlergia.style.display = "none";
        } else {
          campoAlergia.style.display = "block";
        }
      });
    }
    console.log("alerg: " + alerg);
    $$("#form2").on("click", function () {
      let nombre = $$("#nombreFG").val();
      let cedula = $$("#cedulaFG").val();
      let nacionalidad = $$("#nacionalidadFG").val();
      let telefono = $$("#telefonoFG").val();
      let celular = $$("#celularFG").val();
      let atencion = $$("#atencionFG").val();
      let alergias = $$("#alergiasFG").val();
      let seguro = $$("#seguroFG").val();
      let edad = $$("#edadFG").val();
      let nacimiento = $$("#nacimientoFG").val();
      let residencia = $$("#residenciaFG").val();
      let ocupacion = $$("#ocupacionFG").val();
      let tiempo_oficio = $$("#tiempo_oficioFG").val();
      let motivo = $$("#motivoFG").val();
      let antecedentes = $$("#antecedentesFG").val();
      let aspectosg = $$("#aspecto_generalFG").val();

      if (atencion == "" || atencion.length == 0) {
        atencionb = "No";
      } else {
        atencionb = atencion;
      }

      if (alergias == "" || alergias.length == 0) {
        alerg = "No";
      } else {
        alerg = alergias;
      }

      if (seguro == "" || seguro.length == 0) {
        segurob = "No";
      } else {
        segurob = seguro;
      }

      console.log(nombre, cedula, nacionalidad, telefono, celular, edad, nacimiento, residencia, ocupacion, tiempo_oficio, motivo, antecedentes, aspectosg, alergias, atencion, gen, status, segurob, rel, atencionb, alerg);
      console.log("atencionb: " + atencionb);
      window.localStorage.setItem("nombre", nombre);
      window.localStorage.setItem("cedula", cedula);
      window.localStorage.setItem("nacionalidad", nacionalidad);
      window.localStorage.setItem("telefono", telefono);
      window.localStorage.setItem("celular", celular);
      window.localStorage.setItem("edad", edad);
      window.localStorage.setItem("seguro", segurob);
      window.localStorage.setItem("nacimiento", nacimiento);
      window.localStorage.setItem("residencia", residencia);
      window.localStorage.setItem("ocupacion", ocupacion);
      window.localStorage.setItem("tiempo_oficio", tiempo_oficio);
      window.localStorage.setItem("motivo", motivo);
      window.localStorage.setItem("antecedentes", antecedentes);
      window.localStorage.setItem("atencion", atencionb);
      window.localStorage.setItem("alergias", alerg);
      window.localStorage.setItem("aspectosg", aspectosg);
      //app.views.main.router.navigate("/formulario-antecedentes/" + nombre + "/" + cedula + "/" + nacionalidad + "/" + telefono + "/" + celular + "/" + edad + "/" + seguro + "/" + nacimiento + "/" + residencia + "/" + ocupacion + "/" + tiempo_oficio + "/" + motivo + "/" + antecedentes + "/" + atencion + "/" + alergias + "/" + aspectosg, { transition: "f7-cover" });
      app.views.main.router.navigate("/formulario-antecedentes/", {
        transition: "f7-cover"
      });
    });
  }
);

$$(document).on("page:init", '.page[data-name="formulario-antecedentes"]', function (e) {
  console.log("Formulario antecedentes");

  let chk1 = document.getElementById("cardiopatia"),
    ck1;
  let chk2 = document.getElementById("hipertension"),
    ck2;
  let chk3 = document.getElementById("ecerevslr"),
    ck3;
  let chk4 = document.getElementById("epilepsia"),
    ck4;
  let chk5 = document.getElementById("asmabrnq"),
    ck5;
  let chk6 = document.getElementById("erespiratoria"),
    ck6;
  let chk7 = document.getElementById("diabetes"),
    ck7;
  let chk8 = document.getElementById("tiroides"),
    ck8;
  let chk9 = document.getElementById("obesidad"),
    ck9;
  let chk10 = document.getElementById("prostata"),
    ck10;
  let chk11 = document.getElementById("ealergias"),
    ck11;
  let chk12 = document.getElementById("disrenal"),
    ck12;
  let chk13 = document.getElementById("cancer"),
    ck13;
  let chk14 = document.getElementById("hepatopatias"),
    ck14;
  let chk15 = document.getElementById("erenales"),
    ck15;
  let chk16 = document.getElementById("ehematog"),
    ck16;
  let chk17 = document.getElementById("neoplasias"),
    ck17;
  let chk18 = document.getElementById("otros"),
    ck18;
  let ERConfirm = document.getElementById("ERConfirm");
  let EREConfirm = document.getElementById("EREConfirm");
  let EHConfirm = document.getElementById("EHConfirm");
  let NConfirm = document.getElementById("NConfirm");
  let OConfirm = document.getElementById("OConfirm");
  ERConfirm.style.display = "none";
  EREConfirm.style.display = "none";
  EHConfirm.style.display = "none";
  NConfirm.style.display = "none";
  OConfirm.style.display = "none";

  chk1.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk1.value);
      ck1 = 1;
      window.localStorage.setItem("ckcardiopatia", 1);
      sqlChk(1, chk1.value);
    } else {
      ck1 = 0;
      window.localStorage.setItem("ckcardiopatia", 0);
      sqlChk(2, chk1.value);
    }
  });

  chk2.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk2.value);
      sqlChk(1, chk2.value);
      ck2 = 1;
      window.localStorage.setItem("ckhipertension", 1);
    } else {
      ck2 = 0;
      window.localStorage.setItem("ckhipertension", 0);
      sqlChk(2, chk2.value);
    }
  });

  chk3.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk3.value);
      sqlChk(1, chk3.value);
      ck3 = 1;
      window.localStorage.setItem("ckecerevslr", 1);
    } else {
      sqlChk(2, chk3.value);
      ck3 = 0;
      window.localStorage.setItem("ckecerevslr", 0);
    }
  });

  chk4.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk4.value);
      sqlChk(1, chk4.value);
      ck4 = 1;
      window.localStorage.setItem("ckepilepsia", 1);
    } else {
      sqlChk(2, chk4.value);
      ck4 = 0;
      window.localStorage.setItem("ckepilepsia", 0);
    }
  });

  chk5.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk5.value);
      sqlChk(1, chk5.value);
      ck5 = 1;
      window.localStorage.setItem("ckasmabrnq", 1);
    } else {
      sqlChk(2, chk5.value);
      ck5 = 0;
      window.localStorage.setItem("ckasmabrnq", 0);
    }
  });

  chk6.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk6.value);
      sqlChk(1, chk6.value);
      ck6 = 1;
      window.localStorage.setItem("ckerespiratoria", chk6.value);
      ERConfirm.style.display = "block";
    } else {
      sqlChk(2, chk6.value);
      ck6 = 0;
      window.localStorage.setItem("ckerespiratoria", 0);
      ERConfirm.style.display = "none";
    }
  });

  chk7.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk7.value);
      sqlChk(1, chk7.value);
      ck7 = 1;
      window.localStorage.setItem("ckdiabetes", 1);
    } else {
      sqlChk(2, chk7.value);
      ck7 = 0;
      window.localStorage.setItem("ckdiabetes", 0);
    }
  });

  chk8.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk8.value);
      sqlChk(1, chk8.value);
      ck8 = 1;
      window.localStorage.setItem("cktiroides", 1);
    } else {
      sqlChk(2, chk8.value);
      ck8 = 0;
      window.localStorage.setItem("cktiroides", 0);
    }
  });

  chk9.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk9.value);
      sqlChk(1, chk9.value);
      ck9 = 1;
      window.localStorage.setItem("ckobesidad", 1);
    } else {
      sqlChk(2, chk9.value);
      ck9 = 0;
      window.localStorage.setItem("ckobesidad", 0);
    }
  });

  chk10.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk10.value);
      sqlChk(1, chk10.value);
      ck10 = 1;
      window.localStorage.setItem("ckprostata", 1);
    } else {
      sqlChk(2, chk10.value);
      ck10 = 0;
      window.localStorage.setItem("ckprostata", 0);
    }
  });

  chk11.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk11.value);
      sqlChk(1, chk11.value);
      ck11 = 1;
      window.localStorage.setItem("ckealergias", 1);
    } else {
      sqlChk(2, chk11.value);
      ck11 = 0;
      window.localStorage.setItem("ckealergias", 0);
    }
  });

  chk12.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk12.value);
      sqlChk(1, chk12.value);
      ck12 = 1;
      window.localStorage.setItem("ckdisrenal", 1);
    } else {
      sqlChk(2, chk12.value);
      ck12 = 0;
      window.localStorage.setItem("ckdisrenal", 0);
    }
  });

  chk13.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk13.value);
      sqlChk(1, chk13.value);
      ck13 = 1;
      window.localStorage.setItem("ckcancer", 1);
    } else {
      sqlChk(2, chk13.value);
      ck13 = 0;
      window.localStorage.setItem("ckcancer", 0);
    }
  });

  chk14.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk14.value);
      sqlChk(1, chk14.value);
      ck14 = 1;
      window.localStorage.setItem("ckhepatopatias", 1);
    } else {
      sqlChk(2, chk14.value);
      ck14 = 0;
      window.localStorage.setItem("ckhepatopatias", 0);
    }
  });

  chk15.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk15.value);
      sqlChk(1, chk15.value);
      ck15 = 1;
      window.localStorage.setItem("ckerenales", chk15.value);
      EREConfirm.style.display = "block";
    } else {
      sqlChk(2, chk15.value);
      ck15 = 0;
      window.localStorage.setItem("ckerenales", 0);
      EREConfirm.style.display = "none";
    }
  });

  chk16.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk16.value);
      sqlChk(1, chk16.value);
      ck16 = 1;
      window.localStorage.setItem("ckehematog", chk16.value);
      EHConfirm.style.display = "block";
    } else {
      sqlChk(2, chk16.value);
      ck16 = 0;
      window.localStorage.setItem("ckehematog", 0);
      EHConfirm.style.display = "none";
    }
  });

  chk17.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk17.value);
      sqlChk(1, chk17.value);
      ck17 = 1;
      window.localStorage.setItem("ckneoplasias", chk17.value);
      NConfirm.style.display = "block";
    } else {
      sqlChk(2, chk17.value);
      ck17 = 0;
      window.localStorage.setItem("ckneoplasias", 0);
      NConfirm.style.display = "none";
    }
  });

  chk18.addEventListener('change', function () {
    if (this.checked) {
      console.log(chk18.value);
      sqlChk(1, chk18.value);
      ck18 = 1;
      window.localStorage.setItem("ckotros", chk18.value);
      OConfirm.style.display = "block";
    } else {
      sqlChk(2, chk18.value);
      ck18 = 0;
      window.localStorage.setItem("ckotros", 0);
      OConfirm.style.display = "none";
    }
  });

  $$("#form3").on("click", function () {

    let txa = $$("#EresFA").val();
    let txa1 = $$("#ErenalesFA").val();
    let txa2 = $$("#EHeFA").val();
    let txa3 = $$("#NFA").val();
    let txa4 = $$("#otrasFA").val();
    let txAHPadreFA = $$("#AHPadreFA").val();
    let txAHMadreFA = $$("#AHMadreFA").val();
    let txAHermanoFA = $$("#AHermanoFA").val();
    let txAHAbuelosFA = $$("#AHAbuelosFA").val();
    let txAHTiosFA = $$("#AHTiosFA").val();
    let txrespiratoria = window.localStorage.getItem("ckerespiratoria");
    let txrenales = window.localStorage.getItem("ckerenales");
    let txhematog = window.localStorage.getItem("ckehematog");
    let txneoplasia = window.localStorage.getItem("ckneoplasias");
    let txotros = window.localStorage.getItem("ckotros");

    let txaerespiratoria, txaerenales, txaehematog, txaneoplasias, txaotros;

    if (txa == "" || txa.length == 0) {
      txaerespiratoria = "";
      window.localStorage.setItem("txaerespiratoria", "");
    } else {
      txaerespiratoria = txa;
      window.localStorage.setItem("txaerespiratoria", txa);
      sqlChk(3, txrespiratoria, txa);
    }

    if (txa1 == "" || txa1.length == 0) {
      txaerenales = "";
      window.localStorage.setItem("txaerenales", "");
    } else {
      txaerenales = txa1;
      window.localStorage.setItem("txaerenales", txa1);
      sqlChk(3, txrenales, txa1);
    }

    if (txa2 == "" || txa2.length == 0) {
      txaehematog = "";
      window.localStorage.setItem("txaehematog", "");
    } else {
      txaehematog = txa2;
      window.localStorage.setItem("txaehematog", txa2);
      sqlChk(3, txhematog, txa2);
    }

    if (txa3 == "" || txa3.length == 0) {
      txaneoplasias = "";
      window.localStorage.setItem("txaneoplasias", "");
    } else {
      txaneoplasias = txa3;
      window.localStorage.setItem("txaneoplasias", txa3);
      sqlChk(3, txneoplasia, txa3);
    }

    if (txa4 == "" || txa4.length == 0) {
      txaotros = "";
      window.localStorage.setItem("txaotros", "");
    } else {
      txaotros = txa4;
      window.localStorage.setItem("txaotros", txa4);
      sqlChk(3, txotros, txa4);
    }

    window.localStorage.setItem("AHPadreFA", txAHPadreFA);
    window.localStorage.setItem("AHMadreFA", txAHMadreFA);
    window.localStorage.setItem("AHermanoFA", txAHermanoFA);
    window.localStorage.setItem("AHAbuelosFA", txAHAbuelosFA);
    window.localStorage.setItem("AHTiosFA", txAHTiosFA);

    app.views.main.router.navigate("/formulario-otrosAntecedentes/", {
      transition: "f7-cover",
    });
  });
});

function sqlChk(status, chk, detalle){
  console.log('guardar chk: ' + status + chk + detalle);
  let id = getParameterByName("id");
  let detail = "";

  if(detalle == undefined || detalle == "undefined"){
    detail = "";
  } else {
    detail = detalle;
  }
    app.request({
      type: "GET",
      url: "https://laundryappmovil.com/api-nes/public/api/consultCondicion/" + chk,
      dataType: "json",
      success: function(data){
        let idenf = data[0].id;
        console.log(data[0].id);
        if(status == 1){
          app.request({
            type: "POST",
            url: "https://laundryappmovil.com/api-nes/public/api/fEnfermedad/add",
            data: {
              id_enfermedad: idenf,
              detalle: detail,
              titular: 0,
              id_user: id
            }, success: function(data){
              console.log(data);
            }
          });
        } else if (status == 2){
          app.request({
            type: "DELETE",
            url: "https://laundryappmovil.com/api-nes/public/api/fEnfermedad/del/" + idenf + "/" + 0 + "/" + id,
            dataType: "json",
            success: function(data){
              console.log(data);
            }
          });
        } else {
          app.request({
            type: "PUT",
            url: "https://laundryappmovil.com/api-nes/public/api/fEnfermedad/put/" + idenf + "/" + 0 + "/" + id,
            data: {
              detalle: detail,
            },
            success: function(data){
              console.log(data);
            }
          });
        }
      }
    });
}

$$(document).on("page:init", '.page[data-name="formulario-otrosAntecedentes"]', function (e) {
  console.log("Formulario otros antecedentes");

  let aquirurg = document.form1.aquirurg;
  let rhipersens = document.form2.rhipersens;
  let atraumas = document.form3.atraumas;
  let htoxicos = document.form4.htoxicos;
  let hospitalizaciones = document.form5.hospitalizaciones;
  let opercirug = document.form6.opercirug;
  let _6meses = document.form7._6meses;
  let AQConfirm = document.getElementById("AQConfirm");
  let RHAConfirm = document.getElementById("RHAConfirm");
  let ATConfirm = document.getElementById("ATConfirm");
  let HTConfirm = document.getElementById("HTConfirm");
  let HConfirm = document.getElementById("HConfirm");
  let OCConfirm = document.getElementById("OCConfirm");
  let _6mesesConfirm = document.getElementById("_6mesesConfirm");
  AQConfirm.style.display = "none";
  RHAConfirm.style.display = "none";
  ATConfirm.style.display = "none";
  HTConfirm.style.display = "none";
  HConfirm.style.display = "none";
  OCConfirm.style.display = "none";
  _6mesesConfirm.style.display = "none";

  for (var j = 0; j < aquirurg.length; j++) {
    aquirurg[j].addEventListener("change", function () {
      console.log(this.value);
      let valueSet = this.value;
      if (valueSet == "Si") {
        aquirurg = "Si";
        AQConfirm.style.display = "block";
      } else {
        aquirurg = "No";
        AQConfirm.style.display = "none";
      }
    });
  }
  console.log("aquirurg: " + aquirurg);

  for (var j = 0; j < rhipersens.length; j++) {
    rhipersens[j].addEventListener("change", function () {
      console.log(this.value);
      let valueSet = this.value;
      if (valueSet == "Si") {
        rhipersens = "Si";
        RHAConfirm.style.display = "block";
      } else {
        rhipersens = "No";
        RHAConfirm.style.display = "none";
      }
    });
  }
  console.log("rhipersens: " + rhipersens);

  for (var j = 0; j < atraumas.length; j++) {
    atraumas[j].addEventListener("change", function () {
      console.log(this.value);
      let valueSet = this.value;
      if (valueSet == "Si") {
        atraumas = "Si";
        ATConfirm.style.display = "block";
      } else {
        atraumas = "No";
        ATConfirm.style.display = "none";
      }
    });
  }
  console.log("atraumas: " + atraumas);

  for (var j = 0; j < htoxicos.length; j++) {
    htoxicos[j].addEventListener("change", function () {
      console.log(this.value);
      let valueSet = this.value;
      if (valueSet == "Si") {
        htoxicos = "Si";
        HTConfirm.style.display = "block";
      } else {
        htoxicos = "No";
        HTConfirm.style.display = "none";
      }
    });
  }
  console.log("htoxicos: " + htoxicos);

  for (var j = 0; j < hospitalizaciones.length; j++) {
    hospitalizaciones[j].addEventListener("change", function () {
      console.log(this.value);
      let valueSet = this.value;
      if (valueSet == "Si") {
        hospitalizaciones = "Si";
        HConfirm.style.display = "block";
      } else {
        hospitalizaciones = "No";
        HConfirm.style.display = "none";
      }
    });
  }
  console.log("hospitalizaciones: " + hospitalizaciones);

  for (var j = 0; j < opercirug.length; j++) {
    opercirug[j].addEventListener("change", function () {
      console.log(this.value);
      let valueSet = this.value;
      if (valueSet == "Si") {
        opercirug = "Si";
        OCConfirm.style.display = "block";
      } else {
        opercirug = "No";
        OCConfirm.style.display = "none";
      }
    });
  }
  console.log("opercirug: " + opercirug);

  for (var j = 0; j < _6meses.length; j++) {
    _6meses[j].addEventListener("change", function () {
      console.log(this.value);
      let valueSet = this.value;
      if (valueSet == "Si") {
        _6meses = "Si";
        _6mesesConfirm.style.display = "block";
      } else {
        _6meses = "No";
        _6mesesConfirm.style.display = "none";
      }
    });
  }
  console.log("opercirug: " + opercirug);

  $$("#form4").on("click", function () {
    let AOAntec_qFOA = $$("#AOAntec_qFOA").val(), AOAntec_qFOAC = "";
    let AORHFOA = $$("#AORHFOA").val(), AORHFOAC = "";
    let AOAntec_traumaFOA = $$("#AOAntec_traumaFOA").val(), AOAntec_traumaFOAC = "";
    let AOHabitos_toxFOA = $$("#AOHabitos_toxFOA").val(), AOHabitos_toxFOAC = "";
    let AOHospitaliFOA = $$("#AOHospitaliFOA").val(), AOHospitaliFOAC = "";
    let operacionesFOA = $$("#operacionesFOA").val(), operacionesFOAC = "";
    let peso_actualFOA = $$("#peso_actualFOA").val();
    let peso_habitualFAO = $$("#peso_habitualFAO").val();
    let peso_6mesesFOA = $$("#peso_6mesesFOA").val(), peso_6mesesFOAC = "";
    let tallaFOA = $$("#tallaFOA").val();
    let PCFOA = $$("#PCFOA").val();

    if(AOAntec_qFOA == "" || AOAntec_qFOA.length == 0){
      AOAntec_qFOAC = "No";
    } else {
      AOAntec_qFOAC = AOAntec_qFOA;
    }

    if(AORHFOA == "" || AORHFOA.length == 0){
      AORHFOAC = "No";
    } else {
      AORHFOAC = AORHFOA;
    }

    if(AOAntec_traumaFOA == "" || AOAntec_traumaFOA.length == 0){
      AOAntec_traumaFOAC = "No";
    } else {
      AOAntec_traumaFOAC = AOAntec_traumaFOA;
    }

    if(AOHabitos_toxFOA == "" || AOHabitos_toxFOA.length == 0){
      AOHabitos_toxFOAC = "No";
    } else {
      AOHabitos_toxFOAC = AOHabitos_toxFOA;
    }

    if(AOHospitaliFOA == "" || AOHospitaliFOA.length == 0){
      AOHospitaliFOAC = "No";
    } else {
      AOHospitaliFOAC = AOHospitaliFOA;
    }

    if(operacionesFOA == "" || operacionesFOA.length == 0){
      operacionesFOAC = "No";
    } else {
      operacionesFOAC = operacionesFOA;
    }

    if(peso_6mesesFOA == "" || peso_6mesesFOA.length == 0){
      peso_6mesesFOAC = 0;
    } else {
      peso_6mesesFOAC = peso_6mesesFOA;
    }

    window.localStorage.setItem("AOAntec_qFOA", AOAntec_qFOAC);
    window.localStorage.setItem("AORHFOA", AORHFOAC);
    window.localStorage.setItem("AOAntec_traumaFOA", AOAntec_traumaFOAC);
    window.localStorage.setItem("AOHabitos_toxFOA", AOHabitos_toxFOAC);
    window.localStorage.setItem("AOHospitaliFOA", AOHospitaliFOAC);
    window.localStorage.setItem("operacionesFOA", operacionesFOAC);
    window.localStorage.setItem("peso_actualFOA", peso_actualFOA);
    window.localStorage.setItem("peso_habitualFAO", peso_habitualFAO);
    window.localStorage.setItem("peso_6mesesFOA", peso_6mesesFOAC);
    window.localStorage.setItem("tallaFOA", tallaFOA);
    window.localStorage.setItem("PCFOA", PCFOA);

    app.views.main.router.navigate("/formulario-medicacion/", {
      transition: "f7-cover",
    });
  });
});

$$(document).on("page:init", '.page[data-name="formulario-medicacion"]', function (e) {
  console.log('formulario medicacion');

  let button = document.getElementById("addMedicacion");
  let id = getParameterByName("id");
  //let num = 0;

  $$('#addMedicacion').on('click', function () {
    console.log('agregar medicacion actual');
    let pastilla = $$("#pastilla").val();
    let miligramos = $$("#miligramos").val();
    let ddiaria = $$("#ddiaria").val();
    let time_medication = $$("#time_medication").val();
    let result = document.getElementById("resultados");
    //num++;
    console.log(pastilla, miligramos, ddiaria, time_medication);

    if(pastilla == "" || pastilla.length == 0 || miligramos == "" || miligramos.length == 0 || ddiaria == "" || ddiaria.length == 0 || time_medication == "" || time_medication.length == 0){
      app.dialog.alert("Los campos no pueden estar vacios");
    } else {
      app.request({
        type: "POST",
        url: "https://laundryappmovil.com/api-nes/public/api/addMedicacion/add",
        data: {
          pastilla: pastilla,
          miligramos: miligramos,
          dosis_diaria: ddiaria,
          time_medication: time_medication,
          titular: 0,
          usuario: id
        },
        beforeSend: function(xhr){
          app.dialog.preloader();
        }, success: function(data){
          app.dialog.close();
          console.log(data);
          $$("#pastilla").val("");
          $$("#miligramos").val("");
          $$("#ddiaria").val("");
          $$("#time_medication").val("");

          app.request({
            type: "GET",
            url: "https://laundryappmovil.com/api-nes/public/api/verMedicacion/add/" + 0 + "/" + id,
            dataType: "json",
            beforeSend: function(xhr){},
            success: function(data){
              console.log('data: ' + data);
              let html = "";
              $.each(data, function(i, item){
                html += `<div class="list">
                  <ul>
                    <li class="swipeout" id="clase`+ item.id +`">
                      <div class="item-content swipeout-content">
                        <div class="row" style="width: 100%">
                          <div class="col">`+ item.pastilla +`</div>
                          <div class="col">`+ item.miligramos +`</div>
                          <div class="col">`+ item.dosis_diaria +`</div>
                          <div class="col">`+ item.tiempo_medicacion +`</div>
                        </div>
                      </div>
                      <div class="swipeout-actions-right">
                        <a href="#" onclick="eliminarList(`+ item.id +`)" class="swipeout-delete">Eliminar</a>
                      </div>
                    </li>
                  </ul>
                </div>`;
              });
              $$("#listaResult").html(html);
            }, error: function(xhr){
              console.log('error', xhr);
            }
          });
          result.style.display = "block";
        }, error: function(xhr){
          console.log('error', xhr);
        }
      });
    }
  });

  $$("#enviarForms").on('click', function(){
    let array = [];
    let estudios = $$("#estudios").val();
    let analisis = $$("#analisis").val();
    let medicamentos = $$("#medicamentos").val();
    let nombre = window.localStorage.getItem("nombre");
    let cedula = window.localStorage.getItem("cedula");
    let nacionalidad = window.localStorage.getItem("nacionalidad");
    let telefono = window.localStorage.getItem("telefono");
    let celular = window.localStorage.getItem("celular");
    let status = window.localStorage.getItem('status');
    let sexo = window.localStorage.getItem('sexo');
    let religion = window.localStorage.getItem('religion');
    let edad = window.localStorage.getItem("edad");
    let seguro = window.localStorage.getItem("seguro");
    let nacimiento = window.localStorage.getItem("nacimiento");
    let residencia = window.localStorage.getItem("residencia");
    let ocupacion = window.localStorage.getItem("ocupacion");
    let tiempo_oficio = window.localStorage.getItem("tiempo_oficio");
    let motivo = window.localStorage.getItem("motivo");
    let antecedentes = window.localStorage.getItem("antecedentes");
    let atencion = window.localStorage.getItem("atencion");
    let alergias = window.localStorage.getItem("alergias");
    let aspectosg = window.localStorage.getItem("aspectosg");
    let cardiopatia = window.localStorage.getItem("ckcardiopatia");
    let hipertension = window.localStorage.getItem("ckhipertension");
    let cerebral = window.localStorage.getItem("ckecerevslr");
    let epilepsia = window.localStorage.getItem("ckepilepsia");
    let asma = window.localStorage.getItem("ckasmabrnq");
    let respiratoria = window.localStorage.getItem("ckerespiratoria");
    let diabetes = window.localStorage.getItem("ckdiabetes");
    let tiroides = window.localStorage.getItem("cktiroides");
    let obesidad = window.localStorage.getItem("ckobesidad");
    let prostata = window.localStorage.getItem("ckprostata");
    let alergias2 = window.localStorage.getItem("ckealergias");
    let disrenal = window.localStorage.getItem("ckdisrenal");
    let cancer = window.localStorage.getItem("ckcancer");
    let hepatopatia = window.localStorage.getItem("ckhepatopatias");
    let renales = window.localStorage.getItem("ckerenales");
    let hematog = window.localStorage.getItem("ckehematog");
    let neoplasia = window.localStorage.getItem("ckneoplasias");
    let otros = window.localStorage.getItem("ckotros");
    let padre = window.localStorage.getItem("AHPadreFA");
    let madre = window.localStorage.getItem("AHMadreFA");
    let hermanos = window.localStorage.getItem("AHermanoFA");
    let abuelos = window.localStorage.getItem("AHAbuelosFA");
    let tios = window.localStorage.getItem("AHTiosFA");
    let antquirurgicos = window.localStorage.getItem("AOAntec_qFOA");
    let reaccionH = window.localStorage.getItem("AORHFOA");
    let anttraumas = window.localStorage.getItem("AOAntec_traumaFOA");
    let toxicos = window.localStorage.getItem("AOHabitos_toxFOA");
    let hospi = window.localStorage.getItem("AOHospitaliFOA");
    let operaciones = window.localStorage.getItem("operacionesFOA");
    let pactual = window.localStorage.getItem("peso_actualFOA");
    let phabitual = window.localStorage.getItem("peso_habitualFAO");
    let p6meses = window.localStorage.getItem("peso_6mesesFOA");
    let talla = window.localStorage.getItem("tallaFOA");
    let pc = window.localStorage.getItem("PCFOA");
    console.log(nombre, atencion, prostata, madre, phabitual);
    array[cardiopatia, hipertension, cerebral, epilepsia, asma, respiratoria, diabetes, tiroides, obesidad, prostata, alergias2, disrenal, cancer, hepatopatia, renales, hematog, neoplasia, otros]
    console.log(array.length);
    console.log("enviando forms");

    //guardar info en formulario general
    app.request({
      type: "POST",
      url: "https://laundryappmovil.com/api-nes/public/api/fGeneral/add",
      data: {
        nombre: nombre,
        cedula: cedula,
        nacionalidad: nacionalidad,
        telefono: telefono,
        celular: celular,
        sexo: sexo,
        edad: edad,
        estado_civil: status,
        seguro: seguro,
        lugar_nac: nacimiento,
        residencia_actual: residencia,
        religion: religion,
        ocupacion: ocupacion,
        tiempo_oficio: tiempo_oficio,
        motivo_consulta: motivo,
        antecedentes: antecedentes,
        atenciones_previas: atencion,
        alergias: alergias,
        aspectos_generales: aspectosg,
        titular: 0,
        id_user: id
      },
      beforeSend: function(xhr){
        app.dialog.preloader();
      },
      success: function(data){
        app.dialog.close();
        console.log(data);
        console.log('formulario general bien');
        //guardar info antecedentes
        app.request({
          type: "POST",
          url: "https://laundryappmovil.com/api-nes/public/api/fAntecedentes/add",
          data: {
            antecedentes_padre: padre,
            antecedentes_madre: madre,
            antecedentes_hermanos: hermanos,
            antecedentes_tios: tios,
            antecedentes_abuelos: abuelos,
            antecedentes_q: antquirurgicos,
            hipersensibilidad: reaccionH,
            antecedentes_traumas: anttraumas,
            habitos_toxicos: toxicos,
            hospitalizaciones: hospi,
            operaciones_c: operaciones,
            peso_actual: pactual,
            peso_habitual: phabitual,
            peso_6meses: p6meses,
            talla: talla,
            PC: pc,
            estudios: estudios,
            analisis: analisis,
            medicamentos: medicamentos,
            titular: 0,
            id_user: id
          },
          beforeSend: function(xhr){
            app.dialog.preloader();
          },
          success: function(data){
            app.dialog.close();
            console.log('data: ' + data);
            console.log('formulario antecedentes con exito');
          }, error: function(xhr){
            console.log('error',xhr);
          }
        });

        //actualizar formulario a 1 en usuarios
        app.request({
          type: "PUT",
          url: "https://laundryappmovil.com/api-nes/public/api/updateFormU/" + id,
          beforeSend: function(xhr){},
          success: function(data){
            console.log('formulario actualizado con exito');
            app.dialog.alert('Formulario completado con exito');
            app.views.main.router.navigate("/", { transition: true });
          }, error: function(xhr){
            console.log('error',xhr);
          }
        });
      }, error: function(xhr){
        console.log('error',xhr);
      }
    });

    //guardar info enfermedades
    // app.request({
    //   type: "POST",
    //   url: "",
    //   data: {
    //     id_enfermedad: 
    //   },
    //   beforeSend: function(xhr){},
    //   success: function(data){
    //     console.log('formulario actualizado con exito');
    //   }, error: function(xhr){
    //     console.log('error',xhr);
    //   }
    // });

    
  });
});

function eliminarList(idnum){
  console.log('eliminar: ' + idnum);
  
  let div = document.getElementById("clase" + idnum);
  let parent = div.parentElement;
  parent.removeChild(div);
  app.request({
    type: "DELETE",
    url: "https://laundryappmovil.com/api-nes/public/api/delMedicacion/add/" + 0 + "/" + id + "/" + idnum,
    dataType: "json",
    beforeSend: function(xhr){},
    success: function(data){
      console.log(data);
    }, error: function(xhr){
      console.log('error', xhr);
    }
  });
  console.log('eliminado bien');
}

$$(document).on("page:init", '.page[data-name="favoritos"]', function (e) {
  console.log("estoy en la pagina de favoritos");

  $$(".nameUser").html(nombre + " " + apellido);

  var id = getParameterByName("id");
  traerFoto(id, 0);

  app.request({
    type: "GET",
    url: "https://laundryappmovil.com/api-nes/public/api/centrosMedicos/" + id,
    dataType: "json",
    beforeSend: function (xhr) {},
    success: function (data) {
      $.each(data, function (i, item) {
        $$(".centroM").html("1. " + item.centro);
      });
    },
    error: function (xhr) {
      console.log("error: " + xhr);
    },
  });

  app.request({
    type: "GET",
    url: "https://laundryappmovil.com/api-nes/public/api/centrosMedicosD/" + id,
    dataType: "json",
    beforeSend: function (xhr) {},
    success: function (data) {
      $.each(data, function (i, item) {
        $$(".centroD").html("2. " + item.centro);
      });
    },
    error: function (xhr) {
      console.log("error: " + xhr);
    },
  });

  app.request({
    type: "GET",
    url: "https://laundryappmovil.com/api-nes/public/api/doctor/" + id,
    dataType: "json",
    beforeSend: function (xhr) {},
    success: function (data) {
      $.each(data, function (i, item) {
        $$(".doctor").html("3. " + item.doctor);
      });
    },
    error: function (xhr) {
      console.log("error: " + xhr);
    },
  });
});

$$(document).on("page:init",'.page[data-name="informacion-perfil"]',function (e) {
  console.log("estoy en la pagina del informacion del perfil");

  var res = celular.substr(0, 3);
  var res1 = celular.substr(3, 3);
  var res2 = celular.substr(6, 6);
  $$(".TelefonoProfilePage").html("(" + res + ") " + res1 + "-" + res2);
  $$(".UsuarioProfilePage").html(nombre + " " + apellido);

  var id = getParameterByName("id");
  traerFoto(id, 1);
});

/* Pagina de Add Direccion*/
$$(document).on("page:init", '.page[data-name="add_direccion"]', function (e) {
  //localizar();
  /* Tabs hover effect */
  $$(".toolbar-inner .weo").click(function () {
    $$(".weo").removeClass("activess");
    $$(this).addClass("activess");
  });

  //Metodo de guardado de direcciones
  $$("#addAddress").on("click", function (e) {
    console.log("Presionaste add Address");

    var direccion = $$("#AddressLine1Add").val();
    var ciudad = $$("#CityAdreess").val();
    var zipcode = $$("#zipCodeAddress").val();
    var nota = $$("#noteAddress").val();
    var edificio = $$("#NumberAdreess").val();
    var type = $$("#typeAdress").val();
    var lat = $$("#lat").val();
    var lon = $$("#lon").val();

    if (direccion == "" || ciudad == "" || edificio == "") {
      app.dialog.alert("Completa los campo correctamente");
    } else {
      var id = getParameterByName("id");
      app.request({
        type: "GET",
        url: "https://laundryappmovil.com/api-nes/api/adressEdit.php",
        data: {
          id: id,
          direccion: direccion,
          ciudad: ciudad,
          edificio: edificio,
          nota: nota,
          type: type,
          lat: lat,
          lon: lon,
          zipcode: zipcode,
        },
        beforeSend: function (xhr) {
          app.dialog.preloader("Guardando direccion");
        },
        success: function (data) {
          app.dialog.close();
          console.log(data);
          $$("input").val("");
          $$("#noteAddress").val("");
          app.dialog.alert("Dirección guardada con éxito");
        },
        error: function (xhr) {
          app.dialog.close();
          $$("#my-form").val("");
          app.dialog.alert(xhr);
          app.dialog.alert("Error al guardar la direccion");
        },
      });
    }
  });

  var geocoder;
  var map;
  var marker;

  function initialize() {
    var initialLat = $(".search_latitude").val();
    var initialLong = $(".search_longitude").val();
    initialLat = initialLat ? initialLat : 18.202526;
    initialLong = initialLong ? initialLong : -66.507603;

    var latlng = new google.maps.LatLng(initialLat, initialLong);
    var options = {
      zoom: 12,
      center: latlng,
      disableDefaultUI: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };

    map = new google.maps.Map(document.getElementById("geomap"), options);
    geocoder = new google.maps.Geocoder();
    var infowindow = new google.maps.InfoWindow();

    //geocodeLatLng(geocoder, map, infowindow);
    marker = new google.maps.Marker({
      map: map,
      draggable: true,
      position: latlng,
    });

    //pacInput();
    google.maps.event.addListener(marker, "dragend", function () {
      var point = marker.getPosition();
      map.panTo(point);
      geocoder.geocode({
          latLng: marker.getPosition()
        },
        function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            marker.setPosition(results[0].geometry.location);
            /* POSTAL CODE */
            for (var i = 0; i < results[0].address_components.length; i++) {
              var component = results[0].address_components[i];
              if (component.types[0] == "postal_code") {
                $(".search_zip").val(component.long_name);
              }
            }

            $(".search_addr").val(results[0].formatted_address);
            $(".search_ciudad").val(results[0].address_components[2].long_name);
            //$('.search_pais').val(results[0].address_components[5].long_name);
            //$('.search_zip').val(results[1].address_components[0].long_name);
            $(".search_latitude").val(marker.getPosition().lat());
            $(".search_longitude").val(marker.getPosition().lng());
          }
        }
      );
    });

    var options = {
      zoom: 12,
      center: latlng,
      disableDefaultUI: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };

    $("#submitOK").on("click", function () {
      localizar();
      //map = new google.maps.Map(document.getElementById("geomap"), options);
      geocoder = new google.maps.Geocoder();
      var infowindow = new google.maps.InfoWindow();

      var input = $$("#UbicacionActual").val();
      var lati = $$("#lat").val();
      var longi = $$("#lon").val();
      var latlngStr = input.split(",", 2);
      var latlng = {
        lat: parseFloat(lati),
        lng: parseFloat(longi)
      };
      geocoder.geocode({
        location: latlng
      }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          if (results[1]) {
            map.setZoom(11);
            marker.setPosition(results[0].geometry.location);
            /*var marker = new google.maps.Marker({
              position: latlng,
              draggable: true,
              map: map
            });*/
            //initialize();
            //pacInput();
            console.log(results[0].address_components[6].long_name);
            infowindow.setContent(results[1].formatted_address);
            console.log(results[1].formatted_address);
            $$("#direccion").val(results[1].formatted_address);
            $$("#zipcode").val(results[0].address_components[6].long_name);
            $$(".search_ciudad").val(
              results[0].address_components[2].long_name
            );
            $$(".setAddressHome").html(results[1].formatted_address);
            $$(".SavedPlacesURLCurrent").css({
              "font-size": "11px",
              "padding-left": "15px",
              "padding-right": "15px",
            });
            //infowindow.open(map, marker);
          } else {
            window.alert("No results found");
          }
        } else {
          window.alert("Please verify that geolocation is active");
        }
      });
    });
  }

  function pacInput() {
    var PostCodeid = ".pac-input";
    var tik = ".pac-container";

    // Create the search box and link it to the UI element.
    var input = document.getElementById("pac-input");
    var searchti = document.getElementsByClassName("pac-container");
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener("bounds_changed", function () {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    searchBox.addListener("places_changed", function () {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function (marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function (place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }

        var direct = $(".pac-input").val();
        geocoder.geocode({
          address: direct
        }, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            marker.setPosition(results[0].geometry.location);

            /* POSTAL CODE */
            for (var i = 0; i < results[0].address_components.length; i++) {
              var component = results[0].address_components[i];
              if (component.types[0] == "postal_code") {
                $(".search_zip").val(component.long_name);
              }
            }

            $(".search_addr").val(results[0].formatted_address);
            $(".search_ciudad").val(results[0].address_components[2].long_name);
            //$('.search_pais').val(results[0].address_components[5].long_name);
            //$('.search_zip').val(results[1].address_components[0].long_name);
            $(".search_latitude").val(marker.getPosition().lat());
            $(".search_longitude").val(marker.getPosition().lng());
          } else {
            alert(
              "Geocode was not successful for the following reason: " + status
            );
          }
        });

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });

    /*
     * Point location on google map
     */
    $(".get_map").click(function (e) {
      var address = $(".pac-input").val();
      geocoder.geocode({
        address: address
      }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          map.setCenter(results[0].geometry.location);
          marker.setPosition(results[0].geometry.location);

          /* POSTAL CODE */
          for (var i = 0; i < results[0].address_components.length; i++) {
            var component = results[0].address_components[i];
            if (component.types[0] == "postal_code") {
              $(".search_zip").val(component.long_name);
            }
          }

          $(".search_addr").val(results[0].formatted_address);
          $(".search_ciudad").val(results[0].address_components[2].long_name);
          //$('.search_pais').val(results[0].address_components[5].long_name);
          //$('.search_zip').val(results[1].address_components[0].long_name);
          $(".search_latitude").val(marker.getPosition().lat());
          $(".search_longitude").val(marker.getPosition().lng());
        } else {
          alert(
            "Geocode was not successful for the following reason: " + status
          );
        }
      });
      e.preventDefault();
    });

    //Add listener to marker for reverse geocoding
    google.maps.event.addListener(marker, "drag", function () {
      geocoder.geocode({
          latLng: marker.getPosition()
        },
        function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            //if (results[0]) {
            /* POSTAL CODE */
            for (var i = 0; i < results[0].address_components.length; i++) {
              var component = results[0].address_components[i];
              if (component.types[0] == "postal_code") {
                $(".search_zip").val(component.long_name);
              }
            }

            $(".search_addr").val(results[0].formatted_address);
            $(".search_ciudad").val(results[0].address_components[2].long_name);
            //$('.search_pais').val(results[0].address_components[5].long_name);
            //$('.search_zip').val(results[1].address_components[0].long_name);
            $(".search_latitude").val(marker.getPosition().lat());
            $(".search_longitude").val(marker.getPosition().lng());
          }
          //}
        }
      );
    });
  }

  $(document).ready(function () {
    //load google map
    initialize();
    pacInput();
  });
}); /* Cierre de la pagina de direccion */

function effect(event) {
  var page = document.querySelector(".page");
  var touch = event.targetTouches[0];
  var y = touch.pageY;

  console.log(y);

  if (y > 50) {
    page.style.height = y + "px";
    page.style.transition = ".3s";
  }

  if (y > 300) {
    page.style.height = "50px";
    page.style.transition = ".2s";
    setTimeout(function () {}, 500);
  }
}

//Funcion de borrar tarjeta
function borrarTarjeta(id) {
  console.log("klk borro esta tarjeta " + id);
  app.dialog.confirm(
    "Seguro que quieres borrar este metodo de pago?",
    function () {
      app.request({
        type: "delete",
        url: "https://laundryappmovil.com/api-nes/public/api/pagos/delete/" + id,
        beforeSend: function () {
          console.log("Borrando Tarjeta...");
        },
        success: function (resp) {
          console.log(resp);
          pagos();
        },
      });
    }
  );
}

//Funcion de borrar Direcciones
function borrarDireccion(id) {
  console.log("klk borro esta direccion " + id);
  app.dialog.confirm("Seguro que quieres borrar esta direccion?", function () {
    app.request({
      type: "delete",
      url: "https://laundryappmovil.com/api-nes/public/api/direcciones/delete/" +
        id,
      beforeSend: function () {
        console.log("Borrando Direccion...");
      },
      success: function (resp) {
        console.log(resp);
        direcciones();
      },
    });
  });
}

//Funcion de borrar Direcciones
function borrarItem(id) {
  app.dialog.confirm("Seguro que quieres borrar esta articulo ?", function () {
    app.request({
      type: "delete",
      url: "https://laundryappmovil.com/api-nes/public/api/carrito/delete/" + id,
      beforeSend: function () {
        console.log("Borrando Articulo...");
      },
      success: function (resp) {
        console.log(resp);
        carrito();
        $$("#carito").html();
      },
    });
  });
}

function pagos() {
  //ID cliente
  var id = getParameterByName("id");
  console.log(id);
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/api/selectPago.php?id=" + id,
    beforeSend: function (xhr) {},
    success: function (data) {
      console.log(data);
      var html = "";
      html += '<div class="list"><ul>';
      $.each(data, function (i, item) {
        if (item.tipo == "VISA") {
          var type = "img/pagos/visa.png";
          var di = "Visa";
        } else if (item.tipo == "AMEX") {
          var type = "img/pagos/amex.png";
          var di = "Amex";
        } else {
          var type = "img/pagos/mastercard.png";
          var di = "MasterCard";
        }

        var num = item.numero;
        var res = num.substr(14, 14);

        html += '<li class="swipeout deleted-callback">';
        html += '<input type="hidden" id="IdPago" value="' + item.id + '">';
        html += '<a href="#" class="item-link item-content swipeout-content">';
        html += '<div class="item-media">';
        html +=
          '<img src="' + type + '" style="width: 50px;border-radius: 50%;">';
        html += "</div>";
        html += '<div class="item-inner">';
        html += '<div class="item-title">';
        html += '<div class="item-header">' + di + "</div>";
        html += "**** **** **** " + res;
        html += "</div>";
        html +=
          '<div class="item-after">' + item.mes + `/` + item.ano + "</div>";
        html += "</div>";
        html += "</a>";
        html += '<div class="swipeout-actions-right">';
        html +=
          '<a href="#" class="swipeout-delete" onclick="borrarTarjeta(' +
          item.id +
          ')">';
        html += ' <i class="f7-icons">trash</i>';
        html += "</a>";
        html += "</div>";
        html += "</li>";

        $$("#pagos").html(html);
      });
      html += "</div></ul>";
    },
    error: function () {
      app.preloader.hide();
    },
  });
}

//Funcion de notificaciones
function notificaciones() {
  var id = getParameterByName("id");
  $.ajax({
    type: "GET",
    url: "https://laundryappmovil.com/api-nes/api/notifications.php?id=" + id,
    beforeSend: function (xhr) {
      $$("#ulnotidi").html("");
    },
    success: function (datal) {
      console.log(datal);
      app.dialog.close();
      var html = "";
      if (datal == "" || datal == "[]" || datal == " []" || datal.length <= 5) {
        html += "<center>";
        html +=
          "<img src='img/notifications.png' style='width: 160px;height: 160px;margin-top: 50px;'>";
        html += "</center>";
        html +=
          "<p style='color:black;font-size:16px;text-align:center;'>No hay notificaciones disponible.</p>";
      } else {
        $.each(JSON.parse(datal), function (i, item) {
          if (item.titulo == "Proceso de recogida") {
            var img = "img/notificaciones/Recurso6.png";
          } else if (item.titulo == "Camino a su destino") {
            var img = "img/notificaciones/taxi.png";
          } else if (item.titulo == "Orden en proceso") {
            var img = "img/notificaciones/food-and-restaurant.png";
          } else if (item.titulo == "Proceso de entrega") {
            var img = "img/notificaciones/transport.png";
          } else if (item.titulo == "Camino a su destino") {
            var img = "img/notificaciones/recurso10.png";
          } else if (item.titulo == "Orden entregada") {
            var img = "img/notificaciones/completed-task.png";
          } else if (item.titulo == "Orden cancelada") {
            var img = "img/notificaciones/cancel.png";
          } else {
            var img = "img/notificaciones/completed-task.png";
          }

          html +=
            `<li>
        <div class="item-content">
        <div class="item-media">
        <img src="` +
            img +
            `" width="70"/>
        </div>
        <div class="item-inner">
        <div class="item-title-row" style="margin-bottom: 5px;">
        <div class="item-title" style="font-weight: 550;font-size: 13px">` +
            item.titulo +
            `</div>
        <div class="item-after" style="font-size: 10px;">` +
            item.hour +
            `</div>
        </div>
        <div class="item-subtitle" style="white-space: unset;font-size: 10px;color: #757575">` +
            item.mensaje +
            `</div>
        <div class="item-text" style="font-size: 10px;font-weight: bold;">Fecha : ` +
            item.fecha +
            `</div>
        </div>
        </div>
        </li>`;
        });
      }
      $$("#ulnotidi").html(html);
    },
    error: function (xhr) {
      app.dialog.close();
    },
  });
}

function formatAMPM(date) {
  var hours = date.substr(0, 3); //extraer hora
  var minutes = date.substr(3, 3); //extraer minutos
  minutes = minutes.replace(":", "");
  var ampm = hours >= 12 ? "am" : "pm";
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? "" + minutes : minutes;
  var strTime = hours + ":" + minutes + " " + ampm;
  return strTime;
}

function getFormattedDate(date) {
  var year = date.getFullYear();
  var month = (1 + date.getMonth()).toString()/*.padStart(2, "0")*/;
  var day = date.getDate().toString()/*.padStart(2, "0")*/;

  return day + "/" + month + "/" + year;
}

function formatDay(d) {
  var fecha = new Date(d);
  var options = {
    year: "numeric",
    month: "long",
    day: "numeric"
  };
  return fecha.toLocaleDateString("es-ES", options);
}

function timeline(id) {
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/orden/timeline/" + id,
    beforeSend: function (xhr) {},
    success: function (data) {
      var timeLine = "";
      $.each(data, function (i, item) {
        var id = item.task_id;

        if (item.periodo == "Proceso de recogida") {
          color = "gray"; // Proceso de recogida
        } else if (item.periodo == "Camino a su destino") {
          color = "#8C0095"; // Camino a su destino
        } else if (item.periodo == "Orden en proceso") {
          color = "blue"; // Proceso de lavado
        } else if (item.periodo == "Proceso de entrega") {
          color = "orange"; // Proceso de entrega
        } else if (item.periodo == "Camino a su destino") {
          color = "#8C0095"; // Camino a su destino
        } else if (item.periodo == "Orden entregada") {
          color = "green"; // Orden entregada
        } else if (item.periodo == "Firma realizada") {
          color = "black"; // Orden entregada
        } else if (item.periodo == "Orden Cancelada") {
          color = "red"; // Orden Cancelada
        } else {
          color = "red"; //Orden declinada
        }

        timeLine +=
          `<div class="timeline-item">
        <div class="timeline-item-date">` +
          item.fecha +
          ` ` +
          item.hora +
          `</div>
        <div class="timeline-item-divider"></div>
        <div class="timeline-item-content">
        <div class="timeline-item-inner" style="background-color:` +
          color +
          `;color:white;">` +
          item.periodo +
          `</div>
        </div>
        </div>`;
      });
      $$("#resultadoTime").html(timeLine);
    },
    error: function (xhr) {
      //app.preloader.hide();
    },
  });
  console.log("estoy aqui");
}

function detalleOrden(id) {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  $.ajax({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/orden/consulta/" + id,
    beforeSend: function (xhr) {
      app.preloader.show();
      $$("#resultadoOrd").html("");
    },
    success: function (data) {
      app.preloader.hide();
      console.log(data);
      var html = "";
      var timeLine = "";
      var total = 0;
      var subtotal = 0;
      timeline(id); // Llamar la linea de tiempo.
      $.each(data, function (i, item) {
        var date = item.fecha;
        var id = item.id;

        if (item.estado == 1) {
          color = "gray"; // Proceso de recogida
        } else if (item.estado == 2) {
          color = "#8C0095"; // Camino a su destino
        } else if (item.estado == 3) {
          color = "blue"; // Proceso de lavado
        } else if (item.estado == 4) {
          color = "orange"; // Proceso de entrega
        } else if (item.estado == 5) {
          color = "#8C0095"; // Camino a su destino
        } else if (item.estado == 6) {
          color = "green"; // Orden entregada
        } else {
          color = "red"; //Orden declinada
        }

        var hora = date.substr(10, 14);
        var fecha = date.substr(0, 10);
        var horaStr = hora.substr(4, 3);
        var compareHora = horaStr
          .replace(/[^a-z0-9\s]/gi, "")
          .replace(/[_\s]/g, "-");
        $$("#fechaH").html(fecha);
        $$("#idH").html("000" + id);
        console.log(
          new Date(date)
          .toLocaleTimeString()
          .replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3")
        );
        var timeSheet = formatAMPM(hora);
        //console.log('Esta son los minutos '+horaStr);
        $$("#horaH").html(timeSheet);
        $$("#nombreH").html(item.cliente);
        $$("#restauranteName").html(item.restaurante);
        subtotal += parseFloat(item.cantidad) * parseFloat(item.precio);
        var precioFactu = parseInt(item.cantidad) * parseFloat(item.precio);
        var dt = new Date();
        if (item.estado == 6 || item.estado == 8) {
          $$("#HereTrack").html(
            '<p  style="color: gray;text-align: center;font-size: 12px;">Track no disponible</p>'
          );
          $$("#CancelPedido").html(
            '<p style="color: gray;text-align: center;font-size: 12px;">Cancelar no disponinble</p>'
          );
        } else if (compareHora != dt.getMinutes()) {
          $$("#HereTrack").html(
            '<p  style="color: blue;text-align: center;font-size: 12px;" onclick="trackHere(' +
            item.id_conductor +
            "," +
            item.id +
            "," +
            item.lat +
            "," +
            item.lng +
            ')"> Track order</p>'
          );
          $$("#CancelPedido").html(
            '<p style="color: gray;text-align: center;font-size: 12px;">Cancelar no disponinble</p>'
          );
        } else {
          $$("#HereTrack").html(
            '<p  style="color: blue;text-align: center;font-size: 12px;" onclick="trackHere(' +
            item.id_conductor +
            "," +
            item.id +
            "," +
            item.lat +
            "," +
            item.lng +
            ')"> Track order</p>'
          );
          $$("#CancelPedido").html(
            '<p class="open-confirm" style="color: red;text-align: center;font-size: 12px;" onclick="cancelPedido(' +
            id +
            ')"> Cancelar Pedido</p>'
          );
        }

        var porcentajeState = parseFloat(item.taxstate); // Porcentaje
        var porcentajeMunicipal = parseFloat(item.taxmunicipal); // Porcentaje
        $$("#taxState").html(
          "US$ " + formatter.format(porcentajeState).replace("$", "")
        );
        $$("#taxMunicipal").html(
          "US$ " + formatter.format(porcentajeMunicipal).replace("$", "")
        );
        $$("#delivery").html(
          "US$ " + formatter.format(item.delivery).replace("$", "")
        );
        $$("#totalH").html(
          "US$ " +
          formatter.format(parseFloat(item.costo_total)).replace("$", "")
        );
        $$("#subtotalH").html(
          "US$ " + formatter.format(subtotal).replace("$", "")
        );

        html +=
          `<div class="row no-gap" style="padding-left: 30px;padding-right: 30px;">
        <div class="col" style="padding-bottom: 10px;">` +
          item.item +
          `</div>
        <div class="col" style="padding-bottom: 10px;">` +
          item.cantidad +
          `</div>
        <div class="col" style="padding-bottom: 10px;">US$ ` +
          formatter.format(precioFactu).replace("$", "") +
          `</div>
        </div>`;
        $$("#resultadoOrd").html(html);
      });
    },
    error: function () {
      app.preloader.hide();
    },
  });
}

function trackHere(id_conductor, id_orden, lat, lng) {
  app.popup.close(".popup-about");
  app.views.main.router.navigate(
    "/seguimiento/" + id_conductor + "/" + id_orden + "/" + lat + "/" + lng
  );
}

function cancelPedido(id) {
  app.views.main.router.navigate("/cancelar/" + id);
  app.popup.close(".popup-about");
}

function verificarCancel() {}

/* Pagina de Cencelar orden */
$$(document).on("page:init", '.page[data-name="cancelar"]', function (e) {
  console.log("Estoy en la pagina de cancelar");
  var idCancelar = app.view.main.router.currentRoute.params.value;
  $$("#idCancelar").val(idCancelar);

  verificarCancel();

  $$("#sendCancel").on("click", function () {
    console.log("diste click en sendCancel");
    var idCam = $$("#idCancelar").val();
    var id = getParameterByName("id");
    app.request({
      type: "GET",
      url: "https://laundryappmovil.com/api-nes/api/cancelarOrden.php?id=" +
        id +
        "&idOrden=" +
        idCam,
      beforeSend: function (xhr) {
        app.preloader.show();
      },
      success: function (data) {
        console.log(data);
        app.preloader.hide();
        app.views.main.router.navigate("/");
      },
      error: function () {
        app.preloader.hide();
      },
    });
  });
});

/* Pagina de chat */
$$(document).on("page:init", '.page[data-name="chat"]', function (e) {
  var nameClient = app.view.main.router.currentRoute.params.name;
  $$(".titleChange").html(nameClient);
  var nowDate = new Date();
  //Fecha completa
  var options = {
    month: "long",
    day: "numeric"
  };
  var nowt = nowDate.toLocaleDateString("es-ES", options);
  //Dia actual
  var optionsDay = {
    weekday: "long"
  };
  var today = nowDate.toLocaleDateString("es-ES", optionsDay);
  $$(".messages-title").html(
    '<span style="text-transform: capitalize;font-size:13px;">Hoy, ' +
    nowt +
    "</span>"
  );

  var messages = app.messages.create({
    el: ".messages",

    // First message rule
    firstMessageRule: function (message, previousMessage, nextMessage) {
      // Skip if title
      if (message.isTitle) return false;
      /* if:
        - there is no previous message
        - or previous message type (send/received) is different
        - or previous message sender name is different
      */
      if (
        !previousMessage ||
        previousMessage.type !== message.type ||
        previousMessage.name !== message.name
      )
        return true;
      return false;
    },
    // Last message rule
    lastMessageRule: function (message, previousMessage, nextMessage) {
      // Skip if title
      if (message.isTitle) return false;
      /* if:
        - there is no next message
        - or next message type (send/received) is different
        - or next message sender name is different
      */
      if (
        !nextMessage ||
        nextMessage.type !== message.type ||
        nextMessage.name !== message.name
      )
        return true;
      return false;
    },
    // Last message rule
    tailMessageRule: function (message, previousMessage, nextMessage) {
      // Skip if title
      if (message.isTitle) return false;
      /* if (bascially same as lastMessageRule):
      - there is no next message
      - or next message type (send/received) is different
      - or next message sender name is different
    */
      if (
        !nextMessage ||
        nextMessage.type !== message.type ||
        nextMessage.name !== message.name
      )
        return true;
      return false;
    },
  });

  // Init Messagebar
  var messagebar = app.messagebar.create({
    el: ".messagebar",
  });

  // Response flag
  var responseInProgress = false;

  // Send Message
  $$(".send-link").on("click", function () {
    var text = messagebar.getValue().replace(/\n/g, "<br>").trim();
    // return if empty message
    if (!text.length) return;

    // Clear area
    messagebar.clear();

    //Insertar mensaje en la base de datos.
    var task_num = app.view.main.router.currentRoute.params.task; //ID de la tarea
    var id = getParameterByName("id"); //ID del conductor
    var id_cliente = app.view.main.router.currentRoute.params.id; //ID cliente
    InsertText(text, id, task_num, id_cliente);

    // Return focus to area
    messagebar.focus();

    // Add message to messages
    messages.addMessage({
      text: text, //Aqui envia el mensaje escrito
    });

    if (responseInProgress) return;
    // Receive dummy message
    //receiveMessage();
  });

  // Dummy response
  var answers = [
    "Yes!",
    "No",
    "Hm...",
    "I am not sure",
    "And what about you?",
    "May be ;)",
    "Lorem ipsum dolor sit amet, consectetur",
    "What?",
    "Are you sure?",
    "Of course",
    "Need to think about it",
    "Amazing!!!",
  ];
  var people = [{
      name: "Kate Johnson",
      avatar: "https://cdn.framework7.io/placeholder/people-100x100-9.jpg",
    },
    {
      name: "Blue Ninja",
      avatar: "https://cdn.framework7.io/placeholder/people-100x100-7.jpg",
    },
  ];

  //Esta funcion recibe el mensaje enviado de parte del cliente
  function receiveMessage() {
    responseInProgress = true;
    setTimeout(function () {
      //Recibir Mensajes
      $.ajax({
        type: "GET",
        url: "http://laundryappmovil.com/access/public/api/chat-cons/key=eyJ1c2VyIjoiZmVybmFuZG8iLCJwcyI6ImYxMDEyMDQyMSIsImFsZyI6IkhTMjU2In0",
        data: {
          task: task_num,
        },
        beforeSend: function (xhr) {
          //console.log('Enviando el asunto espera...');
        },
        success: function (data) {
          //console.log('Esta es tu respuesta : '+data);

          $.each(JSON.parse("[" + data + "]"), function (i, item) {
            console.log(item.mensaje);

            // Get random answer and random person
            //var answer = item.mensaje[Math.floor(Math.random() * item.mensaje.length)];
            var person = people[Math.floor(Math.random() * people.length)];
            var clienteNam = app.view.main.router.currentRoute.params.cliente;

            // Show typing indicator
            messages.showTyping({
              header: clienteNam + " esta escribiendo",
              avatar: person.avatar,
            });

            setTimeout(function () {
              // Add received dummy message
              messages.addMessage({
                text: item.mensaje,
                type: "received",
                name: clienteNam,
                avatar: person.avatar,
              });
              // Hide typing indicator
              messages.hideTyping();
              responseInProgress = false;
            }, 4000);
          });
        }, //close success ajax
      }); //close ajax
    }, 1000);
  } //close function

  /*Funcion que inserta el mensaje del chat en la base datos
  y envia la notificacion al cliente con el ID del mensaje*/
  function InsertText(text, id, task_num, id_cliente) {
    $.ajax({
      type: "GET",
      //url: "http://laundryappmovil.com/access/public/api/chat/key=eyJ1c2VyIjoiZmVybmFuZG8iLCJwcyI6ImYxMDEyMDQyMSIsImFsZyI6IkhTMjU2In0",
      url: "http://laundryappmovil.com/sg7-driver/apis/chat.php",
      data: {
        mensaje: text,
        id_creador: id,
        task: task_num,
      },
      beforeSend: function (xhr) {
        //console.log('Enviando el asunto espera...');
      },
      success: function (data) {
        console.log("Esta es tu respuesta :-> " + data);
        //Enviar notificacion del chat
        $.ajax({
          type: "POST",
          url: "http://www.SG7.com/services/api/ApiRest/putNotify",
          data: {
            tipo: "mensajes",
            id: id_cliente, //ID del cliente usar de ejemplo el 6
            data: "{" + data + "}", //ID del chat
          },
          beforeSend: function (xhr) {
            //console.log('Enviando Mensaje del Chat...');
            xhr.setRequestHeader("X-Api-key", "123456");
          },
          success: function (data) {
            //console.log('Respuesta del chat : '+data);
          },
          error: function (xhr) {
            //console.log('Hubo error mi loco en el envio del chat chequea eso');
          },
        });
      },
      error: function (xhr) {
        console.log("Hubo error mi loco chequea eso urgente");
      },
    });
  } //Aqui cierra la funcion InsertText()
});

//Funcion de Ordenes
function ordenes() {
  var id = getParameterByName("id");
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/orden/" + id,
    beforeSend: function (xhr) {
      $$("#ulordenes").html("");
    },
    success: function (datal) {
      console.log(datal);
      var html = "";
      var cap = ""; //ID DE LA ORDEN
      var cop = ""; //STATUS DE LA ORDEN

      if (datal == "" || datal == "[]") {
        cop += `<li style="padding-bottom: 5px;">Vacio</li>`;
        cap += `<li style="padding-bottom: 5px;">Vacio</li>`;
        html += "<center>";
        html +=
          "<img src='img/orderk.png' style='width: 160px;height: 160px;margin-top: 50px;'>";
        html += "</center>";
        html += `<p style="text-align: center;font-size: 14px;margin-top: 45px;color:gray;">No hay ordenes disponibles</p>`;
      } else {
        $.each(datal, function (i, item) {
          var chofer;
          if (item.conductor == "" || item.conductor == null) {
            chofer = "no tiene asignado";
          } else {
            chofer = item.conductor;
          }

          if (item.estado == 1) {
            color = "gray"; // Proceso de recogida
          } else if (item.estado == 2) {
            color = "#8C0095"; // Camino a su destino
          } else if (item.estado == 3) {
            color = "blue"; // Orden en proceso
          } else if (item.estado == 4) {
            color = "orange"; // Proceso de entrega
          } else if (item.estado == 5) {
            color = "#8C0095"; // Camino a su destino
          } else if (item.estado == 6) {
            color = "green"; // Orden entregada
          } else if (item.estado == 8) {
            color = "red"; // Orden Cancelada
          } else {
            color = "red"; //Orden declinada
          }

          cop +=
            `<li style="padding-bottom: 10px;color:  ` +
            color +
            `;border-radius: 10px;padding: 2px;padding-left: 10px;">` +
            item.estados +
            `</li>`;
          cap += `<li style="padding-bottom: 5px;">Orden #` + item.id + `</li>`;

          html +=
            `<div class="row popup-open" onclick="detalleOrden(` +
            item.id +
            `);" style="border: 1px solid #FBB540;border-radius: 5px;padding: 10px;margin-bottom: 10px" data-popup=".popup-about">
          <div class="col-40">
          <div class="item-title" style="font-weight:550;font-size:13px;color:black">
          Número de orden #` +
            item.id +
            `</div>
          <div class="item-after" style="font-size: 12px;padding: 5px 0 5px 0;">Fecha y hora:</div>
          <div class="item-subtitle" style="white-space: unset;font-size: 10px;color: #757575;padding: 0 0 5px 0;">
          ` +
            item.fecha +
            `
          </div>
          <div class="item-after" style="font-size: 12px;padding: 0 0 5px 0;">Conductor Asignado:</div>
          <div class="item-subtitle" style="white-space: unset;font-size: 10px;color: #757575;padding: 0 0 5px 0;">
          ` +
            chofer +
            `
          </div>
          </div>
          <div class="col-60">
          <button class="col button button-fill color-` +
            color +
            `" style="font-size: 10px;line-height: 20px;height: 20px;margin-top: 17px;margin-bottom: -6px;font-weight: 400">` +
            item.estados +
            `</button>
          <p style="font-size: 15px;text-align: center;margin-bottom: -14px;">Precio:</p>
          <p style="color: #FBB540;text-align: center;font-weight: bold;font-size: 18px">US$ ` +
            item.costo_total +
            `</p>
          </div>
          </div>`;
        });
      }
      $$("#ulordenes").html(html);
      $$("#idord").html(cap);
      $$("#statord").html(cop);
      $$("#preloaderStatord").hide();
    },
    error: function (xhr) {
      console.log(xhr);
    },
  });
}

//Funcion del valor del tab de direcciones
function tabsDireccion(id) {
  if (id == 1) {
    $$("#type").val("Hogar");
  } else if (id == 2) {
    $$("#type").val("Oficina");
  } else {
    $$("#type").val("Otros");
  }
}

//Funcion del valor del tab de pagos
function tabsPagos(id) {
  if (id == 1) {
    $$("#tipo_tarjeta").val("Credito");
  } else {
    $$("#tipo_tarjeta").val("Debito");
  }
}

function direcciones() {
  var id = getParameterByName("id");
  console.log(id);
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/api/direcciones.php?id=34",
    beforeSend: function (xhr) {},
    success: function (data) {
      console.log(data);
      var html = "";
      html += '<div class="list"><ul>';
      $.each(data, function (i, item) {
        if (item.tipo == "Hogar") {
          var type = "img/direcciones/location.png";
          var di = "Hogar";
        } else if (item.tipo == "Oficina") {
          var type = "img/direcciones/business.png";
          var di = "Oficina";
        } else {
          var type = "img/direcciones/cloud.png";
          var di = "Otro";
        }

        html += '<li class="swipeout deleted-callback">';
        html +=
          '<input type="hidden" id="IdDirecciones" value="' + item.id + '">';
        html += '<a href="#" class="item-link item-content swipeout-content">';
        html += '<div class="item-inner">';
        html += '<div class="item-title" style="white-space:normal">';
        html +=
          '<div class="item-header" style="color:#d02122;">' + di + "</div>";
        html += "" + item.calle + ", numero(" + item.numero + ")";
        html += "</div>";
        html += "</div>";
        html += "</a>";
        html += '<div class="swipeout-actions-right">';
        html +=
          '<a href="#" class="swipeout-delete" onclick="borrarDireccion(' +
          item.id +
          ')">';
        html += ' <i class="f7-icons">trash</i>';
        html += "</a>";
        html += "</div>";
        html += "</li>";

        $$("#direcciones").html(html);
      });
      html += "</div></ul>";
    },
    error: function () {
      app.preloader.hide();
    },
  });
}

function limpiarCarritoError() {
  console.log("limpiarCarritoError function is active");
  var id = getParameterByName("id"); //ID cliente
  app.request({
    type: "DELETE",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/carrito/limpiar/" + id,
    beforeSend: function (xhr) {
      app.preloader.show();
    },
    success: function (data) {
      app.preloader.hide();
      carrito();
      console.log(data);
    },
    error: function () {
      app.preloader.hide();
      carrito();
    },
  });
}

function carrito() {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  var id = getParameterByName("id"); //ID cliente
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/carrito/" + id,
    beforeSend: function (xhr) {
      app.preloader.show();
    },
    success: function (data) {
      app.preloader.hide();
      var html = "";
      var subTotal = 0;
      var cantidad = 0;
      var taxTotalItemMunicipal = 0;
      var taxTotalItemState = 0;

      if (data == "" || data == "[]") {
        $$("#info").hide(); // Informacion del total, subtotal y servicio.
        $$("#buttonOrden").hide(); // Boton del carrito.
        $$(".resumenPedido").hide(); // titulo resumen del pedido.
        html += "<center>";
        html +=
          "<img src='img/carritovacio.png' style='width: 160px;height: 160px;margin-top: 50px;'>";
        html += "</center>";
        html +=
          "<p style='color:black;font-size:16px;text-align:center;'>No hay articulos en el carrito.</p>";
      } else {
        $.each(data, function (i, item) {
          // Impuesto Municipal
          var taxItemMunicipal =
            (parseFloat(item.taxMunicipal) / 100) *
            parseFloat(item.precio) *
            parseInt(item.cantidad);
          taxTotalItemMunicipal +=
            (parseFloat(item.taxMunicipal) / 100) *
            parseFloat(item.precio) *
            parseInt(item.cantidad);

          // Impuesto State
          var taxItemState =
            (parseFloat(item.taxState) / 100) *
            parseFloat(item.precio) *
            parseInt(item.cantidad);
          taxTotalItemState +=
            (parseFloat(item.taxState) / 100) *
            parseFloat(item.precio) *
            parseInt(item.cantidad);

          $$("#TaxMunicipalT").val(taxTotalItemMunicipal);
          $$("#TaxStateT").val(taxTotalItemState);

          console.log(
            "Municipal: " +
            taxTotalItemMunicipal +
            " State: " +
            taxTotalItemState
          );

          subTotal += parseInt(item.cantidad) * parseFloat(item.precio);
          var precioCart = parseInt(item.cantidad) * parseFloat(item.precio);
          var total = subTotal;
          $$("#subTotal").html(
            "US$ " + formatter.format(subTotal).replace("$", "")
          );
          $$("#totalCarrito").html(
            "US$ " + formatter.format(total).replace("$", "")
          );
          $$("#totalInput").val(total); // Monto total.
          $$("#idRestaurantes").val(item.idRestaurante); // ID del restaurante.
          $$("#info").show(); // Informacion del total, subtotal y servicio.
          $$(".resumenPedido").show();
          $$("#buttonOrden").show(); // Boton del carrito.
          var foto = "";
          if (item.foto == null || item.foto == "") {
            foto = "img/eat.png";
          } else {
            foto = item.foto;
          }

          if (item.tipo == 1) {
            console.log("Mostrar ocultar");
            var ti =
              `<i class="f7-icons sheet-open editItemss" style="color: green;float: right;font-size: 18px;margin-right: 6px;" data-sheet=".edit-sheet" onclick="EditarItem(` +
              item.id +
              `,` +
              item.tipo +
              `)">pencil</i>`;
          } else {
            var ti = ``;
          }

          html +=
            `<div class="row" style="border-bottom: 1px solid #ff8000;margin-bottom: 10px;border-top: 1px solid #ff8000;margin-left: -15px;margin-right: -15px;padding: 10px;">
            <div class="col-30">
            <img src="https://sys.eatsoonpr.com/` +
            foto +
            `" style="width: 100%;height:100%;">
            </div>
            <div class="col-70">
            <i class="f7-icons" style="color: red;float: right;font-size: 18px;" onclick="borrarItem(` +
            item.id +
            `)">xmark</i>
            ` +
            ti +
            `
            <div class="item-title" style="font-weight:550;font-size:13px;color:#ff8000">
            ` +
            item.item +
            `</div>
            <div class="item-after" style="font-size: 10px;">Detalle: </div>
            <div class="item-subtitle" style="white-space: unset;font-size: 10px;color: #757575;padding-right: 10px;text-align: justify;">
            ` +
            item.descripcion +
            `</div>
            
            <div class="stepper stepper-fill stepper-round stepper-init" style="height: 20px !important;margin-top: 12px;float: left;">
            <div class="stepper-button-minus" style="height: 15px;width: 15px;border-radius: 100%;display: none;"></div>
            <div class="stepper-input-wrap" style="border-bottom: 0;border-top: 0">
            <input type="text" value="Cant. ` +
            item.cantidad +
            `" min="0" max="100" step="1" readonly style="height: 70%;width: 47px;font-size: 13px;color:#ff8000">
            </div>
            <div class="stepper-button-plus" style="height: 15px;width: 15px;border-radius: 100%;display: none;"></div>
            </div>
            <!-- Precio -->
            <p style="font-size: 13px;padding-left: 5%;margin-top: 10px;color: #ff8000;text-align: center;float: left;
            font-weight: 600;">Precio: US$ ` +
            formatter.format(precioCart).replace("$", "") +
            `</p>
            </div>
            </div>`;
        });
      }
      $$("#carito").html(html);
    },
    error: function () {
      app.preloader.hide();
      console.log("hay bobo");
      $$(".resumenPedido").hide();
    },
  });
}

function imgPayment(tipo, numero, mes, ano, cvv) {
  var number = $$("#number").val(numero);
  var mes = $$("#mes").val(mes);
  var ano = $$("#ano").val(ano);
  var cvv = $$("#cvv").val(cvv);
}

function clickPayment() {
  //ID cliente
  var id = getParameterByName("id");
  console.log(id);
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/api/selectPago.php?id=" + id,
    beforeSend: function (xhr) {
      app.preloader.show();
    },
    success: function (data) {
      app.preloader.hide();
      console.log(data);
      var html = "";
      if (data == "" || data == "[]") {
        console.log("no tiene metodo de pago guardado.");
        app.dialog.alert("no tiene metodo de pago guardado.");
        $$("#nonePago").show();
      } else {
        html += '<option value="">Seleccionar pago</option>';
        $.each(data, function (i, item) {
          if (item.tipo == "VISA") {
            var type = "img/pagos/visa22.png";
            var di = "Visa";
          } else if (item.tipo == "AMEX") {
            var type = "img/pagos/amex22.png";
            var di = "Amex";
          } else {
            var type = "img/pagos/mastercard22.png";
            var di = "MasterCard";
          }

          var num = item.numero;
          var res = num.substr(14, 14);

          html +=
            '<option value="' +
            item.id +
            '" data-option-image="' +
            type +
            '" onclick="imgPayment(' +
            item.tipo +
            "," +
            item.numero +
            "," +
            item.mes +
            "," +
            item.ano +
            "," +
            item.cvv +
            ')">**** ' +
            res +
            "</option>";
          $$("#tarjeta").html(html);
        });
      }
    },
    error: function () {
      app.preloader.hide();
    },
  });
}

//////////////////////////////////
//////////CERRAR SESION////////////
//////////////////////////////////
$$("#logout").on("click", function () {
  var id = getParameterByName("id");
  $.ajax({
    type: "GET",
    url: "https://laundryappmovil.com/api-nes/api/logout.php?id=" + id,
    beforeSend: function (xhr) {
      app.dialog.preloader("Cerrando Sesion...");
    },
    success: function (data) {
      app.dialog.close();
      window.localStorage.setItem("login", 0);
      localStorage.removeItem("usuario");
      localStorage.removeItem("password");
      location.href = "index.html";
    },
    error: function () {
      app.dialog.close();
    },
  });
});

/*******************************************/
/*******************************************/
/*******************************************/
/*******************************************/
/*******************************************/
function autoLogin() {
  console.log(window.localStorage.getItem("login"));

  if (window.localStorage.getItem("login") == 1) {
    $$("#auto-log").hide();
    $$("#login-wrap").show();
    var username = localStorage.getItem("username");
    var password = localStorage.getItem("password");

    $.ajax({
      type: "POST",
      url: "https://laundryappmovil.com/api-nes/public/api/login",
      //crossDomain: true,
      data: {
        email: username,
        password: password,
      },
      statusCode: {
        404: function (xhr) {
          //alert("error 404");
        },
        200: function (xhr) {
          //alert("Site is UP");
        },
      },
      beforeSend: function (xhr) {
        //app.dialog.preloader('Iniciando Sesión');
      },
      success: function (data) {
        $$("#auto-log").hide();
        //app.dialog.preloader('Iniciando Sesión');

        if (data == 1) {
          app.dialog.close();
          app.dialog
            .create({
              title: "Doctor Click",
              text: "contraseña incorrecta.",
              buttons: [{
                text: "OK"
              }],
              verticalButtons: true,
            })
            .open();
        } else if (data == 2) {
          app.dialog.close();
          app.dialog
            .create({
              title: "Doctor Click",
              text: "Usuario o correo incorrecto.",
              buttons: [{
                text: "OK"
              }],
              verticalButtons: true,
            })
            .open();
        } else {
          //Extraer los valores del JSON
          var obj = JSON.parse(JSON.stringify(data));
          console.log(obj.nombre + " " + obj.apellido);

          window.localStorage.setItem("id", obj.id_cliente);
          window.localStorage.setItem("login", 1);
          window.localStorage.setItem("username", username);
          window.localStorage.setItem("password", password);

          window.plugins.OneSignal.getIds(function (ids) {
            var token = ids.userId;
            $.ajax({
              url: "https://laundryappmovil.com/api-nes/api/cliente_token.php?token=" +
                token +
                "&id=" +
                obj.id_cliente,
              method: "GET",
              success: function (resp) {
                console.log(resp);
              },
            });
            console.log(token);
          });

          setTimeout(function () {
            app.dialog.close();
            location.href =
              "home.html?id=" +
              obj.id_cliente +
              "&nombre=" +
              obj.nombre +
              "&apellido=" +
              obj.apellido +
              "&usuario=" +
              obj.usuario +
              "&password=" +
              password +
              "&celular=" +
              obj.celular +
              "&telefono=" +
              obj.telefono +
              "&correo=" +
              obj.email + "&formulario=" + obj.formulario;
          }, 2000);
        }
      },
      error: function () {
        app.dialog.close();
        app.dialog
          .create({
            title: "Doctor Click",
            text: "No tiene acceso a internet.",
            buttons: [{
              text: "OK"
            }],
            verticalButtons: true,
          })
          .open();

        app.dialog.confirm("Quieres reintentar conectarte?", function () {
          autoLogin();
          logout();
        });
      },
    });
  }
}

/***********************************************************/
/***********************************************************/
/***********************************************************/
/***********************************************************/
/***********************************************************/
/**************AREA DE MANTENIMIENTOS DE LA APP*************/
function slideHome() {
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/slidehome",
    beforeSend: function (xhr) {},
    success: function (data) {
      var html = "";
      $.each(data, function (i, item) {
        html +=
          `<div class="swiper-slide" style="height: 200px;width:250px;">
       <img src="http://laundryappmovil.com/api-nes/` +
          item.imagen +
          `" style="width: 100%;border-radius: 10px;">
       </div>`;
        $$("#swiperHomeIMg").html(html);
        var swiper = new Swiper(".estoes", {
          spaceBetween: 30,
          centeredSlides: true,
          autoplay: {
            delay: 2500,
            disableOnInteraction: false,
          },
          pagination: {
            el: ".swiper-pagination",
            clickable: true,
          },
        });
      });
    },
    error: function (xhr) {
      console.log(xhr);
      app.preloader.hide();
    },
  });
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function categoria() {
  app.request.json(
    "https://laundryappmovil.com/api-nes/public/api/categoria",
    function (data) {
      console.log(data);
      var html = "";
      $.each(data, function (i, item) {
        html +=
          `<div class="col-25" onclick="restCat(` +
          item.id +
          `)">
     <center><img src="https://sys.eatsoonpr.com/` +
          item.imagen +
          `" style="width: 40px;margin-top:20px;"></center>
     <p style="text-align: center;margin: 0;font-size:3vw;">` +
          item.categoria +
          `</p>
     </div>`;
        $$("#categoriasHGome").html(html);
      });
    }
  );
}

function restCat(id) {
  app.views.main.router.navigate("/busquedaCategoria/" + id);
}

function goProfileCentro(id, cerrado, idTypeCent, dependiente, titular, lat, long) {
  app.views.main.router.navigate("/hospital-profile/" + id + "/" + idTypeCent + "/" + dependiente + "/" + titular + "/" + lat + "/" + long);
}

function erroGoMenu() {
  app.dialog.alert("Este especialista no esta disponinble");
}

function goMenuLogin(id) {
  app.views.main.router.navigate("/detail-restaurante/" + id);
}

function tiposcentros(id, iddependiente, titular) {
  console.log(
    "id: " + id,
    " dependiente: ",
    iddependiente,
    " titular: ",
    titular
  );
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/centro/categoria/" + id,
    beforeSend: function (xhr) {},
    success: function (data) {
      app.preloader.hide();
      console.log(data);
      var html = "";
      if (data == null || data == "" || data == "[]") {
        html += `<li style="text-align:center;">No disponible</li>`;
      } else {
        $.each(data, function (i, item) {
          if (item.logo == null || item.logo == "") {
            var logoResult = "img/notabalible.jpg";
          } else {
            var logoResult = item.logo;
          }
          if (item.estatus == "Cerrado") {
            html +=
              `<li>
            <a href="#" class="item-link item-content goHospitalProfile" style="opacity:0.5;" onclick="erroGoMenu()">
            <div class="item-media">
            <img src="./img/cedimat-turismo-de-salud.jpg" width="80"/>
            </div>
            <div class="item-inner">
            <div class="item-title-row">
            <div class="item-title">` +
              item.centro +
              `</div>
            </div>
            <div class="item-subtitle">Abre: ` +
              formatAMPMe(item.openTime) +
              ` Cierra: ` +
              formatAMPMe(item.closedTime) +
              `</div>
            <div class="item-text">` +
              item.direccion +
              `</div>
            </div>
            </a>
            </li>`;
          } else {
            html +=
              `<li>
          <a href="#" onclick="goProfileCentro(` +
              item.id +
              "," +
              item.cerrado +
              "," +
              id +
              "," +
              iddependiente +
              "," +
              titular +
              "," +
              item.lat +
              "," +
              item.lng +
              `)" class="item-link item-content goHospitalProfile">
          <div class="item-media">
          <img src="` +
              item.logo +
              `" width="80"/>
          </div>
          <div class="item-inner">
          <div class="item-title-row">
          <div class="item-title">` +
              item.centro +
              `</div>
          </div>
          <div class="item-subtitle">Abre: ` +
              formatAMPMe(item.openTime) +
              ` Cierra: ` +
              formatAMPMe(item.closedTime) +
              `</div>
          <div class="item-text">` +
              item.direccion +
              `</div>
          </div>
          </a>
          </li>`;
          }
        });
      }
      $$("#centroPopulares").html(html);
    },
    error: function () {
      app.preloader.hide();
    },
  });
}

function calificacion() {
  var id_cliente = getParameterByName("id");
  $$("#calificarServi").html("Calificar Servicio");
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/calificaciones/" +
      id_cliente,
    success: function (data) {
      console.log(data);
      var html = "";
      if (data == null || data == "[]" || data == "") {
        html += `<li style="text-align:center;padding:10px;">No hay ordenes para calificar</li>`;
      } else {
        $.each(data, function (i, item) {
          html +=
            `
              <li>
              <a href="#" class="item-link item-content sheet-open" onclick="detalleCalificar(` +
            item.task_id +
            `,` +
            item.idRestaurante +
            `)" data-sheet=".my-sheet-swipe-to-close">
              <div class="item-media">
              <img src="img/star.png" style="width: 28px">
              </div>
              <div class="item-inner">
              <div class="item-title">Orden #` +
            item.task_id +
            `</div>
              <div class="item-after">` +
            item.fecha +
            `</div>
              </div>
              </a>
              </li>
              `;
        });
      }
      $$("#calificacionHTML").html(html);
    },
    error: function (xhr) {
      console.log(xhr);
    },
  });
}

function detalleCalificar(id_tarea, idRestaurante) {
  console.log("Esto funicona jevi " + id_tarea);
  $$("#OrdenCali").html("<b>Calificar la orden " + id_tarea + "</b>");

  $$("#sendCali").on("click", function () {
    var comentario = $$("#comentarioStar").val();
    var estrellas = $$(".estrellas").val();
    var id = getParameterByName("id");

    app.request({
      type: "POST",
      data: {
        restaurante: idRestaurante,
        comentario: comentario,
        estrellas: estrellas,
        id_tarea: id_tarea,
      },
      url: "https://laundryappmovil.com/api-nes/public/api/calificacion/add",
      success: function (data) {
        console.log(data);
        $$("#comentarioStar").val("");
        calificacion();
        tiposcentros();
      },
      error: function (xhr) {
        console.log(xhr);
      },
    });
  });
}

function ofertas() {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/ofertas",
    beforeSend: function (xhr) {
      //app.preloader.show();
    },
    success: function (data) {
      app.preloader.hide();
      console.log(data);
      var html = "";
      $$("#ofertasParTi").html("Ofertas para ti");
      if (data == "" || data == null || data == "[]") {
        html += `<li style="text-align:center;margin-top: 20px;">No disponinble</li>`;
        $$("#cardOfert").css({
          height: "70px"
        });
      } else {
        $$("#cardOfert").css({
          height: "auto"
        });
        $.each(data, function (i, item) {
          html +=
            `<li class="item-link item-content" onclick="goMenu(` +
            item.restaurante +
            `,` +
            item.cerrado +
            `)">
        <div class="item-media">
        <img src="https://sys.eatsoonpr.com/` +
            item.foto +
            `" style="width: 100px;border-radius: 10px;" />
        </div>
        <div class="item-inner">
        <div class="item-title-row">
        <div class="item-title">` +
            item.nombre +
            `</div>
        </div>
        <div class="item-subtitle" style="color: #2D652B;">Price : US$ ` +
            formatter.format(item.precio).replace("$", "") +
            `</div>
        </div>
        </li>`;
        });
      }
      $$("#OfertasItems").html(html);
    },
    error: function () {
      app.preloader.hide();
    },
  });
}

function validEmail(email) {
  app.request({
    type: "POST",
    url: "https://laundryappmovil.com/api-nes/api/emailTest.php",
    data: {
      email: email,
    },
    success: function (resp) {
      app.dialog.close();
      if (resp == 1) {
        app.dialog.alert("Este correo ya existe");
        $$("#save-user").prop("disabled", true);
        $$("#save-user").addClass("dis");
      } else {
        $$("#save-user").removeClass("dis");
        $$("#save-user").prop("disabled", false);
      }
    },
    error: function (xhr) {
      console.log(xhr);
    },
  });
}

function UserNameTest(username) {
  app.request({
    type: "POST",
    url: "https://laundryappmovil.com/api-nes/api/userTest.php",
    data: {
      username: username,
    },
    success: function (resp) {
      app.dialog.close();
      if (resp == 1) {
        app.dialog.alert("Este usuario ya existe");
        $$("#save-user").prop("disabled", true);
        $$("#save-user").addClass("dis");
      } else {
        $$("#save-user").removeClass("dis");
        $$("#save-user").prop("disabled", false);
      }
    },
    error: function (xhr) {
      console.log(xhr);
    },
  });
}

function logout() {
  var id = getParameterByName("id");
  app.request({
    type: "GET",
    url: "https://laundryappmovil.com/api-nes/api/logout.php?id=" + id,
    beforeSend: function (xhr) {
      app.dialog.preloader("Cerrando Sesion...");
    },
    success: function (data) {
      app.dialog.close();
      window.localStorage.setItem("login", 0);
      localStorage.removeItem("usuario");
      localStorage.removeItem("password");
      location.href = "index.html";
    },
    error: function () {
      app.dialog.close();
    },
  });
}

function sumar() {
  console.log("se ejecuto esto");
  var valor1 = $$(".valueTaxStateX").val();
  var valor2 = $$(".valueTaxMunicipalX").val();
  var valor3 = $$(".titol").val();
  var valor4 = $$(".deliveryServInputX").val();
  //var valor5 = $$('.valueTaxMenuX').val();

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  //var porcentajeMenuTax = (parseFloat(valor5) / 100) * parseFloat(valor3);
  /*var porcentajeStateX = (parseFloat(valor1) / 100) * parseFloat(valor3);
  var porcentajeMunicipalX = (parseFloat(valor2) / 100) * parseFloat(valor3);*/

  console.log(valor1 + "+" + valor2 + "+" + valor3 + "+" + valor4);

  var total =
    parseFloat(valor1) +
    parseFloat(valor2) +
    parseFloat(valor3) +
    parseFloat(valor4);
  document.getElementById("spTotal").innerHTML =
    "US$ " + formatter.format(total).replace("$", "");
}

function formatAMPMe(date) {
  var hours = date.substr(0, 2); //extraer hora
  var minutes = date.substr(3, 3); //extraer minutos
  var ampm = hours >= 12 ? "pm" : "am";
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? "0" + minutes : minutes;
  var strTime = hours + ":" + minutes + " " + ampm;
  return strTime;
}

function recordarUsuario() {
  // Recordar pass
  var TicUser = window.localStorage.getItem("UserLogin");
  var TicPassword = window.localStorage.getItem("PasswordLogin");
  console.log("Usuario gurdado " + TicUser + " Pass guardad " + TicPassword);
  $$("#email").val(TicUser);
  $$("#pass").val(TicPassword);

  if (TicUser == null || TicPassword == null) {
    document.getElementById("checkUserRec").checked = false;
  } else {
    document.getElementById("checkUserRec").checked = true;
  }
}

// Listados de dependientes del cliente.
function listDependientes() {
  var id = getParameterByName("id");
  app.request({
    type: "GET",
    dataType: "json",
    url: "http://laundryappmovil.com/api-nes/public/api/dependientes/" + id,
    beforeSend: function (xhr) {
      //app.dialog.preloader('Cargando...');
    },
    success: function (data) {
      //app.dialog.close();
      console.log(data);
      var html = "";
      $.each(data, function (i, item) {
        html +=
          `<li class="swipeout">
         <div href="#" class="item-content swipeout-content">
         <div class="item-inner">
         <div class="item-title">
          <div class="item-header">` + item.parentesco + `</div> ` + item.nombre + ` ` + item.apellido + `
         </div>
         <div class="item-after">Edad ` + item.edad + `</div>
         </div>
         </div>
         <div class="swipeout-actions-right">
         <a href="#" class="open-more-actions" onclick="editarDependiente(` +
          item.id +
          `)">Editar</a>
         <a href="#" class="swipeout-delete" onclick="borrarDependiente(` +
          item.id +
          `)"><i class="f7-icons">trash</i></a>
         </div>
         </li>`;
        $$("#parentsList").html(html);
      });
    },
    error: function (xhr) {
      app.dialog.close();
    },
  });
}

//Funcion de borrar dependientes
function borrarDependiente(id) {
  console.log("klk borro este dependiente " + id);
  app.dialog.confirm("Seguro que quieres borrar este dependiente?",
    function () {
      app.request({
        type: "delete",
        url: "http://laundryappmovil.com/api-nes/public/api/dependientes/delete/" +
          id,
        beforeSend: function () {
          console.log("Borrando dependiente...");
        },
        success: function (resp) {
          console.log(resp);
          app.dialog.close();
          listDependientes();
          app.dialog.alert("Dependiente borrado");
        },
        error: function (xhr) {
          console.log(xhr);
          listDependientes();
        },
      });
    }
  );
}

function editarDependiente(id) {
  app.views.main.router.navigate("/add_dependientes/" + id);
}

function listadoParent() {
  app.request.json("http://laundryappmovil.com/api-nes/public/api/parentesco",
    function (data) {
      console.log(data);
      var html = "";
      html += `<option value="">Seleccionar</option>`;
      $.each(data, function (i, item) {
        html += `<option value="` + item.id + `">` + item.parentesco + `</option>`;
      });
      $$("#parentescoID").html(html);
    }
  );
}

function editParent(id) {
  app.request.json("http://laundryappmovil.com/api-nes/public/api/dependientes/edit/" + id,
    function (data) {
      console.log(data);
      $.each(data, function (i, item) {
        $$("#nombreDependiente").val(item.nombre);
        $$("#apellidoDependiente").val(item.apellido);
        $$("#edadDependiente").val(item.edad);
        $$("#parentescoID").val(item.id_parentesco);
        $$("#idParent").val(item.id);
      });
    }
  );
}

function listadoDoctores(id) {
  app.request.json("https://laundryappmovil.com/api-nes/public/api/doctores/profile/" + id,
    function (data) {
      console.log(data);
      $.each(data, function (i, item) {
        $$("span").removeClass("skeleton-text");
        $$("span").removeClass("skeleton-effect-pulse");
        $$(".NameLastDoc").text(item.nombre + " " + item.apellido);
        $$(".NameDoc").text(item.nombre);
        $$(".LastNameDoc").text(item.apellido);
        $$(".EdadDoc").text(item.edad + " Años");
        $$(".EspecDoc").text(item.especialidades);
        var tel = item.telefono;
        var res = tel.substr(0, 3);
        var res1 = tel.substr(3, 3);
        var res2 = tel.substr(6, 8);
        $$(".TelDoc").text("(" + res + ") " + res1 + "-" + res2);
      });
    }
  );
}

function profileCentro(id) {
  app.request.json("https://laundryappmovil.com/api-nes/public/api/centro/" + id,
    function (data) {console.log(data);
      $.each(data, function (i, item) {
        $$("span").removeClass("skeleton-text");
        $$("span").removeClass("skeleton-effect-pulse");
        $$(".NameCent").html(item.centro);
        $$(".DirecCent").html(item.direccion);
        $$(".LoadPhotoProfile").html( '<img src="' + item.logo + '" width="80"/>');

        $$(".HorarioCent").html(formatAMPMe(item.openTime) + " - " + formatAMPMe(item.closedTime));

        var img_url = "https://maps.googleapis.com/maps/api/staticmap?center=" + item.direccion + "&zoom=14&size=400x400&markers=color:red%7Clabel:C%7C" + item.direccion + "&key=AIzaSyDU3X8kZBJME8-y_8cBgfXB0Hvw0VEsR3g";

        $$(".locationImg").html( '<img src="' + img_url + '" style="width: 100%;border-radius: 20px;">' );

        var tel = item.telefono;
        var res = tel.substr(0, 3);
        var res1 = tel.substr(3, 3);
        var res2 = tel.substr(6, 8);
        $$(".TelDoc").html("(" + res + ") " + res1 + "-" + res2);

        $$(".TelDoc").on('click', function(){
          document.location.href = "tel:+"+item.telefono;
        });
      });
    }
  );
}

function citas() {
  var id = getParameterByName("id");
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/citas/" + id,
    beforeSend: function () {},
    success: function (resp) {
      console.log(resp);
      let htmlNuevas = "", htmlDependiente = "", htmlEditCancel = "", 
      htmlOpcion2 = "", htmlOpcion3 = "", htmlFinCancel = "";
      $.each(resp, function (i, item) {
        var res = item.fecha_creacion.substr(0, 10);
        var res1 = item.hora.substr(0, 5);
        var titulo;
        var especialista = "";

        if (item.doctor == "" || item.doctor == null) {
          especialista = "";
        } else {
          especialista = "Especialista: " + item.doctor;
        }

        if (item.centros == "" || item.centros == "[]" || item.centros == "null" || item.centros == null) {
          titulo = "Cita para dependiente";
        } else {
          titulo = item.centros;
        }
        
        //CITA DE DEPENDIENTE
        htmlDependiente += `<li>
          <a href="/detalle/` + item.id + "/" + id + "/3" + "/" + item.centro + `" class="item-link item-content">
            <div class="item-inner">
              <div class="item-title-row">
              <div class="item-title" style="font-size: 14px;">` + titulo + `</div>
              <div class="item-after" style="font-size: 14px;">` + res + `</div>
            </div>
            <div class="info" style="display: flex;">
              <div class="item-subtitle" style="font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">alarm_fill</i> ` + item.fecha + `</div>
              <div class="item-subtitle" style="margin-left: 10px; font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">calendar</i> ` + res1 + `</div>
            </div>
            <div class="item-text" style="font-size: 12px;">` + especialista + `</div>
            </div>
          </a>
        </li>`;

        if(item.estatus == 1){ //CITAS NUEVAS DISPONIBLES PARA EDITAR O CANCELAR
          htmlEditCancel += `<li>
            <a href="/detalle/` + item.id +"/" + id +"/2" +"/" + item.centro +`" class="item-link item-content">
              <div class="item-inner">
                <div class="item-title-row">
                  <div class="item-title" style="font-size: 14px;">` + titulo + `</div>
                  <div class="item-after" style="font-size: 14px;">` + res + `</div>
                </div>
                <div class="info" style="display: flex;">
                  <div class="item-subtitle" style="font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">alarm_fill</i> ` + item.fecha + `</div>
                  <div class="item-subtitle" style="margin-left: 10px; font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">calendar</i> ` + res1 + `</div>
                </div>
                <div class="item-text" style="font-size: 12px;">` + especialista + `</div>
              </div>
            </a>
          </li>`;

          htmlNuevas += `<li>
            <a href="/detalle/` + item.id +"/" + id +"/1" +"/" + item.centro +`" class="item-link item-content">
              <div class="item-inner">
                <div class="item-title-row">
                  <div class="item-title" style="font-size: 14px;">` + titulo + `</div>
                  <div class="item-after" style="font-size: 14px;">` + res + `</div>
                </div>
                <div class="info" style="display: flex;">
                  <div class="item-subtitle" style="font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">alarm_fill</i> ` + item.fecha + `</div>
                  <div class="item-subtitle" style="margin-left: 10px; font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">calendar</i> ` + res1 + `</div>
                </div>
                <div class="item-text" style="font-size: 12px;">` + especialista + `</div>
              </div>
            </a>
          </li>`;
        } else if(item.estatus == 2){ //CITAS NUEVAS Y ACEPTADAS
          console.log('entro en nuevas');
          htmlNuevas += `<li>
            <a href="/detalle/` + item.id +"/" + id +"/1" +"/" + item.centro +`" class="item-link item-content">
              <div class="item-inner">
                <div class="item-title-row">
                  <div class="item-title" style="font-size: 14px;">` + titulo + `</div>
                  <div class="item-after" style="font-size: 14px;">` + res + `</div>
                </div>
                <div class="info" style="display: flex;">
                  <div class="item-subtitle" style="font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">alarm_fill</i> ` + item.fecha + `</div>
                  <div class="item-subtitle" style="margin-left: 10px; font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">calendar</i> ` + res1 + `</div>
                </div>
                <div class="item-text" style="font-size: 12px;">` + especialista + `</div>
              </div>
            </a>
          </li>`;
        } else if(item.estatus == 5){ //SOLICITUD CAMBIO DE FECHA DE CITA - OPCION 2
          htmlOpcion2 += `<li>
            <a href="/detalleSolicitud/` + item.id +"/" + item.id_doctor +`" class="item-link item-content">
              <div class="item-inner">
                <div class="item-title-row">
                  <div class="item-title" style="font-size: 14px;">` + titulo + `</div>
                  <div class="item-after" style="font-size: 14px;">` + res + `</div>
                </div>
                <div class="info" style="display: flex;">
                  <div class="item-subtitle" style="font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">alarm_fill</i> ` + item.fecha + `</div>
                  <div class="item-subtitle" style="margin-left: 10px; font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">calendar</i> ` + res1 + `</div>
                </div>

                <div class="item-text" style="font-size: 12px;">` + especialista + `</div>
              </div>
            </a>
          </li>`;
        } else if(item.estatus == 4){
          htmlOpcion3 += `<li>
            <a href="/detalleRechazo/` + item.id + '/' + item.centro + '/' + item.dependiente + `" class="item-link item-content">
              <div class="item-inner">
                <div class="item-title-row">
                  <div class="item-title" style="font-size: 14px;">` + titulo + `</div>
                  <div class="item-after" style="font-size: 14px;">` + res + `</div>
                </div>
                <div class="info" style="display: flex;">
                  <div class="item-subtitle" style="font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">alarm_fill</i> ` + item.fecha + `</div>
                  <div class="item-subtitle" style="margin-left: 10px; font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">calendar</i> ` + res1 + `</div>
                </div>
                <div class="item-text" style="font-size: 12px;">` + especialista + `</div>
              </div>
            </a>
          </li>`;
        } else if(item.estatus == 3 || item.estatus == 6){
          htmlFinCancel += `<li>
            <a href="/detalle/` + item.id +"/" + id +"/4" +"/" + item.centro +`" class="item-link item-content">
              <div class="item-inner">
                <div class="item-title-row">
                  <div class="item-title" style="font-size: 14px;">` + titulo + `</div>
                  <div class="item-after" style="font-size: 14px;">` + res + `</div>
                </div>
                <div class="info" style="display: flex;">
                  <div class="item-subtitle" style="font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">alarm_fill</i> ` + item.fecha + `</div>
                  <div class="item-subtitle" style="margin-left: 10px; font-size: 12px;"><i class="f7-icons" style="font-size: 15px; color: #0F7BC3;">calendar</i> ` + res1 + `</div>
                </div>
                <div class="item-text" style="font-size: 12px;">` + especialista + `</div>
              </div>
            </a>
          </li>`;
        }

        $$(".listDeCitasAccion").html(htmlEditCancel);
        $$(".citasdependiente").html(htmlDependiente);
        $$(".listDeCitasRealizad").html(htmlNuevas);
        $$(".listDeCitasSolicitud").html(htmlOpcion2);
        $$(".listDeCitasRechazadas").html(htmlOpcion3);
        $$(".listDeCitasFinCancel").html(htmlFinCancel);
      });
    },
    error: function (xhr) {
      console.log(xhr);
    },
  });
}

$$(document).on("page:init", '.page[data-name="detalleSolicitud"]', function (e) {
  console.log('[DC | Detalle Nueva Solicitud - Opcion 2]');
  var idcita = app.view.main.router.currentRoute.params.idcita;
  var iddoctor = app.view.main.router.currentRoute.params.iddoctor;

  getDates(iddoctor);

  $$("#SendSoliOpcion2").on('click', function(){
    let fecha = $$("#fechaOpcion2").val();
    let hora = $$("#horaOpcion2").val();
    let today = new Date();
    var options = { year: 'numeric', month: 'numeric', day: 'numeric' };
    // console.log('typeof fecha:' + typeof fecha);
    // console.log('typeof today:' + typeof getFormattedDate(today));
    // console.log('fecha:' + fecha);
    // console.log('today:' + getFormattedDate(today));

    if(fecha == "" || hora == "") {
      app.dialog.alert("Por favor rellenar todos los campos.");
    } else if(fecha < getFormattedDate(today)) {
      app.dialog.alert("Por favor elegir una fecha válida.");
    } else {
      console.log('todo bien');
      // putDateOpcion2(idcita, fecha, hora);
    }
  });
});

function getDates(iddoctor){
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/diasExcluidos_Opcion2/"+iddoctor,
    beforeSend: function () {},
    success: function (resp) {
      console.log(resp);
      let disabledDates = [];
      let fecha = new Date();
      fecha.setDate(fecha.getDate() - 1);
      $.each(resp, function(i, item){
        disabledDates.push(new Date(item.fecha));
      });

      calendarDisabled = app.calendar.create({
        inputEl: '#fechaOpcion2',
        disabled: disabledDates
      });
    }, error: function(){}
  });
}

function putDateOpcion2(idcita, fecha, hora) {
  var newFecha = fecha.split('/').reverse().join('-');
  app.request({
    type: "PUT",
    url: "https://laundryappmovil.com/api-nes/public/api/actualizarFechaOpcion2/"+idcita + '/' + id,
    data: {
      "fecha": newFecha,
      "hora": hora
    },
    beforeSend: function () {},
    success: function (resp) {
      console.log(resp);
      app.dialog.alert("Cita actualizada correctamente");
      setTimeout(function () {
        app.views.main.router.navigate("/");
      }, 2000);
    }, error: function(err){
      console.log(err);
    }
  });
}

$$(document).on("page:init", '.page[data-name="detalleRechazo"]', function (e) {
  console.log('[DC | Detalle Cita Rechazada - Opcion 3]');
  
  var idcita = app.view.main.router.currentRoute.params.idcita;
  var idcentro = app.view.main.router.currentRoute.params.idcentro;
  var iddependiente = app.view.main.router.currentRoute.params.iddependiente;
  detalleCita(idcita, id, 3);

  $$("#SendSolidRechazo").on('click', (e) => {
    console.log("DC | Enviar solicitud en Rechazo");

    let fecha = $$("#fechaRechazo").val();
    let hora = $$("#horaRechazo").val();
    let especialista = $$("#ListEspecialistaRechazo").val();

    if (fecha == "" || hora == "" || especialista == "") {
      app.dialog.alert("Por favor rellenar todos los campos.");
    } else {
      cancelarCita(idcita, id, 3)
      sendSolid(fecha, hora, idcentro, especialista, iddependiente);
    }
  });

  $$("#SendSolidOtroCentro").on('click', () => {
    console.log("DC | Crear cita en otro centro");

    if(iddependiente == "null"){
      app.views.main.router.navigate("/programar-cita/" + id + "/1");
    } else {
      app.views.main.router.navigate("/programar-cita/" + iddependiente + "/2");
    }
  });
});

function detalleCita(cita, user, opcion) {
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/detalleCita/" + cita + "/" + user,
    beforeSend: function (xhr) {
      app.preloader.show();
    },
    success: function (data) {
      datos = data;
      console.log("data: " + data);
      app.preloader.hide();
      var paciente;
      $.each(data, function (i, item) {
        if (item.dependienten == "" || item.dependienten == "[]" || item.dependienten == null || item.dependienten == "null" || item.dependienten == "NULL") {
          paciente = item.cliente;
        } else {
          paciente = item.dependienten;
        }

        if (item.doctor == "" || item.doctor == "[]" || item.doctor == null || item.doctor == "null" || item.doctor == "NULL") {
          doctor = "No especificado";
        } else {
          doctor = item.doctor;
        }

        if ( item.centros == "" || item.centros == "[]" || item.centros == null || item.centros == "null" || item.centros == "NULL") {
          centro = "No especificado";
        } else {
          centro = item.centros;
        }

        if(opcion == 1){
          $$(".nombrecentro").text(paciente);
          $$(".fecha_creacion").text(item.fecha_creacion);
          $$(".fechaCita").html(item.fecha);
          $$(".centroCita").text(centro);
          $$(".horaCita").text(item.formatHora);
          $$(".especialistaCita").text(doctor);
          $$(".especialidadCita").text(item.especialidades);
          $$(".pacienteCita").text(item.cliente);
        } else if(opcion == 3){
          $$("#ListEspecialidadRechazo").html(`<option value="` + item.idEspecialidad + `" selected>` + item.especialidades + `</option>`);
          $$(".especialidadNombreRechazo").html(item.especialidades);
          especialistaDisponibles_opcion3(item.idEspecialidad, item.id_doctor);
          $$("#fechaRechazo").val(item.fecha);
          $$("#fechaRechazo").prop('disabled', true);
        }
        
      });
    },
    error: function (xhr) {
      console.log("error: " + xhr);
    },
  });
}

function especialidadesDrop() {
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/allEspecialidades",
    success: function (data) {
      var html = "";
      $.each(data, function (i, item) {
        html += `<option value="` + item.id + `">` + item.especialidades + `</option>`;
      });
      $$("#ListEspecialidad").html(html);
    },
    error: function (xhr) {
      console.log(xhr);
    },
  });
}

function verEspecialista(id) {
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/especialistaVer/" + id,
    success: function (data) {
      var html = "";
      $.each(data, function (i, item) {
        html += `<option data-option-image="../img/doctor.png" value="` + item.id_doctor + `">` + item.especialista + `</option>`;
      });
      $$("#ListEspecialista").html(html);
    },
    error: function (xhr) {
      console.log(xhr);
    },
  });
}

function especialistaDisponibles_opcion3(especialidad, especialista){
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/especialistaDisponibles_Opcion3/" + especialidad + "/" + especialista,
    success: function (data) {
      var html = "";
      $.each(data, function (i, item) {
        html += `<option value="` + item.id_doctor + `">` + item.especialista + `</option>`;
      });
      $$("#ListEspecialistaRechazo").html(html);
    },
    error: function (xhr) {
      console.log(xhr);
    },
  });
}

function consultSeguroM(idPaciente, titular) {
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/seguroM/" + idPaciente + "/" + titular,
    beforeSend: function (xhr) {
      console.log("cargando seguros medicos...");
      $$("#seguroM").html("");
    },
    success: function (data) {
      console.log(data);
      let html = "";
      if (data == "" || data == "[]") {
        html += `<div>
          <center>
          <p>Esta persona no tiene seguros medicos registrados.</p>
          </center>
        </div>`;
      } else {
        $.each(data, function (i, item) {
          html += `<div class="card" style="border-radius: 10px;">
            <div class="card-content card-content-padding" style="border-radius: 10px;background-color:#1ABDBD;">
              <div class="list" style="color: white">
                <ul>
                  <li>
                    <a href="/detalle-seguro/` + item.id + `" class="item-link item-content">
                      <div class="item-inner">
                        <div class="item-title">
                          <div class="item-header">Seguro</div>
                          ` + item.seguro + `
                        </div>
                        <div class="item-after" style="color: white">` + item.tipo + `</div>
                      </div>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>`;
        });
      }
      $$("#seguroM").html(html);
    },
    error: function (xhr) {
      console.log(xhr);
    },
  });
}

function consultDetalleSeguroM(seguro) {
  var id = getParameterByName("id");
  app.request({
    type: "GET",
    dataType: "json",
    url: "https://laundryappmovil.com/api-nes/public/api/detalleSeguroM/" + seguro + "/" + id,
    beforeSend: function (xhr) {
      app.preloader.show();
    },
    success: function (data) {
      app.preloader.hide();
      console.log("fgwdfsdf" + data);
      $.each(data, function (i, item) {
        console.log("datos: " + data);
        $$(".nombre").html(item.nombre + ' ' + item.apellido);
        $$(".seguro").html(item.seguro);
        $$(".fechaEmision").html(item.fecha_emis);
        $$(".planSeguro").html(item.tipo);
        $$(".fechaVencimiento").html(item.fecha_venc);
        $$(".noAfiliado").html(item.NoAfiliado);
        $$(".nss").html(item.nss);
        $$(".contracto").html(item.contracto);
      });
    },
    error: function (xhr) {
      console.log("error: " + xhr);
    },
  });
}

/*clickPayment();
  var card = $$('#tarjeta').val();
  var stripe = Stripe('pk_live_51GyKLAAMPIjnnC2QYxitWjmGpu25bMeQXH4I8ujcDU3Z0RbGSZTHszjbBPjQ0ADHriOcby3ESY8yRLyOoDAZ3GIM00IvXpbkDX');
  //var stripe = Stripe('pk_test_Jpn2hhyVlLu9pmqdYAadfCtP00msyOCjQq');
  //var stripe = Stripe('pk_live_YvuZnehCZnLTmTWKOW1fey1r00rCEBMVnt');
  //ID del restaurante.
  var idRestaurante = app.view.main.router.currentRoute.params.idRestaurante;
  // Total del carrito
  var totalidad = app.view.main.router.currentRoute.params.value;
  // Total impuesto Municipal
  var txMunicipal = app.view.main.router.currentRoute.params.municipal;
  $$('#valueTaxMunicipal').val(txMunicipal);
  $$('#texesMunicipal').html('US$ ' + formatter.format(txMunicipal).replace('$', ''));
  // Total Impuesto State
  var txState = app.view.main.router.currentRoute.params.state;
  $$('#valueTaxState').val(txState);
  $$('#texesState').html('US$ ' + formatter.format(txState).replace('$', ''));
  /*taxState(totalidad);
  taxMunicipal(totalidad);
  taxMenu(totalidad);
  $$('#subTotales').html('US$ ' + formatter.format(totalidad).replace('$', ''));
  $$('.titol').val(formatter.format(totalidad).replace('$', ''));
  $$('#servic').html('US$ 5.00');
  restaurantesID(idRestaurante);
  $$('#deliCarry').change(function () {
    var valores = $$('#deliCarry').val();
    if (valores == 1) {
      $$('.selDir').show();
      $$('#selOptions').html('Seleccionar su dirección');
      $$('#dirEntrega').show();
      $$('#dirRecogida').hide();
      $$('#direccion').val('');

    } else if (valores == 2) {
      $$('.selDir').show();
      $$('#selOptions').html('Lugar de recogida');
      $$('#dirEntrega').hide();
      $$('#dirRecogida').show();
      $$('#direccion').val(idRestaurante);
      $$('#direccionEntrega').html('Dirección');
      //
      $$('#deliveryServ').html('US$ 0.00');
      $$('#deliveryServInput').val('0');
      $$('#endPago').prop('disabled', false);
      $$("#endPago").removeClass("dis");
      sumar();
    } else {
      $$('.selDir').hide();
    }
  });

  setTimeout(function () {
    sumar();
  }, 2000);


  //Boton de proceder a pagar
  $$('#endPago').on('click', function (event) {

    var tarjeta = $$('#tarjeta').val(); //ID de la tarjeta de credito
    var direccion = $$('#direccion').val(); //ID de la direccion
    var valores = $$('#deliCarry').val(); // Entrega o recogida
    var stripeToken = $$('#stripeToken').val(); // token Stripe
    var id = getParameterByName('id'); //ID del usuario
    var taxStateVal = $$('#valueTaxState').val();// Precio de impuesto de state.
    var taxMunicipalVal = $$('#valueTaxMunicipal').val();// precio de impuesto municiapal.
    //var taxMenuVal = $$('#valueTaxMenu').val();// precio de impuesto municiapal.
    var deliveryVal = $$('#deliveryServInput').val();// Precio de entrega.
    var total = app.view.main.router.currentRoute.params.value;

    //Validar campos llenos
    if (tarjeta == "" || direccion == "") {
      app.dialog.alert('Completa todos los campos');
    } else {
      $$('#endPago').prop('disabled', true);
      //Asignar los datos de la tarjeta
      app.request({
        type: "GET",
        dataType: "json",
        url: "https://laundryappmovil.com/api-nes/api/pagoUni.php?id=" + tarjeta + "&user=" + id,
        beforeSend: function (xhr) {
          app.preloader.show();
        }, success: function (data) {
          //app.preloader.hide();
          $.each(data, function (i, item) {
            Stripe.setPublishableKey('pk_live_51GyKLAAMPIjnnC2QYxitWjmGpu25bMeQXH4I8ujcDU3Z0RbGSZTHszjbBPjQ0ADHriOcby3ESY8yRLyOoDAZ3GIM00IvXpbkDX'); //KEY de Stripe
            //Stripe.setPublishableKey('pk_test_Jpn2hhyVlLu9pmqdYAadfCtP00msyOCjQq'); //KEY de Stripe
            //Stripe.setPublishableKey('pk_live_YvuZnehCZnLTmTWKOW1fey1r00rCEBMVnt');
            Stripe.createToken({ //Crear el token con la tarjeta a pagar stripe
              number: item.numero,//Numero de tarjeta
              cvc: item.cvv,//Cvv de la tarjeta
              exp_month: item.mes,// Mes de expiracion
              exp_year: item.ano//Año de expiracion
            }, handleStripeResponse);
            return false;
          });
        }, error: function (xhr) {
          console.log('Bobo al 100');
          app.preloader.hide();
          $$('#endPago').prop('disabled', false);
        }
      });

      function handleStripeResponse(status, response) {
        console.log(JSON.stringify(response));
        if (response.error) {
          app.preloader.hide();
          console.log('Error mi Lord');
          app.dialog.alert(response.error.message);
        } else {
          console.log('Eso esta nitido mi papa');
          var stripeToken = response['id'];
          $('#stripeToken').val(stripeToken);
          var id = getParameterByName('id'); //ID del usuario
          app.request({
            type: "POST",
            url: "https://laundryappmovil.com/api-nes/php_stripe/charge.php",
            data: {
              "id_user": id,
              "token": stripeToken, // Token de stripe
              "precio": total, // Sub Total
              "id_metodo": tarjeta, // ID de la tarjeta
              "direccion": direccion, // ID de la direccion cliente o restaurante.
              "idRestaurante": idRestaurante, // ID del restaurante.
              "tipo": valores, // Recogida o entrega.
              "taxState": taxStateVal, // Precio de impuesto de state.
              "taxMunicipal": taxMunicipalVal, // precio de impuesto municiapal.
              "delivery": deliveryVal// Precio de entrega.

            },
            beforeSend: function (xhr) {
              app.preloader.show();
            }, success: function (data) {
              //console.log(data);

              if (data == 2) {
                console.log(data);
                //MENSAJE DEL CENTRO
                app.preloader.hide();
                $$('#endPago').prop('disabled', false);
                app.preloader.hide();
                var toastCenterFail = app.toast.create({
                  text: 'Tu tarjeta fue rechazada',
                  position: 'center',
                  closeTimeout: 2000,
                });

                toastCenterFail.open();
                console.log('Pago fallido.');

              } else if (data == 1) {
                console.log(data);
                app.preloader.hide();
                $$('#endPago').prop('disabled', true);
                var toastCenterUs = app.toast.create({
                  text: 'Transaccion exitosa, Gracias por confiar en nuestro servicios.',
                  position: 'center',
                  closeTimeout: 2500,
                });
                app.preloader.hide();
                toastCenterUs.open();
                setTimeout(function () {
                  app.views.main.router.navigate('/factura/' + total);
                }, 3000);
                console.log('Pago exitoso');
              } else {
                console.log(data);
                //MENSAJE DEL CENTRO
                app.preloader.hide();
                $$('#endPago').prop('disabled', false);
                app.preloader.hide();
                var toastCenterFail = app.toast.create({
                  text: 'Tu tarjeta fue rechazada',
                  position: 'center',
                  closeTimeout: 2000,
                });

                toastCenterFail.open();
                console.log('Pago fallido error en el codigo.');
              }

            }, error: function () {
              app.preloader.hide();
            }
          });

        }
      }
    } //Cierre condicional de campos vacios
  });
*/