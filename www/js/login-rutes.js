var routes = [

  {
    path: '/',
    url: './index.html',
    name:'index',
  },
  {
    path: '/login/',
    url: './pages/login.html',
  },
  {
    path: '/registrer/:value?',
    url: './pages/register.html',
    name :'registrer',
  },
   {
    path: '/restaurantes-login/',
    url: './pages/restaurantes-login.html',
    name :'restaurantes-login',
  },
    // Restaurantes de page
  {
    path: '/detail-restaurante/:value?',
    url: './pages/detail-restaurante.html',
    name: 'detail-restaurante',
  },
  {
    path: '/reset-password/:value?',
    url: './pages/reset-password.html',
    name :'reset-password',
  },
  {
    path: '/send-email/:value?',
    url: './pages/send-email.html',
    name :'send-email',
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    componentUrl: './pages/dynamic-route.html',
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function (routeTo, routeFrom, resolve, reject) {
      // Router instance
      var router = this;

      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = routeTo.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: 'Fernando',
          lastName: 'Burgos',
          about: 'Hello, i am creator of Moviwashlaundry Hope you like it!',
          links: [
            {
              title: 'Moviwashlaundry',
              url: 'http://Moviwashlaundry.com',
            },
            {
              title: 'Moviwashlaundry',
              url: 'http://Moviwashlaundry.com',
            },
          ]
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            componentUrl: './pages/request-and-load.html',
          },
          {
            context: {
              user: user,
            }
          }
        );
      }, 1000);
    },
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
  
];
